#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 17:49:46 2018

@author: chemgen
"""
#plt.rcParams.update(plt.rcParamsDefault)
import os
import re
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np 
from sklearn.cluster import AgglomerativeClustering, KMeans
import random
from sklearn import metrics
from goatools.semantic import semantic_similarity
from goatools.semantic import lin_sim
import matplotlib.tri as tri
from scipy.interpolate import griddata
from sklearn.preprocessing import RobustScaler
from sklearn.preprocessing import MinMaxScaler
from goatools.godag_plot import plot_gos, plot_results, plot_goid2goobj
"""
GO database
"""
obodag_path = "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Ontology/goslim_yeast.obo" 
gene2go_path = "/media/chemgen/Chemgen/Databases/gene2go"
gene_association_file_path = "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Ontology/go_slim_mapping_sgd.tab"
uniport = "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/yeast_uniport_id_conversion.txt"

def load_id_convertor(uniport):
    id_convertor_dataset= {}
    f = open(uniport, "r")
    lines = f.readlines()
    for line in lines:
        first_split = re.split("  ", line, 1)
        gene =re.split(";", first_split[0])[0]
        onl = re.split("  .*",first_split[1].strip(" "), 1)[0].strip(" ")
        id_convertor_dataset[onl]= gene
        id_convertor_dataset[gene]= onl
    return id_convertor_dataset

def id_concvertor(id_to_conver):
    id_convertor_loaded = load_id_convertor(uniport)
    for name, i in zip(id_to_conver, range(len(id_to_conver))):
        try:
            id_to_conver[int(i)] = id_convertor_loaded[name]
        except KeyError:
            pass
    return(id_to_conver)

def get_dirs (path):
    path_of_dirs = []
    for root, dirs, files in os.walk(path):
        if len(dirs) > 0 :
            for item in dirs:
                path_of_dirs.append(os.path.join(root,item))
    return path_of_dirs        
            
def get_dic (path_of_dirs):
    master_dic = {}
    for file_path in path_of_dirs:
        dic_name = file_path.split("/")[-1] 
        dic_val = {}
        for root, dirs, files in os.walk(file_path):
            for file in files:
                if file.endswith(".list"):
                    file_with_path =  os.path.join(root,file)
                    file_name = file.split(".")[0]
                    if re.search(r".*_ARV1.*_eigenvector", file_name) :
                        dic_val["ARV1_eigenvector"] = pickle.load(open(file_with_path, "rb" ))  
                    if re.search(r".*_ARV1.*_betwenness", file_name) :
                        dic_val["ARV1_betwenness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_ARV1.*_closeness", file_name) :
                        dic_val["ARV1_closeness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_ARV1.*_name", file_name) :
                        dic_val["ARV1_names"] = pickle.load(open(file_with_path, "rb" )) 
        
                    if re.search(r".*_ATOR.*_eigenvector", file_name) :
                        dic_val["ATOR_eigenvector"] = pickle.load(open(file_with_path, "rb" ))  
                    if re.search(r".*_ATOR.*_betwenness", file_name) :
                        dic_val["ATOR_betwenness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_ATOR.*_closeness", file_name) :
                        dic_val["ATOR_closeness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_ATOR.*_name", file_name) :
                        dic_val["ATOR_names"] = pickle.load(open(file_with_path, "rb" )) 
                        
                    if re.search(r".*_CERI.*_eigenvector", file_name) :
                        dic_val["CERI_eigenvector"] = pickle.load(open(file_with_path, "rb" ))  
                    if re.search(r".*_CERI.*_betwenness", file_name) :
                        dic_val["CERI_betwenness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_CERI.*_closeness", file_name) :
                        dic_val["CERI_closeness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_CERI.*_name", file_name) :
                        dic_val["CERI_names"] = pickle.load(open(file_with_path, "rb" )) 
        
                    if re.search(r".*_HMG1.*_eigenvector", file_name) :
                        dic_val["HMG1_eigenvector"] = pickle.load(open(file_with_path, "rb" ))  
                    if re.search(r".*_HMG1.*_betwenness", file_name) :
                        dic_val["HMG1_betwenness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_HMG1.*_closeness", file_name) :
                        dic_val["HMG1_closeness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_HMG1.*_name", file_name) :
                        dic_val["HMG1_names"] = pickle.load(open(file_with_path, "rb" )) 
                        
                    if re.search(r".*_HMG2.*_eigenvector", file_name) :
                        dic_val["HMG2_eigenvector"] = pickle.load(open(file_with_path, "rb" ))  
                    if re.search(r".*_HMG2.*_betwenness", file_name) :
                        dic_val["HMG2_betwenness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_HMG2.*_closeness", file_name) :
                        dic_val["HMG2_closeness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_HMG2.*_name", file_name) :
                        dic_val["HMG2_names"] = pickle.load(open(file_with_path, "rb" )) 
                        
                    if re.search(r".*_Mevalonate genes.*_eigenvector", file_name) :
                        dic_val["Mevalonate genes_eigenvector"] = pickle.load(open(file_with_path, "rb" ))  
                    if re.search(r".*_Mevalonate genes.*_betwenness", file_name) :
                        dic_val["Mevalonate genes_betwenness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_Mevalonate genes.*_closeness", file_name) :
                        dic_val["Mevalonate genes_closeness"] = pickle.load(open(file_with_path, "rb" )) 
                    if re.search(r".*_Mevalonate genes.*_name", file_name) :
                        dic_val["Mevalonate genes_names"] = pickle.load(open(file_with_path, "rb" )) 
                    master_dic[dic_name] = dic_val
    return master_dic

def scatter_for_each_background(bt_list ,name ,title, c):
    sorted_bt = sorted(bt_list)[-10:]
    x_val = bt_list * np.sin(bt_list)
    y_val = bt_list * np.cos(bt_list) 
    plt.axis('off')
    ax =plt.scatter(x_val , y_val, c = c)#,cmap=cdict)
    for label, x, y , cat_c in zip(name, x_val, y_val, bt_list):
        if  cat_c in sorted_bt:
            print(str(label) + "==" + str(x))
            plt.annotate(
                label,
                xy=(x, y), xytext=(random.choice([10,-20]), random.choice([10,20])),
                textcoords='offset points', ha='right', va='bottom',
                arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
            
    ttl = plt.title(title)
    ttl.set_position([.5, 1.25])
    plt.xlabel('Sin Betweenness')
    plt.ylabel('Cos Closness')
    return ax  

# clustering  
def cluster_df(f1_betweeness, f2_closseness, f3_eigen, n_cluster ):
    three_feature = np.column_stack((f1_betweeness, f2_closseness, f3_eigen))
    scaler = MinMaxScaler()
    scaler.fit(three_feature)
    Three_feature_scaled = scaler.transform(three_feature)
    kmean_score = []
    agg_score = []
    agg_score_ca = []
    kmean_score_ca = []
    for i in range(n_cluster , n_cluster+6):
        agg = AgglomerativeClustering(n_clusters=i)
        assignment = agg.fit_predict(Three_feature_scaled)
        score_a = metrics.silhouette_score(Three_feature_scaled , assignment , metric='euclidean')
        score_ca =metrics.calinski_harabaz_score(Three_feature_scaled, assignment)
        agg_score.append(score_a)
        agg_score_ca.append(score_ca)
        
        kmeans_model = KMeans(n_clusters=i, random_state=1).fit(Three_feature_scaled)
        kmeanl = kmeans_model.labels_
        score_k = metrics.silhouette_score(Three_feature_scaled , kmeanl , metric='euclidean')
        score_ca_k =metrics.calinski_harabaz_score(Three_feature_scaled, kmeanl)
        kmean_score.append(score_k)
        kmean_score_ca.append( score_ca_k)
        
    if max(kmean_score) > max(agg_score):
        eff =kmean_score.index(max(kmean_score))
        kmeans_model = KMeans(n_clusters= n_cluster + eff, random_state=1).fit(Three_feature_scaled)
        assignment = kmeans_model.labels_
        ty = "K-Mean"
        score_to_report = max(kmean_score)
        index_to_report = max(kmean_score_ca)
        
    else:
        eff =agg_score.index(max(agg_score))
        agg = AgglomerativeClustering(n_clusters= n_cluster + eff)
        assignment = agg.fit_predict(Three_feature_scaled)
        ty = "Agglomerative"
        score_to_report = max(agg_score)
        index_to_report = max(agg_score_ca)
        
    return  assignment, ty, score_to_report,  index_to_report, eff


def cluster_b_f ( b_f_array, n_cluster):
    one_feature = np.array(b_f_array).reshape(-1, 1)
    kmean_score = []
    agg_score = []
    agg_score_ca = []
    kmean_score_ca = []
    for i in range(n_cluster , n_cluster+6):
        agg = AgglomerativeClustering(n_clusters=i)
        assignment = agg.fit_predict(one_feature)
        score_a = metrics.silhouette_score(one_feature , assignment , metric='euclidean')
        score_ca =metrics.calinski_harabaz_score(one_feature, assignment)
        agg_score.append(score_a)
        agg_score_ca.append(score_ca)
        
        kmeans_model = KMeans(n_clusters=i, random_state=1).fit(one_feature )
        kmeanl = kmeans_model.labels_
        score_k = metrics.silhouette_score(one_feature , kmeanl , metric='euclidean')
        score_ca_k =metrics.calinski_harabaz_score(one_feature , kmeanl)
        kmean_score.append(score_k)
        kmean_score_ca.append( score_ca_k)
    
    if max(kmean_score) > max(agg_score):
        eff =kmean_score.index(max(kmean_score))
        kmeans_model = KMeans(n_clusters= n_cluster + eff, random_state=1).fit(one_feature )
        assignment = kmeans_model.labels_
        ty = "K-Mean"
        score_to_report = max(kmean_score)
        index_to_report = max(kmean_score_ca)
    else:
        eff =agg_score.index(max(agg_score))
        agg = AgglomerativeClustering(n_clusters= n_cluster + eff)
        assignment = agg.fit_predict(one_feature)
        ty = "Agglomerative"
        score_to_report = max(agg_score)
        index_to_report = max(agg_score_ca)
    return  assignment, ty, score_to_report, index_to_report, eff

def get_cluster_value (masterdic, n_cluster_df, n_cluster_sf ): #number of clusters and fetures
    heatmap_dic = {}
    typ_score = {}
    
    for key in masterdic.keys():
        for child_key in masterdic[key].keys():
            if re.search(".*_betwenness", child_key): #betweenness
                name_for_dic = child_key.replace( "_betwenness","")
                f2_closseness = child_key.replace( "betwenness","closeness")
                f3_eigen =  child_key.replace( "betwenness","eigenvector")
                names = child_key.replace( "betwenness","names")
                clustered_three_f, tyd, scd, cascore, effd =cluster_df(masterdic[key][child_key],
                                            masterdic[key][f2_closseness],
                                            masterdic[key][f3_eigen],
                                            n_cluster_df) # thre feature cluster
                
                clustered_betweenness, tys_b, scs_b, cascore_sb, effs_b =cluster_b_f(masterdic[key][child_key], n_cluster_sf) 
                clustered_closness, tys_c, scs_c, cascore_sc, effs_c =cluster_b_f(masterdic[key][f2_closseness], n_cluster_sf)
                clustered_eigen, tys_e, scs_e,cascore_se, effs_e =cluster_b_f(masterdic[key][f3_eigen], n_cluster_sf)
                
                heatmap_dic["{}_{}".format(key,
                             name_for_dic)] = [ masterdic[key][names], masterdic[key][child_key],
    masterdic[key][f2_closseness],masterdic[key][f3_eigen], clustered_three_f,
    clustered_betweenness,
    clustered_closness,
    clustered_eigen] 
               
                
                
                typ_score["{}_{}".format(key,
                             name_for_dic)] = [ tyd, scd,cascore, effd,
               tys_b, scs_b, cascore_sb, effs_b,
               tys_c, scs_c,cascore_sc, effs_c,
               tys_e, scs_e,cascore_se, effs_e] 
                
    return heatmap_dic, typ_score
            
def get_gene_from_cluster (clustered, lim, lim_side, type_cluster):
    active_gene = {}
    for key in clustered.keys():    
        name_list = []
        between_list = []
        close_list = []
        eigen_list = []
        cat_thf_list = []
        cat_between_list = []
        cat_closs_list = []
        cat_eigen_list = []
        lim_r= range(min(clustered[key][type_cluster]), max(clustered[key][type_cluster]))
        for n, b, c, e, thf, bf, cf, ef in  zip(clustered[key][0],
                                               clustered[key][1],
                                               clustered[key][2],
                                               clustered[key][3], 
                                               clustered[key][4], 
                                               clustered[key][5],
                                               clustered[key][6],
                                               clustered[key][7]):
            
             temp_list = [ n, b, c, e, thf, bf, cf, ef]
             if lim_side == "l":
                 if  temp_list[type_cluster] < (min(lim_r) + lim):
                    name_list.append(n)
                    between_list.append(b)
                    close_list.append(c)
                    eigen_list.append(e)
                    cat_thf_list.append(thf)
                    cat_between_list.append(bf)  
                    cat_closs_list.append(cf) 
                    cat_eigen_list.append(ef) 
             if lim_side == "r":
                 if  temp_list[type_cluster]  > (max(lim_r) - (lim -1) ):
                    name_list.append(n)
                    between_list.append(b)
                    close_list.append(c)
                    eigen_list.append(e)
                    cat_thf_list.append(thf)
                    cat_between_list.append(bf)  
                    cat_closs_list.append(cf) 
                    cat_eigen_list.append(ef) 

             active_gene[key]= [name_list, between_list, close_list,
                   eigen_list, cat_thf_list, cat_between_list,
                   cat_closs_list, cat_eigen_list] 
    return active_gene



 # To get N of cluster only X many in the last cluster
        
def counter (assignment, n):
    sec_counter = 0
    for i in assignment:
        if i == (n-1):
             sec_counter += 1
    print("gene in category with n = {}: ".format(n) + str(sec_counter))    
    if sec_counter > 10 or sec_counter < 4:
        n += 1 
        print("number of categories changed to: " + str(n))
    else:
        sec_counter= 5  
    return (n, sec_counter) 


def list_gene (df_array):
    n = 3
    sec_counter = 10
    while sec_counter != 5 :
        assignment = cluster(n, df_array)
        n, sec_counter = counter(assignment, n)
    return (n, assignment)


##########################      
#older version
#def get_list_of_active_gene (clustered, limit_d, dd, limit_s, d):
#    active_gene = {}
#    for key in clustered.keys():
#        name_list = []
#        between_list = []
#        close_list = []
#        cat_d_list = []
#        cat_s_list = []
#        lim_s= range(min(clustered[key][4]), max(clustered[key][4]))
#        lim_d= range(min(clustered[key][3]), max(clustered[key][3])) 
#        for name, between, closenss, cat_d, cat_s in zip(clustered[key][0], clustered[key][1],
#                                                         clustered[key][2],clustered[key][3], 
#                                                         clustered[key][4]):
#            if dd == "l" and d == "l":
#                if  (cat_d < min(lim_d) + limit_d and
#                        cat_s < min(lim_s) + limit_s ):
#                    name_list.append(name)
#                    between_list.append(between)
#                    close_list.append(closenss)
#                    cat_d_list.append(cat_d)
#                    cat_s_list.append(cat_s)
#            if dd == "r" and d == "r":
#                if  (cat_d > max(lim_d) - limit_d  and 
#                     cat_s > max(lim_s) - limit_s):
#                    name_list.append(name)
#                    between_list.append(between)
#                    close_list.append(closenss)
#                    cat_d_list.append(cat_d)
#                    cat_s_list.append(cat_s)
#            if dd == "l" and d == "r":
#                if  (cat_d < min(lim_d) + limit_d and
#                     cat_s > max(lim_s) - limit_s ):
#                    name_list.append(name)
#                    between_list.append(between)
#                    close_list.append(closenss)
#                    cat_d_list.append(cat_d)
#                    cat_s_list.append(cat_s)                    
#            if dd == "r" and d == "l":
#                if  (cat_d > max(lim_d) - limit_d  and 
#                     cat_s < min(lim_s) + limit_s ):
#                    name_list.append(name)
#                    between_list.append(between)
#                    close_list.append(closenss)
#                    cat_d_list.append(cat_d)
#                    cat_s_list.append(cat_s)                      
#            active_gene[key]= [name_list,
#                       between_list, close_list,
#                       cat_d_list, cat_s_list] 
#    return active_gene
            
   
# if clustering is not to be used
def get_top_30 (bt_list_key, names_list_key):
    top_30 = {}
    sorted_bt = sorted(bt_list_key)[-30:]
    for score, name in zip(bt_list_key, names_list_key):
         if  score in sorted_bt:
             top_30[name]=[score]
    return top_30
    
def get_heatmap_dic (masterdic):
    heatmap_dic = {}
    for key in masterdic.keys():
        for child_key in masterdic[key].keys():
            #print(child_key)
            if re.search(".*_betwenness", child_key):
                child_name_key = child_key.replace("betwenness", "names")
                heatmap_dic["{}_{}".format(key, child_key)] = get_top_30(masterdic[key][child_key],
                            masterdic[key][child_name_key])
    return(heatmap_dic)


def fill_hole_heatmap_dic (master_dic):
    filled_heatmap_dic = {}
    name_genes = set()
    with_holes = get_heatmap_dic(master_dic)
    for key in with_holes.keys():
        for i in with_holes[key]:
            name_genes.add(i)
    for i in name_genes:
        filled_heatmap_dic[i]=[0]*20
    for key in master_dic.keys():
        for child_key in master_dic[key].keys():
            if re.search(".*_names", child_key):
                child_score_key = child_key.replace( "names","betwenness")
                #print( master_dic[key][child_score_key])
                for name,score in zip (master_dic[key][child_key], master_dic[key][child_score_key]):
                    if name in name_genes:
                        dic_index = background_key.index("{}_{}".format(key,child_key))
                        filled_heatmap_dic[name][dic_index]=score
    return  filled_heatmap_dic 

def Load_db_GO(obodag_path, gene2go_path ,gene_association_file_path):
    import csv
    from goatools.obo_parser import GODag
    #from gen ontology const 
    obodag = GODag(obodag_path)
    
    from goatools.associations import read_ncbi_gene2go
    #saved on drive /media/chemgen/Chemgen/Databases/gene2go.gz
    geneid2gos_yeast = read_ncbi_gene2go(gene2go_path , taxids=[559292]) #yeast taxid
    print("{N:,} annotated yeast genes".format(N=len(geneid2gos_yeast)))
    
    yeast_gene_GO ={}
    gene_association_file_path = gene_association_file_path
    with open (gene_association_file_path) as gene_associationsdg_file:
        gene_associationsdg_file = csv.reader(gene_associationsdg_file , delimiter='\t', quotechar='|')
        for row in gene_associationsdg_file:
            if row[0] in yeast_gene_GO.keys() :
                val = yeast_gene_GO[row[0]]
                val.append(row[5])
                yeast_gene_GO.update({row[0] : val})
            else:
               yeast_gene_GO[row[0]] = [row[5]]
    
    from goatools.go_enrichment import GOEnrichmentStudy
    goeaobj = GOEnrichmentStudy(
            yeast_gene_GO.keys(), # List of myeast gene
            yeast_gene_GO, # geneid/GO associations
            obodag, # Ontologies
            propagate_counts = False,
            alpha = 0.05, # default significance cut-off
            methods = ['fdr_tsbh']) # defult multipletest correction method
    
    from goatools.semantic import TermCounts
    termcounts = TermCounts(obodag, yeast_gene_GO)
    return obodag,  geneid2gos_yeast ,  goeaobj, termcounts

def get_go_terms(obodag, termcounts, list_gene, pval):
    import statsmodels.sandbox.stats.multicomp as stat 
    #go_id_list = []
    goea_terms = []
    go_terms_name = []
    go_cove_lis = []
    go_pvalue = []
    score = []
    goea_results_all = goeaobj.run_study(list_gene)
    goea_results_sig= [r for r in goea_results_all if stat.multipletests(r.p_uncorrected
                                                                        , alpha=0.05,
                                                                        method="fdr_tsbky" )[1][0] < pval]
    #FDR 2-stage Benjamini-Krieger-Yekutieli
    #Adaptive linear step-up procedures that control the false
    #discovery rate
    for go in goea_results_sig:
       # try :
           # if obodag[ go ].namespace == 'biological_process':
                goea_terms.append(go.GO)  
                go_terms_name.append(go.name)
                go_cove_lis.append(go.ratio_in_study[0]/go.ratio_in_study[1])
                go_pvalue.append(stat.multipletests(go.p_uncorrected, alpha=0.05,method="fdr_tsbky" )[1][0])
        #except KeyError:
            # pass        
    #Orange_result = annotations.get_enriched_terms(list_gene, slims_only=True)
    #for go_id, (genes, p_value, _) in Orange_result.items():
     #   if p_value < pval:
            #try :
             #   if obodag[ go_id ].namespace == 'biological_process':
                   
                    #go_id_list.append(go_id)
                    #go_cove_lis.append(len(genes)/_)
                    #go_pvalue.append(p_value)
            #except KeyError:
    score = get_paire_simil(obodag, termcounts,  goea_terms)         #   pass
    return  goea_terms, go_terms_name, go_cove_lis, go_pvalue, score

def get_go_terms_sorted(obodag, termcounts, list_gene, pval):
    import statsmodels.sandbox.stats.multicomp as stat 
    goea_terms = []
    go_terms_name = []
    go_cove_lis_s = {}
    go_cove_lis = []
    go_pvalue = []
    score = []
    goea_results_all = goeaobj.run_study(list_gene)
    goea_results_sig= [r for r in goea_results_all if stat.multipletests(r.p_uncorrected
                                                                        , alpha=0.05,
                                                                        method="fdr_tsbky" )[1][0] < pval]
    
    for go in goea_results_sig:
#        try:
#            if obodag[ go ].namespace == 'biological_process':
                if (go.ratio_in_study[0]/go.ratio_in_study[1]) > 0 :
                    go_cove_lis_s[(go.ratio_in_study[0]/go.ratio_in_study[1])]= [go.GO,
                                go.name,(go.ratio_in_study[0]/go.ratio_in_study[1]),
                                stat.multipletests(go.p_uncorrected, alpha=0.05,method="fdr_tsbky" )[1][0]]
#            else:
#                    pass
#        except KeyError:
#            pass  
#                
    sorted_go = sorted(go_cove_lis_s.keys(), reverse=True)  
    for cov in sorted_go:
            goea_terms.append(go_cove_lis_s[cov][0])  
            go_terms_name.append(go_cove_lis_s[cov][1])
            go_cove_lis.append(go_cove_lis_s[cov][2])
            go_pvalue.append(go_cove_lis_s[cov][3])
    score = get_paire_simil(obodag, termcounts,  goea_terms)
    return  goea_terms, go_terms_name, go_cove_lis, go_pvalue, score 
    
def draw_con (x,y,z , subx, suby):
    from matplotlib import rc
    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    rc('text', usetex=True)
    axes[subx, suby].set_yticks(np.array([0,0.5]))
    axes[subx, suby].set_xticks(np.array([0,1]))
    ngridx = 1500
    ngridy = 1500
    xi = np.linspace(min(x), max(x), ngridx)
    yi = np.linspace(min(y), max(y), ngridy)
    zi = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')
    axes[subx, suby].contour(xi, yi, zi, 5, linewidths=0.5, colors='k')
    cntr1 = axes[subx, suby].contourf(xi, yi, zi, 14, cmap="RdBu_r")
    axes[subx, suby].plot(x, y, 'ko', ms=1)
    return 


def get_paire_simil(obodag, termcounts, go_id_list):
    score = []
    pairwise = [[lin_sim(i, j, obodag, termcounts)  for i in go_id_list] for j in go_id_list]
    pwm = np.nan_to_num(np.matrix(pairwise, dtype=float))
    mpwm =  pwm.mean()
    for row in range(len(go_id_list)):
        score.append(-1 * (mpwm + np.mean(pwm[row])))
    return score


def ann (tc, given, y, setscore= 0.8, setcev=0.8): # dic, name, y, score, coverage
    import random
    IDs  = re.sub("Score", "GO_id", given)
    covs = re.sub("Score", "Coverage", given)
    for name , score, coverage, pval in zip(tc[IDs], tc[given],
                                       tc[covs], y ):
        if score > setscore and coverage > setcev:
                plt.annotate(name , xy=(score, pval),
                             xytext=(score + random.uniform(-0.5,0.5), pval ),
                             textcoords='offset points', ha='right', va='bottom',
                                bbox=dict(boxstyle='round,pad=0.5', fc='#34495e', alpha=0.5))
                        

 
""" Getting a gene position"""
#with open ("/home/chemgen/Desktop/Temp/Final_Figures/IRE_position.csv", "w") as file :
#    file.write("SGA" + "\t" + "betweenness" + "\t"+ "closeness" + "\t"+ "Eigenvector" + "\t " + "cluster" + "\n")
#    for key in clustered.keys():
#        for i,j,m,n,h in zip (clustered[key][0], clustered[key][1],
#                              clustered[key][2], clustered[key][3], clustered[key][4]):
#            if i== "YHR079C":
#                file.write(str(key) + "\t" + str(j) + "\t"+ str(m) + "\t"+ str(n) + "\t " + str(h)+ "\n")
                
background_key = ['S288C_ARV1_names', 'YPS606_ARV1_names',
  'UWOPS87_ARV1_names', 'Y55_ARV1_names',
  'S288C_CERI_names', 'YPS606_CERI_names',
  'UWOPS87_CERI_names', 'Y55_CERI_names',
  'S288C_ATOR_names', 'YPS606_ATOR_names',
  'UWOPS87_ATOR_names', 'Y55_ATOR_names',
  'S288C_HMG1_names', 'YPS606_HMG1_names',
  'UWOPS87_HMG1_names', 'Y55_HMG1_names',
  'S288C_HMG2_names', 'YPS606_HMG2_names',
  'UWOPS87_HMG2_names', 'Y55_HMG2_names']
##############################


#%%
    #df4plot  =  pd.DataFrame(dic_val)
"""get the master dic"""    
path_of_dirs = get_dirs("/home/chemgen/Desktop/Temp/Bede/Topology")
plot_dic =     get_dic(path_of_dirs)

#clustered, type_dic = get_cluster_value (plot_dic, 3, 3) 
#df_type = pd.DataFrame.from_dict(type_dic  , orient='index')
#df_type.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("Supplementary_Figure5_A")) 
#with open('/home/chemgen/Desktop/Temp/Final_Figures/clusterd.pickle', 'wb') as handle:
#   pickle.dump(clustered, handle, protocol=pickle.HIGHEST_PROTOCOL)

clustered = pickle.load( open( "/home/chemgen/Desktop/Temp/Final_Figures/clusterd.pickle", "rb" ) )
h4f = get_gene_from_cluster(clustered, 1, "r", 4) 
hb = get_gene_from_cluster(clustered, 1, "r", 5) 
hc= get_gene_from_cluster(clustered, 1, "r", 6) 
he = get_gene_from_cluster(clustered, 1, "r", 7)
all_else = get_gene_from_cluster(clustered, 3, "l", 4) 

obodag,  geneid2gos_yeast ,  goeaobj, termcounts =Load_db_GO(obodag_path, gene2go_path ,gene_association_file_path)



#%%%
""" annotation of clusters"""
df_file = pd.ExcelFile("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Final(copy)_for_python.xlsx")
exel_df = {}
for sheet in df_file.sheet_names:
    df_parsed = df_file.parse(sheet)
    Systematic_Name = df_parsed['Systematic Name'].tolist()
    goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag, termcounts, Systematic_Name, 0.05)
    exel_df["{}_GO_id".format(sheet)] =  goea_terms
    exel_df["{}_GO_id_name".format(sheet)] =  go_terms_name
    exel_df["{}_Coverage".format(sheet)] = go_cove_lis
    exel_df["{}_P_val".format(sheet)] = go_pvalue
    exel_df["{}_Score".format(sheet)] = score 
df = pd.DataFrame.from_dict(exel_df, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("GO_file_TI")) 

exel_df3f = {}
tc = h4f
tcn= "h4f"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_df3f["{}_GO_id".format(key)] =  goea_terms
        exel_df3f["{}_GO_id_name".format(key)] =  go_terms_name
        exel_df3f["{}_Coverage".format(key)] = go_cove_lis
        exel_df3f["{}_P_val".format(key)] = go_pvalue
        exel_df3f["{}_Score".format(key)] = score
df = pd.DataFrame.from_dict(exel_df3f, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("3features")) 

exel_dfhb = {}
tc = hb
tcn= "hb"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_dfhb["{}_GO_id".format(key, tcn)] =  goea_terms
        exel_dfhb["{}_GO_id_name".format(key, tcn)] =  go_terms_name
        exel_dfhb["{}_Coverage".format(key, tcn)] = go_cove_lis
        exel_dfhb["{}_P_val".format(key, tcn)] = go_pvalue
        exel_dfhb["{}_Score".format(key, tcn)] = score
df = pd.DataFrame.from_dict(exel_dfhb, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("betweenness")) 

exel_dfhc = {}
tc = hc
tcn= "hc"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_dfhc["{}_GO_id".format(key, tcn)] =  goea_terms
        exel_dfhc["{}_GO_id_name".format(key, tcn)] =  go_terms_name
        exel_dfhc["{}_Coverage".format(key, tcn)] = go_cove_lis
        exel_dfhc["{}_P_val".format(key, tcn)] = go_pvalue
        exel_dfhc["{}_Score".format(key, tcn)] = score
df = pd.DataFrame.from_dict(exel_dfhc, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("closeness")) 

exel_dfhe = {}
tc = he
tcn= "he"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_dfhe["{}_GO_id".format(key, tcn)] =  goea_terms
        exel_dfhe["{}_GO_id_name".format(key, tcn)] =  go_terms_name
        exel_dfhe["{}_Coverage".format(key, tcn)] = go_cove_lis
        exel_dfhe["{}_P_val".format(key, tcn)] = go_pvalue
        exel_dfhe["{}_Score".format(key, tcn)] = score
df = pd.DataFrame.from_dict(exel_dfhc, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("eigenvector")) 
#%%%




"""toplogy maps"""
Y55= sns.light_palette("red", as_cmap=True)
S288C=  sns.light_palette("blue", as_cmap=True)
UWOPS87= sns.light_palette("gold", as_cmap=True)
YPS606= sns.light_palette("green", as_cmap=True)

sns.set_style("white")
plt.style.use("dark_background")

plot = sns.kdeplot(plot_dic['Y55']["ARV1_closeness"],plot_dic['Y55']["ARV1_betwenness"],
                   alpha=0.6, cmap=Y55, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['S288C']["ARV1_closeness"],plot_dic['S288C']["ARV1_betwenness"],
                   alpha=0.6, cmap=S288C, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic["UWOPS87"]["ARV1_closeness"],plot_dic["UWOPS87"]["ARV1_betwenness"],
                   alpha=0.6, cmap=UWOPS87, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['YPS606']["ARV1_closeness"],plot_dic['YPS606']["ARV1_betwenness"],
                   alpha=0.6, cmap=YPS606, shade=True, shade_lowest=False)
plot.set(xlim=(0.23, 0.53), ylim=(0,112000))
plot.set(xlabel='Betweenness', ylabel='Closness',title="ARV1")


plot = sns.kdeplot(plot_dic['Y55']["ATOR_closeness"],plot_dic['Y55']["ATOR_betwenness"],
                   alpha=0.6, cmap=UWOPS87, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['S288C']["ATOR_closeness"],plot_dic['S288C']["ATOR_betwenness"],
                   alpha=0.6, cmap=S288C, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic["UWOPS87"]["ATOR_closeness"],plot_dic["UWOPS87"]["ATOR_betwenness"],
                   alpha=0.6, cmap=UWOPS87, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['YPS606']["ATOR_closeness"],plot_dic['YPS606']["ATOR_betwenness"],
                   alpha=0.6, cmap=YPS606, shade=True, shade_lowest=False)
plot.set(xlim=(0.23, 0.40), ylim=(0,2000))
#plot.xaxis.set_ticks(np.arange(0.23, 0.53, 10))
plot.set(xlim=(0.23, 0.40), ylim=(0,20000))
plot.set(ylabel='Betweenness', xlabel='Closness',title="ATOR")


plot = sns.kdeplot(plot_dic['Y55']["CERI_closeness"],plot_dic['Y55']["CERI_betwenness"],
                   alpha=0.6, cmap=Y55, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['S288C']["CERI_closeness"],plot_dic['S288C']["CERI_betwenness"],
                   alpha=0.6, cmap=S288C, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic["UWOPS87"]["CERI_closeness"],plot_dic["UWOPS87"]["CERI_betwenness"],
                   alpha=0.6, cmap=UWOPS87, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['YPS606']["CERI_closeness"],plot_dic['YPS606']["CERI_betwenness"],
                   alpha=0.6, cmap=YPS606, shade=True, shade_lowest=False)
plot.set(xlim=(0.23, 0.40), ylim=(0,20000))
#plot.xaxis.set_ticks(np.arange(0.23, 0.53, 10))
plot.set(xlim=(0.23, 0.40), ylim=(0,20000))
plot.set(ylabel='Betweenness', xlabel='Closness',title="CERI")


plot = sns.kdeplot(plot_dic['Y55']["HMG1_closeness"],plot_dic['Y55']["HMG1_betwenness"],
                   alpha=0.6, cmap=Y55, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['S288C']["HMG1_closeness"],plot_dic['S288C']["HMG1_betwenness"],
                   alpha=0.6, cmap=S288C, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic["UWOPS87"]["HMG1_closeness"],plot_dic["UWOPS87"]["HMG1_betwenness"],
                   alpha=0.6, cmap=UWOPS87, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['YPS606']["HMG1_closeness"],plot_dic['YPS606']["HMG1_betwenness"],
                   alpha=0.6, cmap=YPS606, shade=True, shade_lowest=False)
plot.set(xlim=(0.23, 0.40), ylim=(0,20000))
#plot.xaxis.set_ticks(np.arange(0.23, 0.53, 10))
plot.set(xlim=(0.23, 0.40), ylim=(0,20000))
plot.set(ylabel='Betweenness', xlabel='Closness',title="HMG1")

plot = sns.kdeplot(plot_dic['Y55']["HMG2_closeness"],plot_dic['Y55']["HMG2_betwenness"],
                   alpha=0.6, cmap=Y55, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['S288C']["HMG2_closeness"],plot_dic['S288C']["HMG2_betwenness"],
                   alpha=0.6, cmap=S288C, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic["UWOPS87"]["HMG2_closeness"],plot_dic["UWOPS87"]["HMG2_betwenness"],
                   alpha=0.6, cmap=UWOPS87, shade=True, shade_lowest=False)
plot = sns.kdeplot(plot_dic['YPS606']["HMG2_closeness"],plot_dic['YPS606']["HMG2_betwenness"],
                   alpha=0.6, cmap=YPS606, shade=True, shade_lowest=False)
plot.set(xlim=(0.23, 0.40), ylim=(0,20000))
#plot.xaxis.set_ticks(np.arange(0.23, 0.53, 10))
plot.set(xlim=(0.23, 0.40), ylim=(0,20000))
plot.set(ylabel='Betweenness', xlabel='Closness',title="HMG2")


"""de convelouted scater plots """
ax=scatter_for_each_background(plot_dic['Y55']["ARV1_betwenness"] ,plot_dic['Y55']["ARV1_names"], "Y55 ARV1", "red")
ax=scatter_for_each_background(plot_dic['S288C']["ARV1_betwenness"] ,plot_dic['S288C']["ARV1_names"], "S288C ARV1", "blue")
ax=scatter_for_each_background(plot_dic['UWOPS87']["ARV1_betwenness"] ,plot_dic['UWOPS87']["ARV1_names"], "UWOPS87 ARV1", "gold")
ax=scatter_for_each_background(plot_dic['YPS606']["ARV1_betwenness"] ,plot_dic['YPS606']["ARV1_names"], "YPS606 ARV1", "green")

ax=scatter_for_each_background(plot_dic['Y55']["ATOR_betwenness"] ,plot_dic['Y55']["ATOR_names"], "Y55 ATOR", "red")
ax=scatter_for_each_background(plot_dic['S288C']["ATOR_betwenness"] ,plot_dic['S288C']["ATOR_names"], "S288C ATOR", "blue")
ax=scatter_for_each_background(plot_dic['UWOPS87']["ATOR_betwenness"] ,plot_dic['UWOPS87']["ATOR_names"], "UWOPS87 ATOR", "gold")
ax=scatter_for_each_background(plot_dic['YPS606']["ATOR_betwenness"] ,plot_dic['YPS606']["ATOR_names"], "YPS606 ATOR", "green")

ax=scatter_for_each_background(plot_dic['Y55']["CERI_betwenness"] ,plot_dic['Y55']["CERI_names"], "Y55 CERI", "red")
ax=scatter_for_each_background(plot_dic['S288C']["CERI_betwenness"] ,plot_dic['S288C']["CERI_names"], "S288C CERI", "blue")
ax=scatter_for_each_background(plot_dic['UWOPS87']["CERI_betwenness"] ,plot_dic['UWOPS87']["CERI_names"], "UWOPS87 CERI", "gold")
ax=scatter_for_each_background(plot_dic['YPS606']["CERI_betwenness"] ,plot_dic['YPS606']["CERI_names"], "YPS606 CERI", "green")

ax=scatter_for_each_background(plot_dic['Y55']["HMG2_betwenness"] ,plot_dic['Y55']["HMG2_names"], "Y55 HMG2", "red")
ax=scatter_for_each_background(plot_dic['S288C']["HMG2_betwenness"] ,plot_dic['S288C']["HMG2_names"], "S288C HMG2", "blue")
ax=scatter_for_each_background(plot_dic['UWOPS87']["HMG2_betwenness"] ,plot_dic['UWOPS87']["HMG2_names"], "UWOPS87 HMG2", "gold")
ax=scatter_for_each_background(plot_dic['YPS606']["HMG2_betwenness"] ,plot_dic['YPS606']["HMG2_names"], "YPS606 HMG2", "green")

"""counter plot"""
fig, axes = plt.subplots(4,5, sharey='row', sharex='col' )
row = 0
for i in ['Y55', 'S288C', 'YPS606', 'UWOP']:
    x0 = plot_dic[i]["ARV1_eigenvector"]
    y0 = plot_dic[i]["ARV1_closeness"]
    z0 = plot_dic[i]["ARV1_betwenness"]
    
    x1 = plot_dic[i]["ATOR_eigenvector"]
    y1 = plot_dic[i]["ATOR_closeness"]
    z1 = plot_dic[i]["ATOR_betwenness"]
    
    x2 = plot_dic[i]["CERI_eigenvector"]
    y2 = plot_dic[i]["CERI_closeness"]
    z2 = plot_dic[i]["CERI_betwenness"]
    
    x3 = plot_dic[i]["HMG1_eigenvector"]
    y3 = plot_dic[i]["HMG1_closeness"]
    z3 = plot_dic[i]["HMG1_betwenness"]
    
    x4 = plot_dic[i]["HMG2_eigenvector"]
    y4 = plot_dic[i]["HMG2_closeness"]
    z4 = plot_dic[i]["HMG2_betwenness"]
    draw_con(x0, y0, z0, row,0)
    draw_con(x1, y1, z1, row,1)
    draw_con(x2, y2, z2, row,2)
    draw_con(x3, y3, z3, row,3)
    cntr1 = draw_con(x4, y4, z4, row,4)
    row += 1
#fig.colorbar(cntr1, ax=axes)
fig.text(0.5, 0.04,'Eigenvector Centrality', ha='center', va='center')
fig.text(0.06, 0.5, 'Closeness Centrality', ha='center', va='center', rotation='vertical')
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/gra.png", dpi=500, transparent = False)



