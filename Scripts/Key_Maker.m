
%% Draw regions
sgn=1;
ucolors = unique(layout_key.groupColor(:,sgn));
if ucolors(1) ~= 1    ucolors = [1; ucolors];
end
rng(3,'twister');
map_colors = colormap(lines((1000*length(ucolors)-1)));
map_colors = map_colors(randperm(size(map_colors,1)),:);
map_colors = [1 1 1; map_colors]; 
layout_key.mapColors = map_colors;
layout_key.geneRGB = ones(length(layout_key.label),3);
layout_key.geneRGB = map_colors(layout_key.labelColor,:);
 
for i = [8,17,3, 15,2,7,14,5, 11,9, 18,10, 13,16,12,4,6]
    %2:length(ucolors)
    
    ax = gca;
     % inds = find(layout_key.labelColor(:,sgn) == ucolors(i));
    indsg = find(layout_key.groupColor(:,sgn) == ucolors(i));
    [r,~,~] = find(layout_key.opacity_01(:,indsg,sgn)>0);
    inds = unique(r);
    x = layout_key.x(inds);
    y = layout_key.y(inds);
    colors = layout_key.geneRGB(inds,:);
    
    xpass=  isoutlier(x);
    x = x(~xpass);
    y = y(~xpass);
    colors = colors(~xpass, :);
    
    ypass= isoutlier(y);
    y = y(~ypass);
    x = x(~ypass);
    colors = colors(~ypass, :);
%     x = vertcat(x, x+65);
%     x= vertcat(x, x-65);
%     y = vertcat(y, y+65);
%     y = vertcat(y, y-65);
    k=boundary(x , y, 0.3);
    patch(ax, x(k),y(k), [1 1 1], 'FaceAlpha', 0.95, 'EdgeColor', [0 0 0], "LineWidth", 1.25,...
        'LineStyle', '-.');
    centroid_x = sum((layout_key.labelOpacity(inds,sgn).^2) .* layout_key.x(inds)) ./ sum((layout_key.labelOpacity(inds,sgn).^2));
    centroid_y = sum((layout_key.labelOpacity(inds,sgn).^2) .* layout_key.y(inds)) ./ sum((layout_key.labelOpacity(inds,sgn).^2));
    sum((layout_key.labelOpacity(inds,sgn).^2) .* layout_key.y(inds)) ./ sum((layout_key.labelOpacity(inds,sgn).^2));
    hold on;
    %hText{i} = text(centroid_x, centroid_y, sprintf('%d',ucolors(i)),'FontSize',10,'Color','k','HorizontalAlignment','center');
    set(ax,'DataAspectRatio',[1 1 1]);
    set(ax, 'YDir','reverse');
    axis off;
    hold on;
    
end
 
 
  k = convhull(layout_key.x, layout_key.y);
  plot(ax, layout_key.x(k), layout_key.y(k),'k--', 'LineWidth',1.7);
