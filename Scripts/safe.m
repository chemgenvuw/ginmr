%% Spatial Analysis of Functional Enrichment (SAFE)
%
% Spatial Analysis of Functional Enrichment (SAFE) is an automated network annotation algorithm. It performs local
% enrichment analysis on a biological network to determine which regions of
% the network are over-represented for functional attributes (binary annotations or quantitative phenotypes).
%
% Author: Anastasia Baryshnikova (2016), abarysh@princeton.edu,
% http://www.baryshnikova-lab.org
%
%% 1. Load settings
addpath(genpath('/home/chemgen/Desktop/Temp/Bede/safe'));
path_to_results_folder = '/home/chemgen/Desktop/Temp/Bede/safe/Out_put';
layout = initialize(path_to_results_folder);

 arvs =   '>';
 ators=   '+';
 btss =   '*';
 ceris=   'x';
 hmg1s=   'h';
 hmg2s=   'v';
 opis =   'o';
 shekl = ""
 lss = '-.';

%  hmg1 =   [0.9290    0.6940    0.1250];
%  arv=   [0.64, 0.08, 0.18];
%  hmg2 =   [0    0    0];
%  ceri=   [0    1.0000    0];
%  opi=   [0,1,1];
%  bts=   [1,1,0];
%  ator =   [0    0.4470    0.7410];
 arv =   [0.9290    0.6940    0.1250];
 ator=   [0.64, 0.08, 0.18];
 hmg1 =   [0    0    0];
 hmg2=   [0    0.700    0];
 ceri= [0    0.4470    0.7410]

rang = hmg2

%% 2. Load network

layout = load_network(layout, 'ImportEdges', 1, 'IsDirected', 0);

%% 3. Apply network layout, if necessary

if ~isempty(layout.layoutAlgorithm)
    layout = generate_network_map(layout);
end

%% 4. Plot network
% 
% if layout.plotNetwork
%     layout = plot_original_network(layout);
% end

%% 5. Translate node labels, if necessary
% The label_orf field is used for enrichment analysis. Multiple nodes can have the same label_orf.

inds = find(cellfun(@isempty, layout.label_orf));

if ~isempty(inds)
    layout.label_orf(inds) = genename2orf(layout.label(inds));  % Yeast ORFs only
    tmp = split_by_delimiter('_', layout.label_orf(inds));
    layout.label_orf(inds) = tmp(:,1);
end

%% 6. Load functional annotations

layout = load_annotation_file(layout);

%% 7. Calculate distances between node pairs

layout = calculate_node_distances(layout);
    
%% 8. Calculate enrichments

layout = compute_enrichment(layout);

%% 9. Plot sample enrichment landscapes

% if layout.plotSampleGroups
%     
%     % Plot 10 random groups
%     plot_sample_groups(layout)
%     
%     % Plot 1 random group
%     plot_sample_groups(layout,'Random',1);
%     
%     % To plot a specific set of groups, indicate their IDs
%     % plot_sample_groups(layout,'Range',[1:2]);  
%     
%     % OPTIONAL. Save the current figure as PDF.
%     export_figures(layout);
%     
% end
% 
% % OPTIONAL. Save all the enrichment scores to a text file.
% write_neighborhood_scores(layout);

%% 10. Combine functional attributes into domains

% Identify region-specific vs multi-regional functional attributes
layout = compute_unimodality(layout);

% Collapse together attributes that have similar enrichment landscapes.
layout = cluster_groups(layout);

% Eliminate the least represented colors & re-sort the rest 
layout = minimize_colors(layout);

%% 11. Generate automatic domain labels

layout = generate_automatic_region_labels(layout);

%% 12. Plot annotated network

% if strcmp('both', layout.annotationsign)
%     layout = plot_composite_map(layout, 'AnnotationSign', 'highest');
%     if layout.showLabels
%         plot_labels(layout, 'AnnotationSign', 'highest');
%     end
%     layout = plot_composite_map(layout, 'AnnotationSign', 'lowest');
%     if layout.showLabels
%         plot_labels(layout, 'AnnotationSign', 'lowest');
%     end
% else
%     layout = plot_composite_map(layout, 'AnnotationSign', layout.annotationsign);
%     if layout.showLabels
%         plot_labels(layout, 'AnnotationSign', layout.annotationsign);
%     end
% end

%% 13. Print results
% 
% print_output_files(layout);
% export_figures(layout);

% %% 14. Save SAFE session
% % 
% % layout = orderfields(layout);
% % 
% % save([layout.outputdir 'safe_session.mat'],'layout');
% 

%% Draw regions
sgn=1;
ucolors = unique(layout.groupColor(:,sgn));
if ucolors(1) ~= 1
    ucolors = [1; ucolors];
end
rng(3,'twister');
map_colors = colormap(lines((length(ucolors)-1)));
map_colors = map_colors(randperm(size(map_colors,1)),:);
map_colors = [1 1 1; map_colors]; 
layout.mapColors = map_colors;
layout.geneRGB = ones(length(layout.label),3);
layout.geneRGB = map_colors(layout.labelColor,:);
%[14,4, 10, 11, 13, 7 ,6,3,9,5] %2:length(ucolors)
for i = 2:length(ucolors)
    ax = gca;
     % inds = find(layout.labelColor(:,sgn) == ucolors(i));
    indsg = find(layout.groupColor(:,sgn) == ucolors(i));
    [r,~,~] = find(layout.opacity_01(:,indsg,sgn)>0);
    inds = unique(r);
    x = layout.x(inds);
    y = layout.y(inds);
    colors = layout.geneRGB(inds,:);
    
    xpass=  isoutlier(x);
    x = x(~xpass);
    y = y(~xpass);
    colors = colors(~xpass, :);
    
    ypass= isoutlier(y);
    y = y(~ypass);
    x = x(~ypass);
    colors = colors(~ypass, :);
    k=boundary(x , y, 0.4);
   
%     direction = datasample([1,-1],1);
%     a = 20;
%     b = 65;
%     r = (b-a).*rand(1,1) + a;
%     x = x + (direction *r);
    direction = datasample([1,-1],1);
    a = 50;
    b = 90;
    r = (b-a).*rand(1,1) + a;
    y = y + (direction*r);
%     scatterPoints = scatter(ax, x, y,75,'.','MarkerEdgeColor', rang,'MarkerEdgeAlpha',.7);
    scatterPoints = scatter(ax, x, y,95,'.','MarkerEdgeColor', rang,'MarkerEdgeAlpha',.75,...
        'jitter','on', 'jitterAmount',19);
%     patch(ax, x(k),y(k), rang , 'FaceAlpha', 0.1, 'EdgeColor', rang, "LineWidth", 1.35,...
%          'LineStyle', lss);
%      patch(ax, x(k),y(k), mode(colors) , 'FaceAlpha', 0.3, 'EdgeColor', [0 0 0], "LineWidth", 0.8,...
%         'LineStyle', '-.');
    
    centroid_x = sum((layout.labelOpacity(inds,sgn).^2) .* layout.x(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    centroid_y = sum((layout.labelOpacity(inds,sgn).^2) .* layout.y(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    sum((layout.labelOpacity(inds,sgn).^2) .* layout.y(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    hold on;     
    %hText{i} = text(centroid_x, centroid_y, sprintf('%d',ucolors(i)),'FontSize',10,'Color','k','HorizontalAlignment','center');
    set(ax,'DataAspectRatio',[1 1 1]);
    set(ax, 'YDir','reverse');
    axis off;
    hold on;
    
end
  k = convhull(layout.x, layout.y);
  plot(ax, layout.x(k), layout.y(k),'k--', 'LineWidth',1.7);

    %% Write Gene File 
    [filepath,name,ext] = fileparts(layout.annotationfile);
    OUTPUTfile2 = {'/home/chemgen/Desktop/Temp/Bede/safe/Out_put/', name, '-Gene.csv' };
    fid = fopen(strjoin(OUTPUTfile2),'w');
    fprintf(fid, '## \n');
    fprintf(fid, '## This file lists the properties of all nodes in the network.\n');
    fprintf(fid, '## \n\n');
    columns = {'Node label', 'Node label ORF', 'Domain (predominant)', 'Neighborhood score [max=1, min=0] (predominant)', 'Total number of enriched domains','Number of enriched attributes per domain'};
    columns_field = {'label', 'label_orf', 'labelColor', 'labelOpacity'};
    columns_format = {'%s', '%s', '%d', '%.3f'};
    
    for k = 1 : length(columns)
        if k > 1, fprintf(fid, '\t'); end
        fprintf(fid, '%s', columns{k});
    end
    fprintf(fid, '\n');
    
    for i = 1 : length(layout.label)
        for k = 1 : length(columns_field)
            if k > 1, fprintf(fid, '\t'); end
            if strcmp('%s', columns_format{k})
                item = layout.(columns_field{k}){i};
            else
                item = layout.(columns_field{k})(i,sgn);
            end
            fprintf(fid, columns_format{k}, item);
        end
        
        fprintf(fid, '\t%d', length(find(layout.cumOpacity01ByColor{sgn}(i,:)>0)));
        
        fprintf(fid, '\t');
        for k = 1 : length(layout.regionId{sgn})
            if k > 1, fprintf(fid, ','); end
            fprintf(fid, '%d', layout.cumOpacity01ByColor{sgn}(i,k));
        end
        fprintf(fid, '\n');
    end
    fclose(fid);

%% Save domain file     
  [filepath,name,ext] = fileparts(layout.annotationfile);
  OUTPUTfile = {'/home/chemgen/Desktop/Temp/Bede/safe/Out_put/', name, '.csv' };
  OUTPUTmatlab = {'/home/chemgen/Desktop/Temp/Bede/safe/Out_put/', name, '.mat' };
  strjoin(OUTPUTmatlab)
  OUTPUTfig = {'/home/chemgen/Desktop/Temp/Bede/safe/Out_put/', name, '.png' };
%   OUTPUTfigin = {'/home/chemgen/Desktop/Temp/safe/Results/Paul_result_fianl/', name, '.fig' };
  saveas(gcf,strjoin(OUTPUTfig));
%   savefig(strjoin(OUTPUTfigin))
  fid = fopen(strjoin(OUTPUTfile),'w');
  for i = 1 : length(ucolors)-1
    fprintf(fid, '%d\t%s\t%d\t%d\t%d\n', ucolors(i), layout.regionName{sgn}{i}, round(map_colors(i,1)*255), round(map_colors(i,2)*255), round(map_colors(i,3)*255));
  end
  fclose(fid);
save(strjoin(OUTPUTmatlab), 'layout');



