#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 11 13:43:40 2018

@author: chemgen

"""
import matplotlib.pyplot as plt
import seaborn as sns 
from sklearn.preprocessing import StandardScaler
import numpy as np
import matplotlib.ticker as ticker
import matplotlib.gridspec as gridspec

plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi=300)
gs = gridspec.GridSpec(4,5, left=None, bottom=None, right=None, top=None,
                wspace=None, hspace=0.5)
pad = 5

fig.suptitle("Distribution of Closeness Centrality Measures", fontsize=16)

ax0_0 = fig.add_subplot(gs[0,0:1])
ax0_0.annotate(r"Ator", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_0.annotate("S288C", xy=(0, 0.1), xytext=(-ax0_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax0_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax0_0 = sns.violinplot(x=plot_dic['S288C']["ATOR_closeness"],  bw=.02,
                       palette=['r'])

ax0_1 = fig.add_subplot(gs[0,1:2])
ax0_1.annotate(r"Ceri", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_1 = sns.violinplot(x=plot_dic['S288C']["CERI_closeness"],  bw=.02,
                       palette=['r'])

ax0_2 = fig.add_subplot(gs[0,2:3])
ax0_2.annotate(r'\textit{hmg1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_2 = sns.violinplot(x=plot_dic['S288C']["HMG1_closeness"],  bw=.02,
                       palette=['r'])

ax0_3 = fig.add_subplot(gs[0,3:4])
ax0_3.annotate(r'\textit{hmg2}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_3 = sns.violinplot(x=plot_dic['S288C']["HMG2_closeness"],  bw=.02,
                       palette=['r'])

ax0_4 = fig.add_subplot(gs[0,4:5])
ax0_4.annotate(r'\textit{arv1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_4 = sns.violinplot(x=plot_dic['S288C']["ARV1_closeness"],  bw=.02,
                       palette=['r'])
########
ax1_0 = fig.add_subplot(gs[1,0:1])
ax1_0.annotate("Y55", xy=(0, 0.1), xytext=(-ax1_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax1_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax1_0 = sns.violinplot(x=plot_dic['Y55']["ATOR_closeness"],  bw=.02,
                       palette=['r'])

ax1_1 = fig.add_subplot(gs[1,1:2])
ax1_1 = sns.violinplot(x=plot_dic['Y55']["CERI_closeness"],  bw=.02,
                       palette=['r'])

ax1_2 = fig.add_subplot(gs[1,2:3])
ax1_2 = sns.violinplot(x=plot_dic['Y55']["HMG1_closeness"],  bw=.02,
                       palette=['r'])

ax1_3 = fig.add_subplot(gs[1,3:4])
ax1_3 = sns.violinplot(x=plot_dic['Y55']["HMG2_closeness"],  bw=.02,
                       palette=['r'])

ax1_4 = fig.add_subplot(gs[1,4:5])
ax1_4 = sns.violinplot(x=plot_dic['Y55']["ARV1_closeness"],  bw=.02,
                       palette=['r'])
#######
ax2_0 = fig.add_subplot(gs[2,0:1])
ax2_0.annotate("UWOPS87", xy=(0, 0.1), xytext=(-ax2_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax2_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax2_0 = sns.violinplot(x=plot_dic['UWOPS87']["ATOR_closeness"],  bw=.02,
                       palette=['r'])


ax2_1 = fig.add_subplot(gs[2,1:2])
ax2_1 = sns.violinplot(x=plot_dic['UWOPS87']["CERI_closeness"],  bw=.02,
                       palette=['r'])

ax2_2 = fig.add_subplot(gs[2,2:3])
ax2_2 = sns.violinplot(x=plot_dic['UWOPS87']["HMG1_closeness"],  bw=.02,
                       palette=['r'])

ax2_3 = fig.add_subplot(gs[2,3:4])
ax2_3 = sns.violinplot(x=plot_dic['UWOPS87']["HMG2_closeness"],  bw=.02,
                       palette=['r'])

ax2_4 = fig.add_subplot(gs[2,4:5])
ax2_4 = sns.violinplot(x=plot_dic['UWOPS87']["ARV1_closeness"],  bw=.02,
                       palette=['r'])
########
ax3_0 = fig.add_subplot(gs[3,0:1])
ax3_0.annotate("YPS606", xy=(0, 0.1), xytext=(-ax3_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax3_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax3_0 = sns.violinplot(x=plot_dic['YPS606']["ATOR_closeness"],  bw=.02,
                       palette=['r'])

ax3_1 = fig.add_subplot(gs[3,1:2])
ax3_1 = sns.violinplot(x=plot_dic['YPS606']["CERI_closeness"],  bw=.02,
                       palette=['r'])

ax3_2 = fig.add_subplot(gs[3,2:3])
ax3_2 = sns.violinplot(x=plot_dic['YPS606']["HMG1_closeness"],  bw=.02,
                       palette=['r'])

ax3_3 = fig.add_subplot(gs[3,3:4])
ax3_3 = sns.violinplot(x=plot_dic['YPS606']["HMG2_closeness"],  bw=.02,
                       palette=['r'])

ax3_4 = fig.add_subplot(gs[3,4:5])
ax3_4 = sns.violinplot(x=plot_dic['YPS606']["ARV1_closeness"],  bw=.02,
                       palette=['r'])

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Supplementary_Figure4_Closeness.png", dpi=300,
            bbox_inches="tight" , transparent = False) 

#%%
############################
##############################
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi=300)
gs = gridspec.GridSpec(4,5, left=None, bottom=None, right=None, top=None,
                wspace=None, hspace=0.5)
pad = 5
fig.suptitle("Distribution of Betweenness Centrality Measures", fontsize=16)

ax0_0 = fig.add_subplot(gs[0,0:1])
ax0_0.annotate(r"Ator", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_0.annotate("S288C", xy=(0, 0.1), xytext=(-ax0_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax0_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax0_0= sns.violinplot(x=plot_dic['S288C']["ATOR_betwenness"],
                       palette=['b'])
ax0_0.set_xlim(0,25000)

ax0_1 = fig.add_subplot(gs[0,1:2])
ax0_1.annotate(r"Ceri", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_1 = sns.violinplot(x=plot_dic['S288C']["CERI_betwenness"],
                       palette=['b'])
ax0_1.set_xlim(0,25000)

ax0_2 = fig.add_subplot(gs[0,2:3])
ax0_2.annotate(r'\textit{hmg1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_2 = sns.violinplot(x=plot_dic['S288C']["HMG1_betwenness"],
                       palette=['b'])
ax0_2.set_xlim(0,25000)

ax0_3 = fig.add_subplot(gs[0,3:4])
ax0_3.annotate(r'\textit{hmg2}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_3 = sns.violinplot(x=plot_dic['S288C']["HMG2_betwenness"],
                       palette=['b'])
ax0_3.set_xlim(0,25000)

ax0_4 = fig.add_subplot(gs[0,4:5])
ax0_4.annotate(r'\textit{arv1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_4 = sns.violinplot(x=plot_dic['S288C']["ARV1_betwenness"],
                       palette=['b'])
ax0_4.set_xlim(0,25000)
########
ax1_0 = fig.add_subplot(gs[1,0:1])
ax1_0.annotate("Y55", xy=(0, 0.1), xytext=(-ax1_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax1_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax1_0 = sns.violinplot(x=plot_dic['Y55']["ATOR_betwenness"],
                       palette=['b'])
ax1_0.set_xlim(0,25000)

ax1_1 = fig.add_subplot(gs[1,1:2])
ax1_1 = sns.violinplot(x=plot_dic['Y55']["CERI_betwenness"],
                       palette=['b'])
ax1_1.set_xlim(0,25000)

ax1_2 = fig.add_subplot(gs[1,2:3])
ax1_2 = sns.violinplot(x=plot_dic['Y55']["HMG1_betwenness"],
                       palette=['b'])
ax1_2.set_xlim(0,25000)

ax1_3 = fig.add_subplot(gs[1,3:4])
ax1_3 = sns.violinplot(x=plot_dic['Y55']["HMG2_betwenness"],
                       palette=['b'])
ax1_3.set_xlim(0,25000)

ax1_4 = fig.add_subplot(gs[1,4:5])
ax1_4 = sns.violinplot(x=plot_dic['Y55']["ARV1_betwenness"],
                       palette=['b'])
ax1_4.set_xlim(0,25000)
#######
ax2_0 = fig.add_subplot(gs[2,0:1])
ax2_0.annotate("UWOPS87", xy=(0, 0.1), xytext=(-ax2_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax2_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax2_0 = sns.violinplot(x=plot_dic['UWOPS87']["ATOR_betwenness"],
                       palette=['b'])
ax2_0.set_xlim(0,25000)

ax2_1 = fig.add_subplot(gs[2,1:2])
ax2_1 = sns.violinplot(x=plot_dic['UWOPS87']["CERI_betwenness"],
                       palette=['b'])
ax2_1.set_xlim(0,25000)

ax2_2 = fig.add_subplot(gs[2,2:3])
ax2_2 = sns.violinplot(x=plot_dic['UWOPS87']["HMG1_betwenness"],
                       palette=['b'])
ax2_2.set_xlim(0,25000)

ax2_3 = fig.add_subplot(gs[2,3:4])
ax2_3 = sns.violinplot(x=plot_dic['UWOPS87']["HMG2_betwenness"],
                       palette=['b'])
ax2_3.set_xlim(0,25000)

ax2_4 = fig.add_subplot(gs[2,4:5])
ax2_4 = sns.violinplot(x=plot_dic['UWOPS87']["ARV1_betwenness"],
                       palette=['b'])
ax2_4.set_xlim(0,25000)
########
ax3_0 = fig.add_subplot(gs[3,0:1])
ax3_0.annotate("YPS606", xy=(0, 0.1), xytext=(-ax3_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax3_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax3_0 = sns.violinplot(x=plot_dic['YPS606']["ATOR_betwenness"],
                       palette=['b'])
ax3_0.set_xlim(0,25000)

ax3_1 = fig.add_subplot(gs[3,1:2])
ax3_1 = sns.violinplot(x=plot_dic['YPS606']["CERI_betwenness"],
                       palette=['b'])
ax3_1.set_xlim(0,25000)

ax3_2 = fig.add_subplot(gs[3,2:3])
ax3_2 = sns.violinplot(x=plot_dic['YPS606']["HMG1_betwenness"],
                       palette=['b'])
ax3_2.set_xlim(0,25000)

ax3_3 = fig.add_subplot(gs[3,3:4])
ax3_3 = sns.violinplot(x=plot_dic['YPS606']["HMG2_betwenness"],
                       palette=['b'])
ax3_3.set_xlim(0,25000)

ax3_4 = fig.add_subplot(gs[3,4:5])
ax3_4 = sns.violinplot(x=plot_dic['YPS606']["ARV1_betwenness"],
                       palette=['b'])
ax3_4.set_xlim(0,25000)

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Supplementary_Figure4_betwenness.png", dpi=300,
            bbox_inches="tight" , transparent = False) 

#%%%
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi=300)
gs = gridspec.GridSpec(4,5, left=None, bottom=None, right=None, top=None,
                wspace=0.35, hspace=0.5)
pad = 5
fig.suptitle("Distribution of Eigenvector Centrality Measures", fontsize=16)

ax0_0 = fig.add_subplot(gs[0,0:1])
ax0_0.annotate(r"Ator", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_0.annotate("S288C", xy=(0, 0.1), xytext=(-ax0_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax0_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax0_0= sns.violinplot(x=plot_dic['S288C']["ATOR_eigenvector"],  bw=.02,
                       palette=['g'])
ax0_0.set_xlim(0,0.1)

ax0_1 = fig.add_subplot(gs[0,1:2])
ax0_1.annotate(r"Ceri", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_1 = sns.violinplot(x=plot_dic['S288C']["CERI_eigenvector"], bw=.02,
                       palette=['g'])
ax0_1.set_xlim(0,0.1)

ax0_2 = fig.add_subplot(gs[0,2:3])
ax0_2.annotate(r'\textit{hmg1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_2 = sns.violinplot(x=plot_dic['S288C']["HMG1_eigenvector"], bw=.02,
                       palette=['g'])
ax0_2.set_xlim(0,0.1)

ax0_3 = fig.add_subplot(gs[0,3:4])
ax0_3.annotate(r'\textit{hmg2}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_3 = sns.violinplot(x=plot_dic['S288C']["HMG2_eigenvector"], bw=.02,
                       palette=['g'])
ax0_3.set_xlim(0,0.1)

ax0_4 = fig.add_subplot(gs[0,4:5])
ax0_4.annotate(r'\textit{arv1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_4 = sns.violinplot(x=plot_dic['S288C']["ARV1_eigenvector"], bw=.02,
                       palette=['g'])
ax0_4.set_xlim(0,0.1)
########
ax1_0 = fig.add_subplot(gs[1,0:1])
ax1_0.annotate("Y55", xy=(0, 0.1), xytext=(-ax1_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax1_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax1_0 = sns.violinplot(x=plot_dic['Y55']["ATOR_eigenvector"], bw=.02,
                       palette=['g'])
ax1_0.set_xlim(0,0.1)

ax1_1 = fig.add_subplot(gs[1,1:2])
ax1_1 = sns.violinplot(x=plot_dic['Y55']["CERI_eigenvector"], bw=.02,
                       palette=['g'])
ax1_1.set_xlim(0,0.1)

ax1_2 = fig.add_subplot(gs[1,2:3])
ax1_2 = sns.violinplot(x=plot_dic['Y55']["HMG1_eigenvector"], bw=.02,
                       palette=['g'])
ax1_2.set_xlim(0,0.1)

ax1_3 = fig.add_subplot(gs[1,3:4])
ax1_3 = sns.violinplot(x=plot_dic['Y55']["HMG2_eigenvector"], bw=.02,
                       palette=['g'])
ax1_3.set_xlim(0,0.1)

ax1_4 = fig.add_subplot(gs[1,4:5])
ax1_4 = sns.violinplot(x=plot_dic['Y55']["ARV1_eigenvector"], bw=.02,
                       palette=['g'])
ax1_4.set_xlim(0,0.1)
#######
ax2_0 = fig.add_subplot(gs[2,0:1])
ax2_0.annotate("UWOPS87", xy=(0, 0.1), xytext=(-ax2_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax2_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax2_0 = sns.violinplot(x=plot_dic['UWOPS87']["ATOR_eigenvector"], bw=.02,
                       palette=['g'])
ax2_0.set_xlim(0,0.1)

ax2_1 = fig.add_subplot(gs[2,1:2])
ax2_1 = sns.violinplot(x=plot_dic['UWOPS87']["CERI_eigenvector"], bw=.02,
                       palette=['g'])
ax2_1.set_xlim(0,0.1)

ax2_2 = fig.add_subplot(gs[2,2:3])
ax2_2 = sns.violinplot(x=plot_dic['UWOPS87']["HMG1_eigenvector"], bw=.02,
                       palette=['g'])
ax2_2.set_xlim(0,0.1)

ax2_3 = fig.add_subplot(gs[2,3:4])
ax2_3 = sns.violinplot(x=plot_dic['UWOPS87']["HMG2_eigenvector"], bw=.02,
                       palette=['g'])
ax2_3.set_xlim(0,0.1)

ax2_4 = fig.add_subplot(gs[2,4:5])
ax2_4 = sns.violinplot(x=plot_dic['UWOPS87']["ARV1_eigenvector"], bw=.02,
                       palette=['g'])
ax2_4.set_xlim(0,0.1)
########
ax3_0 = fig.add_subplot(gs[3,0:1])
ax3_0.annotate("YPS606", xy=(0, 0.1), xytext=(-ax3_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax3_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax3_0 = sns.violinplot(x=plot_dic['YPS606']["ATOR_eigenvector"], bw=.02,
                       palette=['g'])
ax3_0.set_xlim(0,0.1)

ax3_1 = fig.add_subplot(gs[3,1:2])
ax3_1 = sns.violinplot(x=plot_dic['YPS606']["CERI_eigenvector"], bw=.02,
                       palette=['g'])
ax3_1.set_xlim(0,0.1)

ax3_2 = fig.add_subplot(gs[3,2:3])
ax3_2 = sns.violinplot(x=plot_dic['YPS606']["HMG1_eigenvector"], bw=.02,
                       palette=['g'])
ax3_2.set_xlim(0,0.1)

ax3_3 = fig.add_subplot(gs[3,3:4])
ax3_3 = sns.violinplot(x=plot_dic['YPS606']["HMG2_eigenvector"], bw=.02,
                       palette=['g'])
ax3_3.set_xlim(0,0.1)

ax3_4 = fig.add_subplot(gs[3,4:5])
ax3_4 = sns.violinplot(x=plot_dic['YPS606']["ARV1_eigenvector"], bw=.02,
                       palette=['g'])
ax3_4.set_xlim(0,0.1)

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Supplementary_Figure4_eigenvector.png", dpi=300,
            bbox_inches="tight" , transparent = False) 