
"""
Created on Sat Dec 30 18:29:36 2017

@author: chemgen
"""
from lib import *
import argparse
import os


def main(seeds_filepath, f_tri_file, data_filename,  distance=-1):
    test=False
    print("making seed files")
    get_seed(seeds_filepath)
    print("Done \n \n \n")
    for root, dirs, files in os.walk(seeds_filepath):
        for file in files:
            if file.endswith(".csv"):
                print("One file found")
                seed_filename = os.path.join(root, file)
                print("l20 done")
                seed_file = read(seed_filename, False)
                print("l22 done")
                data_file = read(data_filename)
                print("l24 done")
                print(f_tri_file)
                f_tri = read(f_tri_file)
                print("l26 done")
                output_name= output_file_name(seed_filename)
                print("l28 done")
                check=os.path.join(root, "gene_topology_output_network_{}".format(output_name))
                print("l30 done")
                print("reading done")
                if os.path.isfile(check):
                    print("ignored")
                else:
                    print("Make list of seed")
                    seed = get_seed_list(seed_file)
                    write_network_file = os.path.join(root, "gene_topology_output_network_{}".format(output_name))
                    seeded_networks = set()
                    
                    if distance == -1:
                    
                        print("make list of networks")
                        networks = find_networks(data_file)
                        
                        print("network doesnt contain aseed")
                        seeded_networks = filter_by_seed(networks, seed)
                        
                        print("output")
                        test()
                        results = get_data_for_seeded_networks(seeded_networks,
                                                               data_file, test, root, output_name)
                        
                    else:
                        print("make list of networks to distance %s" % distance)
#                        seeded_networks = find_cropped_networks(data_file, seed, distance)
                        seeded_networks = find_cropped_networks_tri(data_file, seed, f_tri)
                
                        print("output")
                        results = get_data_for_seeded_networks_pairs(seeded_networks,
                                                                     data_file, test, root, output_name)
                    
                    #for NEtworkx
                    print("Constructing the Network")
                    make_network(write_network_file, root, output_name)
                    
                    """
                    print("Calculating topology")
                    computing_topology(G, output_name, seed_filename, root )
                    """
                    print("Constructing the Network(igraph")
                    g = make_network_igraph(write_network_file)
                    print("Calculating topology")
                    computing_topology_igraph(g, output_name, seed_filename, root )
                    
                    
                    print("Done ------------------------------------------- \n \n \n")

parser = argparse.ArgumentParser(description='Get list of arrgvs from comandline')
parser.add_argument("-s","--seeds_filepath", help="Seeds path",type=str)
parser.add_argument("-t","--tri_file", help="Tridgenic data base",type=str)
parser.add_argument("-da","--data_file", help="Costanzo database",type=str)
parser.add_argument("-di","--distance", help="Seeds path",type=int)




args = parser.parse_args()
main(args.seeds_filepath, args.tri_file, args.data_file,  args.distance)
    
