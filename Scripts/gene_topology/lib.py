"""
Created on Sat Dec 30 18:29:36 2017

@author: chemgen
"""
INDEX_G1 = 0
INDEX_G2 = 2
INDEX_SCORE = 5
INDEX_EXTRA_DATA = 6

import os 
from multiprocessing import Pool
import multiprocessing
import time
import pickle
import itertools
from numpy import linalg as LA
import networkx as nx
import pandas as pd
#import seaborn as sns
import re
import igraph

def get_seed(seeds_filepath):
    for root, dirs, files in os.walk(seeds_filepath):
        for file in files:
            if file.endswith(".xlsx"):
                file_towrite = re.sub("xlsx", "csv", "Seed_%s" % file) 
                file = os.path.join(root, file)
                file_towrite = os.path.join(root, file_towrite)
                print(file)
                print(file_towrite)
                df_file = pd.ExcelFile(file)
                df_parsed = df_file.parse('Sheet1')
                ORF_seed = df_parsed['Systematic Name']
                ORF_seed.to_csv(file_towrite, sep='\t' ,
                                header=False, index=False, index_label=None)

def output_file_name(input_file):
    return input_file.split(".")[0].split(os.sep)[-1]
     
def read(filename, skip_header=True):
    f = open(filename)

    if skip_header:
        f.readline()

    return f


def get_seed_list(seed_file):
    print("loading the seed list")
    seed = []

    for line in seed_file:
        seed.append(line.strip())

    return seed




def significant_p(pvalue):
    return float(pvalue) < 0.05


def find_networks(f):
    # initialise empty set of sets
    # each set represents a connected network
    networks = []

    for line in f:
        list_line = line.split("\t")
        g1 = list_line[INDEX_G1].split("_")[0]
        g2 = list_line[INDEX_G2].split("_")[0]
        score = list_line[INDEX_SCORE]
        p = list_line[INDEX_EXTRA_DATA]

        # if the nodes are connected
        if relevant_score(score) and significant_p(p):

            g1_index = -1
            g2_index = -1
            g1_network = []
            g2_network = []
            for index, network in enumerate(networks):

                if g1 in network:
                    g1_index = index
                    g1_network = network
                if g2 in network:
                    g2_index = index
                    g2_network = network

                if g1_index != -1 and g2_index != -1:
                    break

            # if new network
            if g1_index == -1 and g2_index == -1:
                networks.append({g1, g2})

            elif g1_index != -1 and g2_index != -1:
                # if 2 different current networks, union those networks
                if g1_index != g2_index:
                    networks[g1_index].update(networks[g2_index])
                    del networks[g2_index]
                # otherwise ignore - they are already recorded

            # if 1 current network
            elif g1_index != -1:
                g1_network.add(g2)
            elif g2_index != -1:
                g2_network.add(g1)
            else:
                raise Exception()

    return networks


def gene_pair_symbol(g1, g2):
    return "%s+%s" % (g1, g2)

def relevant_score(score):
    return float(score) > 0.16 or float(score) < - 0.12

def find_cropped_networks(f, seed, distance_from_seed):
    gene_pairs = set()
    seeds = set(seed)
    adjacent_to_seed = set()

    for counter in range(distance_from_seed):
        print("counter: %s" % counter)
        for line in f:
            list_line = line.split("\t")
            g1 = list_line[INDEX_G1].split("_")[0]
            g2 = list_line[INDEX_G2].split("_")[0]
            score = list_line[INDEX_SCORE]
            p = list_line[INDEX_EXTRA_DATA]
            
            if relevant_score(score) and significant_p(p):
                if g1 in seeds or g2 in seeds:
                    gene_pairs.add(gene_pair_symbol(g1, g2))
                    if g1 in seeds:
                        adjacent_to_seed.add(g2)
                    else:
                        adjacent_to_seed.add(g1)
        seeds.update(adjacent_to_seed)
        f.seek(0)
        f.readline()
    print(len(gene_pairs))
    return gene_pairs

def find_cropped_networks_tri(f, seed, f_tri):
    gene_pairs = set()
    double_gene = set()
    seeds = set(seed)
    adjacent_to_seed = set()

    print("Checking in Costazo database")
    for line in f:
        list_line = line.split("\t")
        g1 = list_line[INDEX_G1].split("_")[0]
        g2 = list_line[INDEX_G2].split("_")[0]
        score = list_line[INDEX_SCORE]
        p = list_line[INDEX_EXTRA_DATA]
        if relevant_score(score) and significant_p(p):
            if g1 in seeds or g2 in seeds:
                gene_pairs.add(gene_pair_symbol(g1, g2))
                double_gene.add(g2)
                double_gene.add(g1)                

    print("checking in trio database")
    for line in f_tri:
         list_line = line.split("\t")
         trigene_link = list_line[2].split("_")[0]
         trigene = list_line[0].split("_")[0]
         if trigene_link in double_gene :
             gene_pairs.add(gene_pair_symbol(trigene.split("+")[0],trigene_link))
             gene_pairs.add(gene_pair_symbol(trigene.split("+")[0], trigene.split("+")[1]))
    print(len(gene_pairs))
    return gene_pairs   


def filter_by_seed(networks, seed):
    seeded_networks = []
    for network in networks:
        # if there is a seed in the network
        if len(network.intersection(seed)) > 0:
            seeded_networks.append(network)
        # otherwise, ignore it

    return seeded_networks


def get_data_for_seeded_networks(seeded_networks, f, test, root, output_name):
    print()
    f.seek(0)
    f.readline()  # skip header
    results = []

    # open the output files for writing
    """
    with open("output/gene_topology_output_network_{}".format("temp"), "w") as out_files :
        out_files.write("Query allele name" + "\t" + "Array allele name" + "\t" +
                        "Genetic interaction score (Îµ)" + "\t" +	
                        "P-value	Query single mutant fitness (SMF)" + "\t" +
                        "Array SMF" + "\t" +	"Double mutant fitness" + "\t" +
                        "Double mutant fitness" + "\t" + "standard deviation" + "\n" )
        """
    
    out_files = [open(os.path.join(root, "gene_topology_output_network_{}".format(output_name))
    + str(index), "w")
             for index in range(len(seeded_networks))]

    print(seeded_networks)

    for line in f:
        list_line = line.split("\t")
        g1 = list_line[INDEX_G1].split("_")[0]
        g2 = list_line[INDEX_G2].split("_")[0]
        print("g1 " + g1 + " g2 " + g2)
        score = list_line[INDEX_SCORE]
        extra = list_line[INDEX_EXTRA_DATA:]

        if relevant_score(score):
            for index, seeded_network in enumerate(seeded_networks):
                if g1 in seeded_network and g2 in seeded_network:
                    row = [g1, g2, score]
                    row.extend(extra)
                    results.append(row)

                    if not test:
                        out_files[index].write("\t".join(row) + "\n")

                    break

    for out_file in out_files:
        out_file.close()


# todo sort out this vs above
def get_data_for_seeded_networks_pairs(seeded_networks, f, test, root, output_name):
    f.seek(0)
    f.readline()  # skip header
    results = []

    # open the output files for writing
    out_file = open(os.path.join(root, "gene_topology_output_network_{}".format(output_name)), "w")

    for line in f:
        list_line = line.split("\t")
        g1 = list_line[INDEX_G1].split("_")[0]
        g2 = list_line[INDEX_G2].split("_")[0]
        score = list_line[INDEX_SCORE]
        extra = list_line[INDEX_EXTRA_DATA:]
        p = list_line[INDEX_EXTRA_DATA]

        if relevant_score(score) and significant_p(p):
            if gene_pair_symbol(g1, g2) in seeded_networks:
            #if g1 in seeded_networks and g2 in seeded_networks:
                row = [g1, g2, score]
                row.extend(extra)
                results.append(row)

                if not test:
                    write_row = "\t".join(row)
                    out_file.write(write_row)

    out_file.close()

    return results

def make_network(network_file, root, output_name):
    network_read = pd.read_csv(network_file , sep = "\t")
    networkx_read = {'Query_allele_name':network_read.iloc[:,0],
                    'Array_allele_name':network_read.iloc[:,1],
                    'Genetic_interaction_score':network_read.iloc[:,3]}
    networkx_costanzo_df = pd.DataFrame(networkx_read)
    del networkx_read
    del network_read
    G = nx.from_pandas_edgelist(networkx_costanzo_df, 'Query_allele_name',
                            'Array_allele_name', ['Genetic_interaction_score'])
    out_file = os.path.join(root, "Gaph_{}.edgelist".format(output_name))
    nx.write_edgelist(G, out_file)
    return G

def chunks(l, n):
    """Divide a list of nodes `l` in `n` chunks"""
    l_c = iter(l)
    while 1:
        x = tuple(itertools.islice(l_c, n))
        if not x:
            return
        yield x
        
def _betmap(G_normalized_weight_sources_tuple):
    """Pool for multiprocess only accepts functions with one argument.
    This function uses a tuple as its only argument. We use a named tuple for
    python 3 compatibility, and then unpack it when we send it to
    `betweenness_centrality_source`
    """
    return nx.betweenness_centrality(*G_normalized_weight_sources_tuple)

def betweenness_centrality_parallel(G, processes=None):
    """Parallel betweenness centrality  function"""
    p = Pool(processes=processes)
    node_divisor = len(p._pool) * multiprocessing.cpu_count()
    if G.order() / node_divisor < 1:
         node_divisor = 2
         node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    else:   
        node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    num_chunks = len(node_chunks)
    bt_sc = p.map(_betmap,
                  zip([G] * num_chunks,
                      [None] * num_chunks, #k
                      [True] * num_chunks, #normalized
                      ['weight'] * num_chunks, #weight
                      [False] * num_chunks, #endpoints
                      [None] * num_chunks #seed
                      ))
    # Reduce the partial solutions
    bt_c = bt_sc[0]
    for bt in bt_sc[1:]:
        for n in bt:
            bt_c[n] += bt[n]
    return bt_c


def _betmap_closeness_centrality(G_normalized_weight_sources_tuple):
    return nx.closeness_centrality(*G_normalized_weight_sources_tuple)

def closeness_centrality_parallel(G, processes=None):
    """Parallel betweenness centrality  function"""
    p = Pool(processes=processes)
    node_divisor = len(p._pool) * multiprocessing.cpu_count()
    if G.order() / node_divisor < 1:
         node_divisor = 2
         node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    else:   
        node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    num_chunks = len(node_chunks)
    bt_sc = p.map(_betmap_closeness_centrality,
                  zip([G] * num_chunks,
                      ))
    bt_c = bt_sc[0]
    for bt in bt_sc[1:]:
        for n in bt:
            bt_c[n] += bt[n]
    return bt_c
    
def eigenvector_centrality(G):
    adjacancy_matrix = nx.to_numpy_matrix(G)
    enigvaleus = LA.eigvals(adjacancy_matrix)
    return enigvaleus  

def computing_topology(G, file_name, seed_file, root):
    print("Computing betweenness_parallel for:")
    print(seed_file)
    print(nx.info(G))
    print("\tParallel version")
    start = time.time()
    bt = betweenness_centrality_parallel(G)
    print("\t\tTime: %.4F" % (time.time() - start))
    bt_list=list(bt.values())
    with open(os.path.join(root,"{}_betwenness.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt_list, fp)
    

    print("Computing closeness_parallel for:")
    print(seed_file)
    print(nx.info(G))
    print("\tParallel version")
    start = time.time()
    bt = closeness_centrality_parallel(G)
    print("\t\tTime: %.4F" % (time.time() - start))
    bt_list=list(bt.values())
    with open(os.path.join(root,"{}_closeness.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt_list, fp)

    print("Computing eigenvector for:")
    print(seed_file)
    print(nx.info(G))
    start = time.time()
    eigenvector= eigenvector_centrality(G)
    print("\t\tTime: %.4F" % (time.time() - start))
    with open(os.path.join(root,"{}_eigenvector.list".format(file_name)), "wb") as fp: 
        pickle.dump(eigenvector, fp)  

def make_network_igraph(network_file):
    network_read = pd.read_csv(network_file , header=None, sep = "\t")
    networkx_read = {'Query_allele_name':network_read.iloc[:,0],
                    'Array_allele_name':network_read.iloc[:,1],
                    'Genetic_interaction_score':network_read.iloc[:,3]}
    networkx_costanzo_df = pd.DataFrame(networkx_read)
    temp_file_add= "{}_temp".format(network_file)
    networkx_costanzo_df.to_csv(temp_file_add, sep=' ',header=False, index=False) 
    g = igraph.Graph()
    g = igraph.Graph.Read_Ncol(temp_file_add, directed=False)
    edges_no_repeat = {}
    for e in g.es:
        if "{};{}".format(e.tuple[0],e.tuple[1]) in edges_no_repeat.keys():
            edges_no_repeat["{};{}".format(e.tuple[0],e.tuple[1])]= edges_no_repeat["{};{}".format(e.tuple[0],e.tuple[1])] +1
        else :
             edges_no_repeat["{};{}".format(e.tuple[0],e.tuple[1])] = 0
            
    for key, value in edges_no_repeat.items():
        if value > 0:
            for i in range(value):
                try:
                   #print(key[0] + " ---> " + key[2])
                   g.delete_edges([(int(key[0]),int(key[2]))])
                except ValueError:
                   pass
        """
        Print Functions
        for v in g.vs:
            print(v)
        
        for e in g.es:
            print(e.tuple)
        """    
    return g

def computing_topology_igraph(g, file_name, seed_file, root):
    group = str(file_name).split("_")[-1]
    print(group)
    print(str(file_name))
    print("Computing betweenness(igraph) for:")
    print(seed_file)
    start = time.time()
    bt = g.betweenness()
    print("\t\tTime: %.4F" % (time.time() - start))
    with open(os.path.join(root, group,"{}_betwenness.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt, fp)
    
    print("Computing closeness_parallel(igraph) for:")
    print(seed_file)
    start = time.time()
    bt = g.closeness()
    print("\t\tTime: %.4F" % (time.time() - start))
    with open(os.path.join(root, group, "{}_closeness.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt, fp)
        
    print("Computing eigenvector(igraph) for:")
    print(seed_file)
    start = time.time()
    bt = g.evcent()
    print("\t\tTime: %.4F" % (time.time() - start))
    with open(os.path.join(root, group, "{}_eigenvector.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt, fp)
     
    name = []
    for v in g.vs: 
        name.append(v["name"])
    with open(os.path.join(root, group,"{}_name.list".format(file_name)), "wb") as fp: 
        pickle.dump(name, fp)
             
        
    
    
