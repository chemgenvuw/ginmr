#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 16 13:10:03 2018

@author: chemgen
"""

import os 
from multiprocessing import Pool
import multiprocessing
import time
import pickle
import itertools
from numpy import linalg as LA
import networkx as nx
import pandas as pd
import seaborn as sns


Costanzo_network_path = "/home/chemgen/Desktop/Temp/Network/gene_topology/output/gene_topology_output_network_Ator"
output_path = "/home/chemgen/Desktop/Temp/Network"

def chunks(l, n):
    """Divide a list of nodes `l` in `n` chunks"""
    l_c = iter(l)
    while 1:
        x = tuple(itertools.islice(l_c, n))
        if not x:
            return
        yield x
        
def _betmap(G_normalized_weight_sources_tuple):
    """Pool for multiprocess only accepts functions with one argument.
    This function uses a tuple as its only argument. We use a named tuple for
    python 3 compatibility, and then unpack it when we send it to
    `betweenness_centrality_source`
    """
    return nx.betweenness_centrality(*G_normalized_weight_sources_tuple)

def betweenness_centrality_parallel(G, processes=None):
    """Parallel betweenness centrality  function"""
    p = Pool(processes=processes)
    node_divisor = len(p._pool) * multiprocessing.cpu_count()
    if G.order() / node_divisor < 1:
         node_divisor = 2
         node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    else:   
        node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    num_chunks = len(node_chunks)
    bt_sc = p.map(_betmap,
                  zip([G] * num_chunks,
                      [None] * num_chunks, #k
                      [True] * num_chunks, #normalized
                      ['weight'] * num_chunks, #weight
                      [False] * num_chunks, #endpoints
                      [None] * num_chunks #seed
                      ))
    # Reduce the partial solutions
    bt_c = bt_sc[0]
    for bt in bt_sc[1:]:
        for n in bt:
            bt_c[n] += bt[n]
    return bt_c


def _betmap_closeness_centrality(G_normalized_weight_sources_tuple):
    return nx.closeness_centrality(*G_normalized_weight_sources_tuple)

def closeness_centrality_parallel(G, processes=None):
    """Parallel betweenness centrality  function"""
    p = Pool(processes=processes)
    node_divisor = len(p._pool) * multiprocessing.cpu_count()
    if G.order() / node_divisor < 1:
         node_divisor = 2
         node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    else:   
        node_chunks = list(chunks(G.nodes(), int(G.order() / node_divisor)))
    num_chunks = len(node_chunks)
    bt_sc = p.map(_betmap_closeness_centrality,
                  zip([G] * num_chunks,
                      ))
    bt_c = bt_sc[0]
    for bt in bt_sc[1:]:
        for n in bt:
            bt_c[n] += bt[n]
    return bt_c
    
def eigenvector_centrality(G):
    adjacancy_matrix = nx.to_numpy_matrix(G)
    enigvaleus = LA.eigvals(adjacancy_matrix)
    return enigvaleus

def make_network(network_file):
    network_read = pd.read_csv(network_file , sep = "\t")
    networkx_read = {'Query_allele_name':network_read['Query allele name'],
                    'Array_allele_name':network_read['Array allele name'],
                    'Genetic_interaction_score':network_read["Genetic interaction score (ε)"]}
    networkx_costanzo_df = pd.DataFrame(networkx_read)
    del networkx_read
    del network_read
    G = nx.from_pandas_edgelist(networkx_costanzo_df, 'Query_allele_name',
                            'Array_allele_name', ['Genetic_interaction_score'])
    return G
    
#fix the file name from network lib
def computing_topology(G, file_name):
    print("Computing betweenness_centrality_parallel for:")
    print(nx.info(G))
    print("\tParallel version")
    start = time.time()
    bt = betweenness_centrality_parallel(G)
    print("\t\tTime: %.4F" % (time.time() - start))
    bt_list_ator=list(bt.values())
    with open(os.path.join(output_path, "{}_betwenness_centrality.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt_list_ator, fp)
    
    print("Computing betweenness_centrality_parallel for:")
    print(nx.info(G))
    print("\tParallel version")
    start = time.time()
    bt = betweenness_centrality_parallel(G)
    print("\t\tTime: %.4F" % (time.time() - start))
    bt_list_ator=list(bt.values())
    with open(os.path.join(output_path, "{}_betwenness_centrality.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt_list_ator, fp)

    print("Computing closeness_centrality_parallel for:")
    print("\tParallel version")
    start = time.time()
    bt = closeness_centrality_parallel(G)
    print("\t\tTime: %.4F" % (time.time() - start))
    bt_list=list(bt.values())
    with open(os.path.join(output_path, "{}_closeness_centrality.list".format(file_name)), "wb") as fp: 
        pickle.dump(bt_list, fp)

    eigenvector= eigenvector_centrality(G)
    with open(os.path.join(output_path, "{}_eigenvector_centrality.list".format(file_name)), "wb") as fp: 
        pickle.dump(eigenvector, fp)


p1=sns.kdeplot(bt_list, shade=True, color="r")
p1=sns.kdeplot(bt_list_ator, shade=True, color="b")

