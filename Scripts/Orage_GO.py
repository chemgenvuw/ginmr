#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 16:46:33 2018

@author: chemgen
"""
from orangecontrib.bio import go
ontology = go.Ontology()
annotations = go.Annotations("sgd", ontology=ontology)
ontology.set_slims_subset("goslim_yeast")


res = annotations.get_enriched_terms(["YGR270W", "YIL075C", "YDL007W"],
                                     slims_only=True)

print("Enriched slim terms:")
for go_id, (genes, p_value, _) in res.items():
    if p_value < 0.05:
        print(ontology[go_id].name + " with p-value: %.4f " % p_value + ", ".join(genes))
        
ontology.extract_super_graph('GO:0051603') # get parents of a term
ontology.slims_for_term('GO:0051603')
ontology["GO:0016740"]
