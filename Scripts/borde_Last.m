
sgn=1;
ucolors = unique(layout.groupColor(:,sgn));
if ucolors(1) ~= 1
    ucolors = [1; ucolors];
end
rng(3,'twister');
map_colors = colormap(lines((1000*length(ucolors)-1)));
map_colors = map_colors(randperm(size(map_colors,1)),:);
map_colors = [1 1 1; map_colors]; 
layout.mapColors = map_colors;
layout.geneRGB = ones(length(layout.label),3);
layout.geneRGB = map_colors(layout.labelColor,:);
%[14,4, 10, 11, 13, 7 ,6,3,9,5] %2:length(ucolors)
for i = 2:length(ucolors)
    ax = gca;
     % inds = find(layout.labelColor(:,sgn) == ucolors(i));
    indsg = find(layout.groupColor(:,sgn) == ucolors(i));
    [r,~,~] = find(layout.opacity_01(:,indsg,sgn)>0);
    inds = unique(r);
    x = layout.x(inds);
    y = layout.y(inds);
    colors = layout.geneRGB(inds,:);
    
    xpass=  isoutlier(x);
    x = x(~xpass);
    y = y(~xpass);
    colors = colors(~xpass, :);
    
    ypass= isoutlier(y);
    y = y(~ypass);
    x = x(~ypass);
    colors = colors(~ypass, :);
    
    k=boundary(x , y, 0.2);
% 
%     scatterPoints = scatter(ax, layout.x(inds), layout.y(inds), 10,...
%         layout.geneRGB(inds,:),'filled',...
%         'MarkerEdgeColor',[0 0 0],...
%         'MarkerFaceAlpha',.3,'MarkerEdgeAlpha',.1);
    patch(ax, x(k),y(k), mode(colors), 'FaceAlpha', 0.3, 'EdgeColor', [0 0 0], "LineWidth", 1,...
        'LineStyle', '-.', 'LineWidth', 0.85);
    centroid_x = sum((layout.labelOpacity(inds,sgn).^2) .* layout.x(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    centroid_y = sum((layout.labelOpacity(inds,sgn).^2) .* layout.y(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    sum((layout.labelOpacity(inds,sgn).^2) .* layout.y(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    hold on;
     hText{i} = text(centroid_x, centroid_y, sprintf('%d',ucolors(i)),'FontSize',10,'Color','k','HorizontalAlignment','center');
    set(ax,'DataAspectRatio',[1 1 1]);
    set(ax, 'YDir','reverse');
    axis off;
    hold on;
    
end
  k = convhull(layout.x, layout.y);
  plot(ax, layout.x(k), layout.y(k),'k--', 'LineWidth',2);
 
  [filepath,name,ext] = fileparts(layout.annotationfile);
  OUTPUTfile = {'/home/chemgen/Desktop/Temp/safe/Results/', name, '.csv' };
  OUTPUTfig = {'/home/chemgen/Desktop/Temp/safe/Results/', name, '.png' };
  saveas(gcf,strjoin(OUTPUTfig));
  fid = fopen(strjoin(OUTPUTfile),'w');
  for i = 1 : length(ucolors)
    fprintf(fid, '%d\t%s\t%d\t%d\t%d\n', ucolors(i), layout.regionName{sgn}{i}, round(map_colors(i,1)*255), round(map_colors(i,2)*255), round(map_colors(i,3)*255));
  end
  fclose(fid);
 

  

  
