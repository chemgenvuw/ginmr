import networkx as nx
import matplotlib as plt
import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import patches
import community
from collections import defaultdict
import seaborn as sns
import matplotlib.gridspec as gridspec
import os
import pandas as pd


color = ["#9b59b6","#ffff00", "#3498db", "#e74c3c", "#2ecc71",
          "#ff9933", "#996633", "#00ff00","#0000ff"]
          
          
def draw_adjacency_matrix(G, node_order, partitions, colors, title, path, ax):
    
    adjacency_matrix = nx.to_numpy_matrix(G, dtype=np.bool, nodelist=node_order)
    #fig = plt.figure(figsize=(2.5,2.5), dpi =500)# in inches
#    sns.set(font_scale=1)
#    from matplotlib import rc
#    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#    rc('text', usetex=True)
    plt.style.use('seaborn-paper')    
    #ax.title.set_text(title )
    ax.spy(adjacency_matrix, origin = "lower", marker="o",
            markersize=0.3, alpha = 0.6, c = "k")
    ax.axis('off')
    assert len(partitions) == len(colors)
    #ax_ = ax.gca()
    cnt = 0
    current_idx = 0
    for partition, color in zip(list(partitions), colors): 
            ax.add_patch(patches.Rectangle((current_idx, current_idx),
                                          len(partitions[cnt]), # Width
                                          len(partitions[cnt]), # Height
                                          facecolor=color,
                                          edgecolor="none"))
            current_idx += len(partitions[cnt])
            cnt += 1
    plt.savefig(path, dpi=500,
            bbox_inches = 'tight', transparent = True)

def cluster_network (edge_list, color):
    G = nx.read_edgelist(edge_list)
    louvain_community_dict = community.best_partition(G)
    louvain_comms = defaultdict(list)
    for node_index, comm_id in louvain_community_dict.items():
        louvain_comms[comm_id].append(node_index)
    louvain_comms_v = louvain_comms.values()
    nodes_louvain_ordered = [node for comm in louvain_comms_v for node in comm]
    N = len(louvain_comms)
    color = sns.color_palette(color, N)
    return G, nodes_louvain_ordered, louvain_comms, color
                

def make_figure(edge_list, color, title, path, ax ):
    G, nodes_louvain_ordered, louvain_comms, color = cluster_network(edge_list, color)
    draw_adjacency_matrix(G, nodes_louvain_ordered, louvain_comms, color,  title, path, ax)
    


plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi=1000)
gs = gridspec.GridSpec(4,5, left=None, bottom=None, right=None, top=None,
                wspace=None, hspace=None)

pad = 5
ax0_0 = fig.add_subplot(gs[0,0:1])
#ax0_0.set_title(r'Ator')
ax0_0.annotate(r"Ator", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_0.annotate("S288C", xy=(0, 0.1), xytext=(-ax0_0.yaxis.labelpad + (3.5 *pad), 0),
                xycoords=ax0_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ATOR_S288C.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ATOR_S288C.png"
title =  r'$ATOR$'
make_figure(edge_list, color, title, path, ax0_0)


ax0_1 = fig.add_subplot(gs[0,1:2])
#ax0_1.set_title(r'Ceri')
ax0_1.annotate(r"Ceri", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_CERI_S288C.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/CERI_S288C.png"
title =  r'$CERI$'
make_figure(edge_list, color, title, path, ax0_1)

ax0_2 = fig.add_subplot(gs[0,2:3])
#ax0_2.set_title(r'\textit{hmg1}-$Delta$')
ax0_2.annotate(r'\textit{hmg1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG1_S288C.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG1_S288C.png"
title =  r'$HMG1$'
make_figure(edge_list, color, title, path, ax0_2)

ax0_3 = fig.add_subplot(gs[0,3:4])
#ax0_3.set_title(r'\textit{hmg2}-$Delta$')
ax0_3.annotate(r'\textit{hmg2}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG2_S288C.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG2_S288C.png"
title =  r'$HMG2$'
make_figure(edge_list, color, title, path, ax0_3)

ax0_4 = fig.add_subplot(gs[0,4:5])
ax0_4.annotate(r'\textit{arv1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
#ax0_4.set_title(r'\textit{arv1}-$Delta$')
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ARV1_S288C.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ARV1_S288C.png"
title =  r'$ARV1$'
make_figure(edge_list, color, title, path, ax0_4)

########

ax1_0 = fig.add_subplot(gs[1,0:1])
ax1_0.annotate("Y55", xy=(0, 0.1), xytext=(-ax1_0.yaxis.labelpad + (3.5 *pad), 0),
                xycoords=ax1_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ATOR_Y55.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ATOR_Y55.png"

title =  r'$ATOR$'
make_figure(edge_list, color, title, path, ax1_0)

ax1_1 = fig.add_subplot(gs[1,1:2])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_CERI_Y55.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/CERI_Y55.png"
title =  r'$CERI$'
make_figure(edge_list, color, title, path, ax1_1)

ax1_2 = fig.add_subplot(gs[1,2:3])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG1_Y55.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG1_Y55.png"
title =  r'$HMG1$'
make_figure(edge_list, color, title, path, ax1_2)

ax1_3 = fig.add_subplot(gs[1,3:4])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG2_Y55.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG2_Y55.png"
title =  r'$HMG2$'
make_figure(edge_list, color, title, path, ax1_3)

ax1_4 = fig.add_subplot(gs[1,4:5])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ARV1_Y55.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ARV1_Y55.png"
title =  r'$ARV1$'
make_figure(edge_list, color, title, path, ax1_4)

#######

ax2_0 = fig.add_subplot(gs[2,0:1])
ax2_0.annotate("UWOPS87", xy=(0, 0.1), xytext=(-ax2_0.yaxis.labelpad + (3.5 *pad), 0),
                xycoords=ax2_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ATOR_UWOPS87.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ATOR_UWOPS87.png"
title =  r'$ATOR$'
make_figure(edge_list, color, title, path, ax2_0)

ax2_1 = fig.add_subplot(gs[2,1:2])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_CERI_UWOPS87.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/CERI_UWOPS87.png"
title =  r'$CERI$'
make_figure(edge_list, color, title, path, ax2_1)

ax2_2 = fig.add_subplot(gs[2,2:3])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG1_UWOPS87.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG1_UWOPS87.png"
title =  r'$HMG1$'
make_figure(edge_list, color, title, path, ax2_2)

ax2_3 = fig.add_subplot(gs[2,3:4])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG2_UWOPS87.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG2_UWOPS87.png"
title =  r'$HMG2$'
make_figure(edge_list, color, title, path, ax2_3)

ax2_4 = fig.add_subplot(gs[2,4:5])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ARV1_UWOPS87.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ARV1_UWOPS87.png"
title =  r'$ARV1$'
make_figure(edge_list, color, title, path, ax2_4)


########

ax3_0 = fig.add_subplot(gs[3,0:1])
ax3_0.annotate("YPS606", xy=(0, 0.1), xytext=(-ax3_0.yaxis.labelpad + (3.5 *pad), 0),
                xycoords=ax3_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ATOR_YPS606.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ATOR_YPS606.png"
title =  r'$ATOR$'
make_figure(edge_list, color, title, path, ax3_0)

ax3_1 = fig.add_subplot(gs[3,1:2])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_CERI_YPS606.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/CERI_YPS606.png"
title =  r'$CERI$'
make_figure(edge_list, color, title, path, ax3_1)

ax3_2 = fig.add_subplot(gs[3,2:3])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG1_YPS606.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG1_YPS606.png"
title =  r'$HMG1$'
make_figure(edge_list, color, title, path, ax3_2)

ax3_3 = fig.add_subplot(gs[3,3:4])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_HMG2_YPS606.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/HMG2_YPS606.png"
title =  r'$HMG2$'
make_figure(edge_list, color, title, path, ax3_3)

ax3_4 = fig.add_subplot(gs[3,4:5])
edge_list = "/home/chemgen/Desktop/Temp/Bede/Topology/Gaph_Seed_ARV1_YPS606.edgelist"
path = "/home/chemgen/Desktop/Temp/Bede/Topology/ARV1_YPS606.png"
title =  r'$ARV1$'
make_figure(edge_list, color, title, path, ax3_4)

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Figure4.png", dpi=1000,
            bbox_inches="tight" , transparent = False)  


#%%% make the Supp File
path = "/home/chemgen/Desktop/Temp/Bede/Topology"
exel_file = "/home/chemgen/Desktop/Temp/Final_Figures/Figure_4_sup_Gene_Community.xlsx"
writer = pd.ExcelWriter(exel_file, engine = 'xlsxwriter')
for root, dirs, files in os.walk(path):
    for file in files:
        if file.endswith(".edgelist"):
            name= str(file).split(".")[0]
            name= name.split("_")[2:4]
            name = "_".join(name)
            file_with_path =  os.path.join(root,file)
            G = nx.read_edgelist( file_with_path)
            louvain_community_dict = community.best_partition(G)
            louvain_comms = {}
            for node_index, comm_id in louvain_community_dict.items():
                if comm_id in louvain_comms.keys():
                    louvain_comms[comm_id].append(node_index)
                else:
                    louvain_comms[comm_id]= [node_index]
            for key in louvain_comms.keys():
                anootated = id_concvertor(louvain_comms[key])
                louvain_comms[key] = anootated
            df = pd.DataFrame.from_dict(louvain_comms, orient='index')
            df= df.transpose()
            df.to_excel(writer, sheet_name=name, index= False)
writer.save()

path = "/home/chemgen/Desktop/Temp/Bede/Topology"
exel_file = "/home/chemgen/Desktop/Temp/Final_Figures/Figure_4_sup_Gene_Community_ORF.xlsx"
writer = pd.ExcelWriter(exel_file, engine = 'xlsxwriter')
for root, dirs, files in os.walk(path):
    for file in files:
        if file.endswith(".edgelist"):
            name= str(file).split(".")[0]
            name= name.split("_")[2:4]
            name = "_".join(name)
            file_with_path =  os.path.join(root,file)
            G = nx.read_edgelist( file_with_path)
            louvain_community_dict = community.best_partition(G)
            louvain_comms = {}
            for node_index, comm_id in louvain_community_dict.items():
                if comm_id in louvain_comms.keys():
                    louvain_comms[comm_id].append(node_index)
                else:
                    louvain_comms[comm_id]= [node_index]
            df = pd.DataFrame.from_dict(louvain_comms, orient='index')
            df= df.transpose()
            df.to_excel(writer, sheet_name=name, index= False)
writer.save()

df_file = pd.ExcelFile("/home/chemgen/Desktop/Temp/Final_Figures/Figure_4_sup_Gene_Community_ORF.xlsx")
exel_file = "/home/chemgen/Desktop/Temp/Final_Figures/Figure_4_sup_Go_Term.xlsx"
writer = pd.ExcelWriter(exel_file, engine = 'xlsxwriter')
for sheet in df_file.sheet_names:
    exel_df = {}
    df_parsed = df_file.parse(sheet)
    for col in df_parsed :
        Systematic_Name = df_parsed[col].tolist()
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag, termcounts, Systematic_Name, 0.05)
        exel_df["{}_GO_id".format(col)] =  goea_terms
        exel_df["{}_GO_id_name".format(col)] =  go_terms_name
        exel_df["{}_Coverage".format( col)] = go_cove_lis
        exel_df["{}_P_val".format(col)] = go_pvalue
        exel_df["{}_Score".format(col)] = score 
    df = pd.DataFrame.from_dict(exel_df, orient='index') 
    df = df.transpose()  
    df.to_excel(writer, sheet_name=sheet, index= False) 
writer.save()    
#%%%
df_file = pd.ExcelFile("/home/chemgen/Desktop/Temp/Final_Figures/Figure_4_sup_Gene_Community_ORF.xlsx")
exel_file = "/home/chemgen/Desktop/Temp/Final_Figures/Figure_4_sup_Go_Term_picked.xlsx"
writer = pd.ExcelWriter(exel_file, engine = 'xlsxwriter')
for sheet in df_file.sheet_names:
    exel_df = {}
    df_parsed = df_file.parse(sheet)
    for col in df_parsed :
        go_terms_name_new_list =[]
        Systematic_Name = df_parsed[col].tolist()
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag, termcounts, Systematic_Name, 0.01)
        for i, j in zip(go_terms_name, go_cove_lis):
            if j > 0.1:
                go_terms_name_new_list.append(i)
        exel_df["{}".format(col)] =  go_terms_name_new_list
    df = pd.DataFrame.from_dict(exel_df, orient='index') 
    df = df.transpose()  
    df.to_excel(writer, sheet_name=sheet, index= False) 
writer.save()  

    