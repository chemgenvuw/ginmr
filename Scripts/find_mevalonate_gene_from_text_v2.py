#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 14:43:19 2018

@author: chemgen
"""

import csv
import os
import pandas as pd

mevalonate_gene_file= "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/Mevalonate_pathway_ORF.csv"
raw_data_path = "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/Raw_data"
mevalonate_gene_dic = {}
mergef_out_df={}

"""
background_key = ["Ator_S288c", "Ator_UWOPS87", "Ator_YPS606", "Ator_Y55",
                  "Ceri_S288c", "Ceri_UWOPS87", "Ceri_YPS606", "Ceri_Y55",
                  "hmg1_S288c", "hmg1_UWOPS87", "hmg1_YPS606", "hmg1_Y55",
                  "hmg2_S288c", "hmg2_UWOPS87", "hmg2_YPS606", "hmg2_Y55",
                  "arv1_S288c", "arv1_UWOPS87", "arv1_YPS606", "arv1_Y55"]

"""

with open(mevalonate_gene_file , "r") as f:
    mevalonate_gene_csv = csv.reader(f,delimiter='\t')
    for row in mevalonate_gene_csv:
        mevalonate_gene_dic[row[0]]= [[],[]]
for root, dirs, files in os.walk(raw_data_path):
    for file in files:
        if file.endswith(".txt"):  
            SGA_name= file.split(".")[0]
            file = os.path.join(root, file)
            tem_dic = {}
            with open(file , "r", encoding='iso-8859-1') as csv_file:
                mevalonate_gene_csv = csv.reader(csv_file ,delimiter='\t', quotechar='|')
                for row in mevalonate_gene_csv:
                    if row[18] in mevalonate_gene_dic :
                        tem_dic[row[18]] = row[6]
            mergef_out_df[SGA_name] =  tem_dic        



df = pd.DataFrame(mergef_out_df)                      
df_parsed = pd.DataFrame.from_dict(mergef_out_df)
df_parsed.to_excel("/home/chemgen/Desktop/testdata.xlsx")

 