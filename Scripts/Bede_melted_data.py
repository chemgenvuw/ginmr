#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  5 09:41:55 2018

@author: chemgen
"""

import pandas as pd 


file = pd.read_csv("/home/chemgen/Desktop/Bede_SAFE_Input_Merged.csv", sep=',')

bede_dic = {}
for row in file.iterrows() :
    bede_dic[row[1][0] +"\t"+ row[1][1]] = ["NA", "NA", "NA", "NA", "NA",
            "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA",
            "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA", "NA"]
name = "koof "
for row in file.iterrows() :
    if str(row[1][2]) != name:
        print(str(row[1][2]))
        name = str(row[1][2])

for row in file.iterrows() :   
    if str(row[1][2]) == str(r"ARV1 S288c "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][0]=row[1][3]
    if str(row[1][2]) == str(r"ARV1 SK1 "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][1]=row[1][3]
    if str(row[1][2]) == str(r"ARV1 Y55"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][2]=row[1][3]
    if str(row[1][2]) == str(r"ARV1 YPS606"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][3]=row[1][3]
    #       
    if str(row[1][2]) == str(r"Ator S288c "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][4]=row[1][3]
    if str(row[1][2]) == str(r"Ator SK1"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][5]=row[1][3]
    if str(row[1][2]) == str(r"Ator Y55"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][6]=row[1][3]
    if str(row[1][2]) == str(r"Ator YPS606"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][7]=row[1][3]
    #        
    if str(row[1][2]) == str(r"BTS1 S288c "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][8]=row[1][3]
    if str(row[1][2]) == str(r"BTS1 SK1"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][9]=row[1][3]
    if str(row[1][2]) == str(r"BTS1 Y55"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][20]=row[1][3]
    if str(row[1][2]) == str(r"BTS1 YPS606"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][11]=row[1][3]
    #        
    if str(row[1][2]) == str(r"Ceri S288c"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][12]=row[1][3]     
    if str(row[1][2]) == str(r"Ceri SK1"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][13]=row[1][3]
    if str(row[1][2]) == str(r"Ceri Y55"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][14]=row[1][3]
    if str(row[1][2]) == str(r"Ceri YPS606"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][15]=row[1][3]
    #        
    if str(row[1][2]) == str(r"HMG1 S288c "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][16]=row[1][3]
    if str(row[1][2]) == str(r"HMG1 SK1"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][17]=row[1][3]
    if str(row[1][2]) == str(r"HMG1 Y55 "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][18]=row[1][3]
    #        
    if str(row[1][2]) == str(r"HMG2 S288c"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][19]=row[1][3]
    if str(row[1][2]) == str(r"HMG2 SK1"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][20]=row[1][3]
    if str(row[1][2]) == str(r"HMG2 Y55 "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][21]=row[1][3]
    #        
    if str(row[1][2]) == str(r"OPI3 S288c"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][22]=row[1][3]     
    if str(row[1][2]) == str(r"OPI3 Y55"):
            bede_dic[row[1][0] +"\t"+ row[1][1]][23]=row[1][3]    
    if str(row[1][2]) == str(r"OPI3 YPS606 "):
            bede_dic[row[1][0] +"\t"+ row[1][1]][24]=row[1][3]
        
        
A= pd.DataFrame.from_dict(bede_dic, orient='index', dtype=None)    
A.columns=["ARV1 S288c ","ARV1 SK1","ARV1 Y55",
           "ARV1 YPS606","Ator S288c ","Ator SK1","Ator Y55",
           "Ator YPS606","BTS1 S288c ","BTS1 SK1","BTS1 Y55","BTS1 YPS606",
           "Ceri S288c","Ceri SK1","Ceri Y55","Ceri YPS606",
           "HMG1 S288c ","HMG1 SK1","HMG1 Y55","HMG2 S288c","HMG2 SK1",
           "HMG2 Y55","OPI3 S288c","OPI3 Y55","OPI3 YPS606" ]        
A.to_csv("/home/chemgen/Desktop/melted.csv")