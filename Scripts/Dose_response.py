#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 15:55:45 2018

@author: chemgen
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 
import matplotlib.gridspec as gridspec


df_file = pd.ExcelFile("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Namal_Growth_curve.xlsx")
df_parsed = df_file.parse("DTT", index_col =0)
x = np.log10(np.array([10, 3.162, 1, 0.316, 0.100, 0.030, 0.01, 0.003, 0.001, 0.0001]))
#10, 3.162, 1, 0.316, 0.100, 0.030, 0.01, 0.003, 0.001, 0.0001


y_s288c =  df_parsed.loc['S288C$^{+}$']
y_err_s288c=df_parsed.loc['S288C$^{+}$_err']

y_Y55 =  df_parsed.loc['Y55$$^{+}$']
y_err_Y55=df_parsed.loc['Y55$$^{+}$_err']

y_yps= df_parsed.loc['YPS606$^{+}$']
y_yps_err= df_parsed.loc['YPS606$^{+}$_err']

y_UWP= df_parsed.loc['UWOPS87$^{+}$']
y_UWP_err= df_parsed.loc['UWOPS87$^{+}$_err']

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

#f, (ax, ax2) = plt.subplots(2, 1, sharex=True)
f, (ax) = plt.subplots(1, 1, sharex=True)
#ax.set_ylim(80, 120)  # outliers only

#ax2.set_ylim(0, 40)  # most of the data
#ax2.set_yticklabels(["0" ,"10" ,"20", "30"])
#plt.subplots_adjust(hspace=0.1)

ax.errorbar(x, y_s288c, yerr=y_err_s288c, c ='red', marker = 's', label="S288C$^{+}$" )
#ax2.errorbar(x, y_s288c, yerr=y_err_s288c, c ='red', marker = '.')

ax.errorbar(x, y_Y55, yerr=y_err_Y55, c ='blue', marker = 'p' , label="Y55$^{+}$")
#ax2.errorbar(x, y_Y55, yerr=y_err_Y55, c ='blue', marker = '>')

ax.errorbar(x, y_yps, yerr=y_yps_err, c ='green', marker = 'P' , label="YPS606$^{+}$" )
#ax2.errorbar(x, y_yps, yerr=y_yps_err, c ='green', marker = '<')

ax.errorbar(x, y_UWP, yerr=y_UWP_err, c ='black', marker = 'X' , label="UWOPS87$^{+}$")
#ax2.errorbar(x,y_UWP, yerr=y_UWP_err, c ='black', marker = '*')

ax.legend()
#ax.spines['bottom'].set_visible(False)
#ax2.spines['top'].set_visible(False)
#ax.xaxis.tick_top()
#ax.tick_params(labeltop='off')  # don't put tick labels at the top
#ax2.xaxis.tick_bottom()

#d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
#kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
#ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
#ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

#kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
#ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
#ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

ax.set_xlabel('DDT $\log_{10}(\mu M)$')
ax.set_ylabel('Growth$\%$')
#ax2.set_xlabel('DDT$\mu$M')
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/Dose_response.png", dpi=500,
            bbox_inches = 'tight', transparent = True)

