 arv =   [0.9290    0.6940    0.1250];
 ator=   [0.64, 0.08, 0.18];
 bts =   [0    0    0];
 ceri=   [0    1.0000    0];
 hmg1=   [0,1,1];
 hmg2=   [1,1,0];
 opi =   [0    0.4470    0.7410];
 
 rang = ator
%% Draw regions
sgn=1;
ucolors = unique(layout_key.groupColor(:,sgn));
if ucolors(1) ~= 1
    ucolors = [1; ucolors];
end
rng(3,'twister');
map_colors = colormap(lines((1000*length(ucolors)-1)));
map_colors = map_colors(randperm(size(map_colors,1)),:);
map_colors = [1 1 1; map_colors]; 
layout_key.mapColors = map_colors;
layout_key.geneRGB = ones(length(layout_key.label),3);
layout_key.geneRGB = map_colors(layout_key.labelColor,:);
 
for i = length(ucolors)
    %2:length(ucolors)
    
    ax = gca;
     % inds = find(layout_key.labelColor(:,sgn) == ucolors(i));
    indsg = find(layout_key.groupColor(:,sgn) == ucolors(i));
    [r,~,~] = find(layout_key.opacity_01(:,indsg,sgn)>0);
    inds = unique(r);
    x = layout_key.x(inds);
    y = layout_key.y(inds);
    colors = layout_key.geneRGB(inds,:);
    
    xpass=  isoutlier(x);
    x = x(~xpass);
    y = y(~xpass);
    colors = colors(~xpass, :);
    
    ypass= isoutlier(y);
    y = y(~ypass);
    x = x(~ypass);
    colors = colors(~ypass, :);
%     x = vertcat(x, x+65);
%     x= vertcat(x, x-65);
%     y = vertcat(y, y+65);
%     y = vertcat(y, y-65);
    k=boundary(x , y, 0.3);
%     patch(ax, x(k),y(k), [1 1 1], 'FaceAlpha', 0.95, 'EdgeColor', [0 0 0], "LineWidth", 1.25,...
%         'LineStyle', '-.');
    centroid_x = sum((layout_key.labelOpacity(inds,sgn).^2) .* layout_key.x(inds)) ./ sum((layout_key.labelOpacity(inds,sgn).^2));
    centroid_y = sum((layout_key.labelOpacity(inds,sgn).^2) .* layout_key.y(inds)) ./ sum((layout_key.labelOpacity(inds,sgn).^2));
    sum((layout_key.labelOpacity(inds,sgn).^2) .* layout_key.y(inds)) ./ sum((layout_key.labelOpacity(inds,sgn).^2));
    hold on;
    %hText{i} = text(centroid_x, centroid_y, sprintf('%d',ucolors(i)),'FontSize',10,'Color','k','HorizontalAlignment','center');
    set(ax,'DataAspectRatio',[1 1 1]);
    set(ax, 'YDir','reverse');
    axis off;
    hold on;
    
end
 
 
  k = convhull(layout_key.x, layout_key.y);
  plot(ax, layout_key.x(k), layout_key.y(k),'k--', 'LineWidth',1.7);

 
 %% Draw regions
sgn=1;
ucolors = unique(layout.groupColor(:,sgn));
if ucolors(1) ~= 1
    ucolors = [1; ucolors];
end
rng(3,'twister');
map_colors = colormap(lines((length(ucolors)-1)));
map_colors = map_colors(randperm(size(map_colors,1)),:);
map_colors = [1 1 1; map_colors]; 
layout.mapColors = map_colors;
layout.geneRGB = ones(length(layout.label),3);
layout.geneRGB = map_colors(layout.labelColor,:);
%[14,4, 10, 11, 13, 7 ,6,3,9,5] %2:length(ucolors)
for i = 2:length(ucolors)
    ax = gca;
     % inds = find(layout.labelColor(:,sgn) == ucolors(i));
    indsg = find(layout.groupColor(:,sgn) == ucolors(i));
    [r,~,~] = find(layout.opacity_01(:,indsg,sgn)>0);
    inds = unique(r);
    x = layout.x(inds);
    y = layout.y(inds);
    colors = layout.geneRGB(inds,:);
    
    xpass=  isoutlier(x);
    x = x(~xpass);
    y = y(~xpass);
    colors = colors(~xpass, :);
    
    ypass= isoutlier(y);
    y = y(~ypass);
    x = x(~ypass);
    colors = colors(~ypass, :);
    k=boundary(x , y, 0.4);
   

    direction = datasample([1,-1],1);
    a = 50;
    b = 90;
    r = (b-a).*rand(1,1) + a;
    y = y + (direction*r);
    scatterPoints = scatter(ax, x, y,75,'.','MarkerEdgeColor', rang,'MarkerEdgeAlpha',.999);

    centroid_x = sum((layout.labelOpacity(inds,sgn).^2) .* layout.x(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    centroid_y = sum((layout.labelOpacity(inds,sgn).^2) .* layout.y(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    sum((layout.labelOpacity(inds,sgn).^2) .* layout.y(inds)) ./ sum((layout.labelOpacity(inds,sgn).^2));
    hold on;     
    %hText{i} = text(centroid_x, centroid_y, sprintf('%d',ucolors(i)),'FontSize',10,'Color','k','HorizontalAlignment','center');
    set(ax,'DataAspectRatio',[1 1 1]);
    set(ax, 'YDir','reverse');
    axis off;
    hold on;
    
end
  k = convhull(layout.x, layout.y);

    plot(ax, layout_key.x(k), layout_key.y(k),'k--', 'LineWidth',1.7);




%% Save domain file     
  [filepath,name,ext] = fileparts(layout.annotationfile);
  OUTPUTfile = {'/home/chemgen/Desktop/Temp/Paul/safe/Results/', name, '.csv' };
  OUTPUTmatlab = {'/home/chemgen/Desktop/Temp/Paul/safe/Results/', name, '.mat' };
  strjoin(OUTPUTmatlab)
  OUTPUTfig = {'/home/chemgen/Desktop/Temp/Paul/safe/Results/', name, '.png' };
%   OUTPUTfigin = {'/home/chemgen/Desktop/Temp/safe/Results/Paul_result_fianl/', name, '.fig' };
  saveas(gcf,strjoin(OUTPUTfig));
%   savefig(strjoin(OUTPUTfigin))
  fid = fopen(strjoin(OUTPUTfile),'w');
  for i = 1 : length(ucolors)-1
    fprintf(fid, '%d\t%s\t%d\t%d\t%d\n', ucolors(i), layout.regionName{sgn}{i}, round(map_colors(i,1)*255), round(map_colors(i,2)*255), round(map_colors(i,3)*255));
  end
  fclose(fid);
save(strjoin(OUTPUTmatlab), 'layout');
