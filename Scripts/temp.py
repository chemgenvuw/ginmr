"""
Created on Thu Jun 28 09:07:33 2018

@author: chemgen
"""




import igraph
from igraph import plot as p
import mglearn
import numpy as np
from sklearn.cluster import AgglomerativeClustering, KMeans, AffinityPropagation, DBSCAN
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import matplotlib as mpl
import re
from sklearn import preprocessing
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, ClusterNode,leaves_list
from scipy.spatial.distance import pdist
from sklearn import metrics
from orangecontrib.bio import go

from mpl_toolkits.mplot3d import Axes3D

 c = [[i * j for i in a] for j in b]                               
                
                

        
def make_exel_file (active_gene, Des_func): #desgnated function
    df = {}
    for key in active_gene.keys():
        df[key] = active_gene[key][0]
    df = pd.DataFrame.from_dict(df, orient='index') 
    df= df.transpose()
    df.to_excel("/home/chemgen/Desktop/Temp/Ontology/{}.xlsx".format(Des_func))   
    return df


def get_go_graph (clusterd_list, pval):
    for key in clusterd_list.keys():
        go_withp= get_go_terms(obodag, clusterd_list[key][0], pval)

                                     slims_only=True)
    
    

%%%%%%%%%%%%%%%%%%%%%%%%%%
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "yellow"
    if type_dic == 1:
         tc =exel_dfhc
         color = "black"
    if type_dic == 2:
         tc =exel_dfhe
         color = "green"
    if type_dic == 3:
         tc = exel_dfhb
         color = "blue"
    if type_dic == 4:
         tc = exel_df3f
         color = "red"  
    x= tc['S288C_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['S288C_ATOR_P_val']]
    s= [i * 400 for i in tc['S288C_ATOR_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle=":",  marker = "<")
    x= tc['Y55_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['Y55_ATOR_P_val']]
    s= [i * 400 for i in tc['Y55_ATOR_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-.", marker  = "<")
    x= tc['UWOP_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_ATOR_P_val']]
    s= [i * 400 for i in tc['UWOP_ATOR_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="--", marker  = "<")
    x= tc['YPS606_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_ATOR_P_val']]
    s= [i * 400 for i in tc['YPS606_ATOR_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "<")
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/Ator.png", dpi=500, transparent = False) 
    ####
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "yellow"
    if type_dic == 1:
         tc =exel_dfhc
         color = "black"
    if type_dic == 2:
         tc =exel_dfhe
         color = "green"
    if type_dic == 3:
         tc = exel_dfhb
         color = "blue"
    if type_dic == 4:
         tc = exel_df3f
         color = "red"  
    x= tc['S288C_CERI_Score']
    y= [-(np.log10(i)) for i in tc['S288C_CERI_P_val']]
    s= [i * 400 for i in tc['S288C_CERI_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle=":",  marker = ">")
    x= tc['Y55_CERI_Score']
    y= [-(np.log10(i)) for i in tc['Y55_CERI_P_val']]
    s= [i * 400 for i in tc['Y55_CERI_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-.", marker  = ">")
    x= tc['UWOP_CERI_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_CERI_P_val']]
    s= [i * 400 for i in tc['UWOP_CERI_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="--", marker  = ">")
    x= tc['YPS606_CERI_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_CERI_P_val']]
    s= [i * 400 for i in tc['YPS606_CERI_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = ">")
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/CERI.png", dpi=500, transparent = False) 
    ####
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "yellow"
    if type_dic == 1:
         tc =exel_dfhc
         color = "black"
    if type_dic == 2:
         tc =exel_dfhe
         color = "green"
    if type_dic == 3:
         tc = exel_dfhb
         color = "blue"
    if type_dic == 4:
         tc = exel_df3f
         color = "red"      
    x= tc['S288C_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['S288C_ARV1_P_val']]
    s= [i * 400 for i in tc['S288C_ARV1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle=":",  marker = ",")
    x= tc['Y55_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['Y55_ARV1_P_val']]
    s= [i * 400 for i in tc['Y55_ARV1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-.", marker  = ",")
    x= tc['UWOP_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_ARV1_P_val']]
    s= [i * 400 for i in tc['UWOP_ARV1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="--", marker  = ",")
    x= tc['YPS606_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_ARV1_P_val']]
    s= [i * 400 for i in tc['YPS606_ARV1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = ",")
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/ARV1.png", dpi=500, transparent = False)     
    
    ####
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "yellow"
    if type_dic == 1:
         tc =exel_dfhc
         color = "black"
    if type_dic == 2:
         tc =exel_dfhe
         color = "green"
    if type_dic == 3:
         tc = exel_dfhb
         color = "blue"
    if type_dic == 4:
         tc = exel_df3f
         color = "red"      
    x= tc['S288C_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['S288C_HMG1_P_val']]
    s= [i * 400 for i in tc['S288C_HMG1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle=":",  marker = "h")
    x= tc['Y55_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['Y55_HMG1_P_val']]
    s= [i * 400 for i in tc['Y55_HMG1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-.", marker  = "h")
    x= tc['UWOP_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_HMG1_P_val']]
    s= [i * 400 for i in tc['UWOP_HMG1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="--", marker  = "h")
    x= tc['YPS606_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_HMG1_P_val']]
    s= [i * 400 for i in tc['YPS606_HMG1_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "h")
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/HMG1.png", dpi=500, transparent = False) 
    ##
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "yellow"
    if type_dic == 1:
         tc =exel_dfhc
         color = "black"
    if type_dic == 2:
         tc =exel_dfhe
         color = "green"
    if type_dic == 3:
         tc = exel_dfhb
         color = "blue"
    if type_dic == 4:
         tc = exel_df3f
         color = "red"          
    x= tc['S288C_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['S288C_HMG2_P_val']]
    s= [i * 400 for i in tc['S288C_HMG2_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle=":",  marker = "d")
    x= tc['Y55_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['Y55_HMG2_P_val']]
    s= [i * 400 for i in tc['Y55_HMG2_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-.", marker  = "d")
    x= tc['UWOP_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_HMG2_P_val']]
    s= [i * 400 for i in tc['UWOP_HMG2_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="--", marker  = "d")
    x= tc['YPS606_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_HMG2_P_val']]
    s= [i * 400 for i in tc['YPS606_HMG2_Coverage']]
    sct = plt.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/HMG2.png", dpi=500, transparent = False)     

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


































