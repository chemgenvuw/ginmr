#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 20:19:19 2018

@author: chemgen
"""

import csv
import os
import pandas as pd

mevalonate_gene_file= "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/Mevalonate_pathway_ORF.csv"
raw_data_path = "/media/chemgen/Chemgen/Projects/Bede/Raw data"
mevalonate_gene_dic = {}
mergef_out_df={}
with open(mevalonate_gene_file , "r") as f:
    mevalonate_gene_csv = csv.reader(f,delimiter='\t')
    for row in mevalonate_gene_csv:
        mevalonate_gene_dic[row[0]]= [[],[]]
        
for root, dirs, files in os.walk(raw_data_path):
    for file in files:
        if file.endswith(".xlsx"):  
            out_put_file = os.path.join(root,"output_mevalonate_gene.xlsx" )
            sheet_name= file.split(".")[0]
            file = os.path.join(root, file)
            df_file = pd.ExcelFile(file)
            df_parsed = df_file.parse(sheet_name)
            ORF = df_parsed['ID Column']
            z_score= df_parsed['Z-Score']
            p_value= df_parsed['P-Value']
            
            list_1 = []
            list_2 = []
            list_3 = [] 
            for a,b,c in zip(ORF,z_score,p_value):
                if a in mevalonate_gene_dic:
                    list_1.append(a)
                    list_2.append(b)
                    list_3.append(c)
            #mergef_out_df.update({'%s_gene' %sheet_name:list_1})  
            mergef_out_df.update({'%s' %sheet_name:list_2}) 
            #mergef_out_df.update({'%s_p-value' %sheet_name:list_3}) 

df = pd.DataFrame(mergef_out_df)
writer = pd.ExcelWriter(out_put_file, engine='xlsxwriter')                           
df.to_excel(writer, sheet_name='Sheet1')
writer.save()
"""
if a in mevalonate_gene_dic:
print(a+"="+str(b)+ "-->"+ str(c))
w_f.write(a+"\t"+str(b)+"\t"+str(c)+"\n")
 """                           