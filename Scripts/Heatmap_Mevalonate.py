#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 30 16:38:39 2018

@author: chemgen
"""

import seaborn as sns; sns.set(color_codes=True)
import pandas as pd
from sklearn import preprocessing
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet
from scipy.spatial.distance import pdist


df_file = pd.ExcelFile("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Mevalonate_pathway_scores.xlsx")
df_parsed = df_file.parse("name", index_col =0)

num_col =0
for i in df_parsed.iteritems():
    num_col += 1
    
for i in range(num_col):
    float_array = df_parsed.iloc[:,i].values.astype(float).reshape(-1,1)
    imputer = preprocessing.Imputer()
    float_array= imputer.fit_transform(float_array)
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(float_array)
    df_parsed.iloc[:,i] = x_scaled
    
latex_row_name = {}
for i in range(len(df_parsed.index)):
    latex_row_name[df_parsed.index[i]] = "\\textit{%s}-$\Delta$" % df_parsed.index[i].lower()
df_parsed.rename(index=latex_row_name, inplace=True)   
    

sns.set(font_scale=1)
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)


cmap = sns.diverging_palette(220, 15, sep=90, as_cmap=True)


plt.figure(dpi =500, figsize=(4.3, 8.6))

cm = sns.clustermap(df_parsed, cmap =cmap, method="centroid")
plt.setp(cm.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
plt.setp(cm.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
hm = cm.ax_heatmap.get_position()
cm.ax_heatmap.set_position([hm.x0, hm.y0, hm.width*0.5, hm.height*0.8])
col = cm.ax_col_dendrogram.get_position()
cm.ax_col_dendrogram.set_position([col.x0, col.y0*0.83, col.width*0.5, col.height])
row = cm.ax_row_dendrogram.get_position()
cm.ax_row_dendrogram.set_position([row.x0, row.y0, row.width, row.height*0.8])
plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/3-D.png", dpi=500,
            bbox_inches = 'tight', transparent = True)





Z= linkage(df_parsed, 'centroid')
c, coph_dists = cophenet(Z, pdist(df_parsed))
print(c)


    