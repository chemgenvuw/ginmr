""" annotation of clusters"""
df_file = pd.ExcelFile("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Final(copy)_for_python.xlsx")
exel_df = {}
for sheet in df_file.sheet_names:
    df_parsed = df_file.parse(sheet)
    Systematic_Name = df_parsed['Systematic Name'].tolist()
    goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag, termcounts, Systematic_Name, 0.05)
    exel_df["{}_GO_id".format(sheet)] =  goea_terms
    exel_df["{}_GO_id_name".format(sheet)] =  go_terms_name
    exel_df["{}_Coverage".format(sheet)] = go_cove_lis
    exel_df["{}_P_val".format(sheet)] = go_pvalue
    exel_df["{}_Score".format(sheet)] = score 
df = pd.DataFrame.from_dict(exel_df, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("GO_file_TI")) 

exel_df3f = {}
tc = h4f
tcn= "h4f"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_df3f["{}_GO_id".format(key)] =  goea_terms
        exel_df3f["{}_GO_id_name".format(key)] =  go_terms_name
        exel_df3f["{}_Coverage".format(key)] = go_cove_lis
        exel_df3f["{}_P_val".format(key)] = go_pvalue
        exel_df3f["{}_Score".format(key)] = score
df = pd.DataFrame.from_dict(exel_df3f, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("3features")) 

exel_dfhb = {}
tc = hb
tcn= "hb"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_dfhb["{}_GO_id".format(key, tcn)] =  goea_terms
        exel_dfhb["{}_GO_id_name".format(key, tcn)] =  go_terms_name
        exel_dfhb["{}_Coverage".format(key, tcn)] = go_cove_lis
        exel_dfhb["{}_P_val".format(key, tcn)] = go_pvalue
        exel_dfhb["{}_Score".format(key, tcn)] = score
df = pd.DataFrame.from_dict(exel_dfhb, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("betweenness")) 

exel_dfhc = {}
tc = hc
tcn= "hc"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_dfhc["{}_GO_id".format(key, tcn)] =  goea_terms
        exel_dfhc["{}_GO_id_name".format(key, tcn)] =  go_terms_name
        exel_dfhc["{}_Coverage".format(key, tcn)] = go_cove_lis
        exel_dfhc["{}_P_val".format(key, tcn)] = go_pvalue
        exel_dfhc["{}_Score".format(key, tcn)] = score
df = pd.DataFrame.from_dict(exel_dfhc, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("closeness")) 

exel_dfhe = {}
tc = he
tcn= "he"
for key in tc.keys():
    Systematic_Name = tc[key][0]
    if len(Systematic_Name) == 0:
        pass
    else:
        goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                                termcounts, Systematic_Name, 0.05)
        exel_dfhe["{}_GO_id".format(key, tcn)] =  goea_terms
        exel_dfhe["{}_GO_id_name".format(key, tcn)] =  go_terms_name
        exel_dfhe["{}_Coverage".format(key, tcn)] = go_cove_lis
        exel_dfhe["{}_P_val".format(key, tcn)] = go_pvalue
        exel_dfhe["{}_Score".format(key, tcn)] = score
df = pd.DataFrame.from_dict(exel_dfhc, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("eigenvector")) 


#%%
""""" graphhhh """""
plt.style.use("dark_background")
fig, axes = plt.subplots(2,2, sharey='row', sharex='col' )
plt.subplots_adjust(left=None, bottom=None, right=None, top=None,
                wspace=0.05, hspace=0.165)

plt.style.use("dark_background")
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

from matplotlib.lines import Line2D
legend_elements = [Line2D([0], [0], marker='<', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='>', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='s', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='h', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='d', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#95a5a6"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#e74c3c"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#2ecc71"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#3498db"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#fce166")]
                  
axes[0,0].legend(legend_elements, [r'Ator',
                                   r'Ceri',
                         r'\textit{arv1}-$\Delta$',
                         r'\textit{hmg1}-$\Delta$',
                         r'\textit{hmg2}-$\Delta$',
                         "Crude",
                         "CC","EC","BC", "3FC"], ncol=2)



axes[0,0].title.set_text(r'S288C')
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"    
    x= tc['S288C_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['S288C_ATOR_P_val']]
    s= [i * 100 for i in tc['S288C_ATOR_Coverage']]
    axes[0,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "<")
    
    x= tc['S288C_CERI_Score']
    y= [-(np.log10(i)) for i in tc['S288C_CERI_P_val']]
    s= [i * 100 for i in tc['S288C_CERI_Coverage']]
    axes[0,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ">")
    
    x= tc['S288C_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['S288C_ARV1_P_val']]
    s= [i * 100 for i in tc['S288C_ARV1_Coverage']]
    axes[0,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ",")
    
    x= tc['S288C_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['S288C_HMG1_P_val']]
    s= [i * 100 for i in tc['S288C_HMG1_Coverage']]
    axes[0,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "h")
    
    x= tc['S288C_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['S288C_HMG2_P_val']]
    s= [i * 100 for i in tc['S288C_HMG2_Coverage']]
    axes[0,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "d")
    
axes[0,1].title.set_text(r'Y55')   
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"     
    
    x= tc['Y55_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['Y55_ATOR_P_val']]
    s= [i * 100 for i in tc['Y55_ATOR_Coverage']]
    axes[0,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "<")
    
    x= tc['Y55_CERI_Score']
    y= [-(np.log10(i)) for i in tc['Y55_CERI_P_val']]
    s= [i * 100 for i in tc['Y55_CERI_Coverage']]
    axes[0,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ">")
    
    x= tc['Y55_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['Y55_ARV1_P_val']]
    s= [i * 100 for i in tc['Y55_ARV1_Coverage']]
    axes[0,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ",")
    
    x= tc['Y55_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['Y55_HMG1_P_val']]
    s= [i * 100 for i in tc['Y55_HMG1_Coverage']]
    axes[0,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "h")
    
    x= tc['Y55_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['Y55_HMG2_P_val']]
    s= [i * 100 for i in tc['Y55_HMG2_Coverage']]
    axes[0,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "d")

axes[1,0].title.set_text(r'UWOPS87')     
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"     
    
    x= tc['UWOP_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_ATOR_P_val']]
    s= [i * 100 for i in tc['UWOP_ATOR_Coverage']]
    axes[1,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "<")
    
    x= tc['UWOP_CERI_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_CERI_P_val']]
    s= [i * 100 for i in tc['UWOP_CERI_Coverage']]
    axes[1,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ">")
    
    x= tc['UWOP_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_ARV1_P_val']]
    s= [i * 100 for i in tc['UWOP_ARV1_Coverage']]
    axes[1,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ",")
    
    x= tc['UWOP_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_HMG1_P_val']]
    s= [i * 100 for i in tc['UWOP_HMG1_Coverage']]
    axes[1,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "h")
    
    x= tc['UWOP_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['UWOP_HMG2_P_val']]
    s= [i * 100 for i in tc['UWOP_HMG2_Coverage']]
    axes[1,0].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "d")

axes[1,1].title.set_text(r'YPS606')     
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"     
    
    x= tc['YPS606_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_ATOR_P_val']]
    s= [i * 100 for i in tc['YPS606_ATOR_Coverage']]
    axes[1,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "<")
    
    x= tc['YPS606_CERI_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_CERI_P_val']]
    s= [i * 100 for i in tc['YPS606_CERI_Coverage']]
    axes[1,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ">")
    
    x= tc['YPS606_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_ARV1_P_val']]
    s= [i * 100 for i in tc['YPS606_ARV1_Coverage']]
    axes[1,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = ",")
    
    x= tc['YPS606_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_HMG1_P_val']]
    s= [i * 100 for i in tc['YPS606_HMG1_Coverage']]
    axes[1,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "h")
    
    x= tc['YPS606_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_HMG2_P_val']]
    s= [i * 100 for i in tc['YPS606_HMG2_Coverage']]
    axes[1,1].scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.4, linestyle="-",  marker = "d")
    
plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/3d.png", dpi=500, transparent = False)  

#%%
#Figure Supp
exel_file = "/home/chemgen/Desktop/Temp/Final_Figures/Figure_5_sup_Gene_GO.xlsx"
writer = pd.ExcelWriter(exel_file, engine = 'xlsxwriter')
for key in clustered.keys():
    for i in range(4,8):
        name_list= []
        goea_terms = []
        go_terms_name = []
        go_cove_lis_s = {}
        go_cove_lis = []
        go_pvalue = []
        score = []
        for n, b, c, e, thf, bf, cf, ef in  zip(clustered[key][0],
                                       clustered[key][1],
                                       clustered[key][2],
                                       clustered[key][3], 
                                       clustered[key][4], 
                                       clustered[key][5],
                                       clustered[key][6],
                                       clustered[key][7]):
            if i == thf:
                    name_list.append(n)
        if len(name_list) == 0:
            pass
        else:
            goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                 termcounts, name_list, 0.05)
        if len(goea_terms) > 0:  
            exel_df = {}       
            exel_df["{}_{}_GO_id".format(key,i)] =  goea_terms
            exel_df["{}_{}_GO_id_name".format(key,i)] =  go_terms_name
            exel_df["{}_{}_Coverage".format(key,i)] = go_cove_lis
            exel_df["{}_{}_P_val".format(key, i)] = go_pvalue
            exel_df["{}_{}_Score".format(key, i)] = score
            gene_name= id_concvertor(name_list)
            exel_df["{}_{}_Name".format(key, i)] = gene_name
            df = pd.DataFrame.from_dict(exel_df, orient='index') 
            df = df.transpose()  
            df.to_excel(writer, sheet_name="{}_{}_3FCC".format(key,i), index= False) 

        for n, b, c, e, thf, bf, cf, ef in  zip(clustered[key][0],
                                       clustered[key][1],
                                       clustered[key][2],
                                       clustered[key][3], 
                                       clustered[key][4], 
                                       clustered[key][5],
                                       clustered[key][6],
                                       clustered[key][7]):
            if i == bf:
                    name_list.append(n)
        if len(name_list) == 0:
            pass
        else:
            goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                 termcounts, name_list, 0.05)
        if len(goea_terms) > 0:  
            exel_df = {}       
            exel_df["{}_{}_GO_id".format(key,i)] =  goea_terms
            exel_df["{}_{}_GO_id_name".format(key,i)] =  go_terms_name
            exel_df["{}_{}_Coverage".format(key,i)] = go_cove_lis
            exel_df["{}_{}_P_val".format(key, i)] = go_pvalue
            exel_df["{}_{}_Score".format(key, i)] = score
            gene_name= id_concvertor(name_list)
            exel_df["{}_{}_Name".format(key, i)] = gene_name
            df = pd.DataFrame.from_dict(exel_df, orient='index') 
            df = df.transpose()  
            df.to_excel(writer, sheet_name="{}_{}_BCC".format(key,i), index= False) 

        for n, b, c, e, thf, bf, cf, ef in  zip(clustered[key][0],
                                       clustered[key][1],
                                       clustered[key][2],
                                       clustered[key][3], 
                                       clustered[key][4], 
                                       clustered[key][5],
                                       clustered[key][6],
                                       clustered[key][7]):
            if i == cf:
                    name_list.append(n)
        if len(name_list) == 0:
            pass
        else:
            goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                 termcounts, name_list, 0.05)
        if len(goea_terms) > 0:  
            exel_df = {}       
            exel_df["{}_{}_GO_id".format(key,i)] =  goea_terms
            exel_df["{}_{}_GO_id_name".format(key,i)] =  go_terms_name
            exel_df["{}_{}_Coverage".format(key,i)] = go_cove_lis
            exel_df["{}_{}_P_val".format(key, i)] = go_pvalue
            exel_df["{}_{}_Score".format(key, i)] = score
            gene_name= id_concvertor(name_list)
            exel_df["{}_{}_Name".format(key, i)] = gene_name
            df = pd.DataFrame.from_dict(exel_df, orient='index') 
            df = df.transpose()  
            df.to_excel(writer, sheet_name="{}_{}_CCC".format(key,i), index= False)             

        for n, b, c, e, thf, bf, cf, ef in  zip(clustered[key][0],
                                       clustered[key][1],
                                       clustered[key][2],
                                       clustered[key][3], 
                                       clustered[key][4], 
                                       clustered[key][5],
                                       clustered[key][6],
                                       clustered[key][7]):
            if i == ef:
                    name_list.append(n)
        if len(name_list) == 0:
            pass
        else:
            goea_terms, go_terms_name, go_cove_lis, go_pvalue, score = get_go_terms(obodag,
                                                                 termcounts, name_list, 0.05)
        if len(goea_terms) > 0:  
            exel_df = {}       
            exel_df["{}_{}_GO_id".format(key,i)] =  goea_terms
            exel_df["{}_{}_GO_id_name".format(key,i)] =  go_terms_name
            exel_df["{}_{}_Coverage".format(key,i)] = go_cove_lis
            exel_df["{}_{}_P_val".format(key, i)] = go_pvalue
            exel_df["{}_{}_Score".format(key, i)] = score
            gene_name= id_concvertor(name_list)
            exel_df["{}_{}_Name".format(key, i)] = gene_name
            df = pd.DataFrame.from_dict(exel_df, orient='index') 
            df = df.transpose()  
            df.to_excel(writer, sheet_name="{}_{}_ECC".format(key,i), index= False)             
writer.save()     