#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 20:14:30 2018

@author: chemgen
"""

import pandas as pd
import os

output_folder = "/home/chemgen/Desktop/Temp/Bede/Topology/"
df_file = pd.ExcelFile("/home/chemgen/Desktop/Temp/Bede/Topology/Final(copy)_for_python.xlsx")

for sheet in df_file.sheet_names:
    df_parsed = df_file.parse(sheet)
    df = {'Gene':df_parsed['Gene'], 'Systematic Name':df_parsed['Systematic Name'],
          'P-Value':df_parsed['P-Value'],'Z-Score':df_parsed['Z-Score']}
    df= pd.DataFrame(df)
    df.to_excel(os.path.join(output_folder,(str(sheet)+".xlsx")),sheet_name='Sheet1', index=False)