#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 11:21:33 2018

@author: chemgen
"""
import pandas as pd 
import csv
from goatools.semantic import semantic_similarity
from goatools.semantic import lin_sim
from goatools.semantic import resnik_sim


def Load_db_GO():
    from goatools.obo_parser import GODag
    obodag = GODag("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Ontology/goslim_yeast.obo")
    
    from goatools.associations import read_ncbi_gene2go
    #saved on drive /media/chemgen/Chemgen/Databases/gene2go.gz
    geneid2gos_yeast = read_ncbi_gene2go("/media/chemgen/Chemgen/Databases/gene2go")
    print("{N:,} annotated yeast genes".format(N=len(geneid2gos_yeast)))
    
    yeast_gene_GO ={}
    gene_association_file_path = "/home/chemgen/Desktop/Temp/Ontology/DB/go_slim_mapping_sgd.tab"
    with open (gene_association_file_path) as gene_associationsdg_file:
        gene_associationsdg_file = csv.reader(gene_associationsdg_file , delimiter='\t', quotechar='|')
        for row in gene_associationsdg_file:
            if row[0] in yeast_gene_GO.keys() :
                val = yeast_gene_GO[row[0]]
                val.append(row[5])
                yeast_gene_GO.update({row[3] : val})
            else:
               yeast_gene_GO[row[0]] = [row[5]]
    
    from goatools.go_enrichment import GOEnrichmentStudy
    goeaobj = GOEnrichmentStudy(
            yeast_gene_GO.keys(), # List of myeast gene
            yeast_gene_GO, # geneid/GO associations
            obodag, # Ontologies
            propagate_counts = False,
            alpha = 0.5, # default significance cut-off
            methods = ['fdr_tsbh']) # defult multipletest correction method
    
    from goatools.semantic import TermCounts
    termcounts = TermCounts(obodag, yeast_gene_GO)
    return obodag,  geneid2gos_yeast ,  goeaobj, termcounts

def run(genids_study):
    goea_results_all = goeaobj.run_study(genids_study)
    goea_results_sig = [r for r in goea_results_all if r.p_fdr_bh < 0.05]
    return goea_results_all, goea_results_sig




termcounts = TermCounts(obodag, yeast_gene_GO)

sim = semantic_similarity('GO:0016568', 'GO:0016568', obodag)


go_id3  =  'GO:0048364' 
go_id4 = 'GO:0044707'
print(obodag['GO:0015646'])
print(obodag[go_id4])

a, b = run(
        
        sample= ['com2',
 'rpp1b',
 'spo7',
 'coa4',
 'cat5',
 'qcr2',
 'fun30',
 'ats1',
 'aps3',
 'pmt2',
 'saw1',
 'myo4',
 'mot3',
 'hmg1',
 'she9',
 'ubp5',
 'rim15',
 'mic19',
 'gep7',
 'ygr122c-a',
 'phb2',
 'pfk26',
 'aly2',
 'lcb3',
 'rcy1',
 'tom70',
 'aim2',
 'oaf1',
 'pex22',
 'pex1'])





path = "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/New_dataset/Final data for Nature Syst Biol paper_09.07.2018.xlsx"
 
goea_results_all = goeaobj.run_study(name_gene_study)
goea_results_sig = [r for r in goea_results_all if r.p_uncorrected < 0.5]
return goea_results_all, goea_results_sig


goeaobj.wr_xlsx("/home/chemgen/Desktop/nbt3102.xlsx", goea_results_all)


df_file = pd.ExcelFile("/home/chemgen/Desktop/nbt3102.xlsx")
f_parsed = df_file.parse("Sheet1", index_col =0)
goid_subset = df_parsed.index.tolist()
goid_subset = [
'GO:0036490',
'GO:1903573',
'GO:0070059',
'GO:0030968',
'GO:1905897',
'GO:0006983',

]



plot_gos("nbt3102_MF_RNA_Symbols.png", 
    goid_subset, # Source GO ids
    obodag,
    goea_results=goea_results_all, # use pvals for coloring
    # We can further configure the plot...
    id2symbol=geneid2symbol, # Print study gene Symbols, not Entrez GeneIDs
    study_items=6, # Only only 6 gene Symbols max on GO terms
    items_p_line=3, # Print 3 genes per line
    )


result = [[compute_stat(data[row], data[col]) for col in labels]
          for row in labels]














