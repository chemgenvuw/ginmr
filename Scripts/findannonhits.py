#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 13 13:08:59 2017

@author: chemgen
"""
import pandas as pd
import os


filepath = "/home/chemgen/Desktop/Temp/Bede"
uotput_path = "/home/chemgen/Desktop/Temp/Bede/SAFE_Input"
annondic = {}
annotdf = pd.read_csv('/media/chemgen/Chemgen/Projects/Bede/safe/data/go_bp_140819.txt', index_col = "ORF", sep = "\t")
annotdf_ind = pd.read_csv('/media/chemgen/Chemgen/Projects/Bede/safe/data/go_bp_140819.txt', sep = "\t")
annotdf['ORF'] = annotdf.index
annon_file_ORFS = []   
for row in annotdf_ind['ORF'] :
    annon_file_ORFS.append(row)


for root, dirs, files in os.walk(filepath):
    for file in files:
        if file.endswith(".xlsx"):
            print(file)
            file_out_name = os.path.join(uotput_path,file.split(".")[0] + ".txt")
            file = os.path.join(root, file)
            df_ = pd.DataFrame( columns=list(annotdf) , index = annotdf.index)
            df_file = pd.ExcelFile(file)
            df_parsed = df_file.parse('Sheet1')
            ORFS = df_parsed['Systematic Name'] # ID Column
            for ORF in ORFS :
                if ORF in annon_file_ORFS :
                    df_.loc[ORF] = annotdf.loc[ORF]
                    #print(annotdf.loc[ORF])
            df_for_fig = df_.dropna(how='any')
            df_for_fig.to_csv(file_out_name , sep='\t', columns=list(annotdf) , header=True, index=True, index_label=None)
               
            
           #  df_file= pd.read_csv('/home/chemgen/Desktop/OneDrive/notebook/Logs/Liams_figure/eli_tamin_Scored.csv', sep = ",")
            # ORFS = df_file['ORF']