#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 31 21:38:26 2018

@author: chemgen
"""
def ann (tc, given, y, setscore= 1.8, setcev=2): # dic, name, y, score, coverage
    import random
    IDs  = re.sub("Score", "GO_id", given)
    covs = re.sub("Score", "Coverage", given)
    for name , score, coverage, pval in zip(tc[IDs], tc[given],
                                       tc[covs], y ):
        if score > setscore and coverage > setcev:
                plt.annotate(name , xy=(score, pval),
                             xytext=(score + random.uniform(-0.5,0.5), pval ),
                             textcoords='offset points', ha='right', va='bottom',
                                bbox=dict(boxstyle='round,pad=0.5', fc='#34495e', alpha=0.5))
                        



import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
plt.style.use("dark_background")
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

fig = plt.figure(dpi=1000)
gs = gridspec.GridSpec(2, 6)

fig.text(0.5, 0.04, 'Uniqueness', ha='center')
fig.text(0.04, 0.5, '$-log_{10}$ adj.p-value', va='center', rotation='vertical')


from matplotlib.lines import Line2D
legend_elements = [ Line2D([0], [0], marker='>', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='s', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='o', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='d', color='k', markerfacecolor='w'),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="k"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#95a5a6"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#e74c3c"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#2ecc71"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#3498db"),
                   Line2D([0], [0], marker='*', color='k', markerfacecolor="#fce166")]
                  
ax8 = fig.add_subplot(gs[0,4:6])
ax8.legend(legend_elements, [r'S288C',
                                   r'Y55',
                                   r'UWOPS87',
                                   r'YPS606',
                                   r'',
                         "TI","CCC","ECC","BCC","3FCC"], ncol=2, handlelength=1, handleheight= 1)
ax8.axis('off')


###########ATOR

ax1 = fig.add_subplot(gs[0,0:2])
ax1.set_ylim(1, 7, auto=False)
ax1.set_xlim(0.14, 1, auto=False)
ax1.set_xticklabels([])
ax1.set_xticks([])
ax1.set_title(r'Ator')
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"   
    x= tc['S288C_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['S288C_ATOR_P_val']]
    s= [i * 100 for i in tc['S288C_ATOR_Coverage']]
    ax1.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle="-",  marker = ">")
    ann(tc, 'S288C_ATOR_Score', y)
    
    x= tc['Y55_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['Y55_ATOR_P_val']]
    s= [i * 100 for i in tc['Y55_ATOR_Coverage']]
    ax1.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "s")
    ann(tc, 'Y55_ATOR_Score', y)
   
    x= tc['UWOPS87_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['UWOPS87_ATOR_P_val']]
    s= [i * 100 for i in tc['UWOPS87_ATOR_Coverage']]
    ax1.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "o")
    ann(tc, 'UWOPS87_ATOR_Score', y )
    
    x= tc['YPS606_ATOR_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_ATOR_P_val']]
    s= [i * 100 for i in tc['YPS606_ATOR_Coverage']]
    ax1.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")
    ann(tc, 'YPS606_ATOR_Score', y)
############ CERI
ax2= fig.add_subplot(gs[0,2])
ax2.set_ylim(1, 7, auto=False)
ax2.set_xlim(0.11, 0.6, auto=False)
ax2.set_yticklabels([])
ax2.set_yticks([])
ax2.set_xticklabels([])
ax2.set_xticks([])
ax2.spines['right'].set_visible(False)
ax2.yaxis.tick_left()
ax2.set_yticklabels([])
ax2.tick_params(labelright='off')
d = .015 
kwargs = dict(transform=ax2.transAxes, color='w', clip_on=False)
ax2.plot((1-d,1+d), (-d,+d), **kwargs)
ax2.plot((1-d,1+d),(1-d,1+d), **kwargs)
ax2.title.set_text(r'Ceri')
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"   
    x= tc['S288C_CERI_Score']
    y= [-(np.log10(i)) for i in tc['S288C_CERI_P_val']]
    s= [i * 100 for i in tc['S288C_CERI_Coverage']]
    ax2.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle="-",  marker = ">")
    ann(tc, 'S288C_CERI_Score', y)
    
    x= tc['Y55_CERI_Score']
    y= [-(np.log10(i)) for i in tc['Y55_CERI_P_val']]
    s= [i * 100 for i in tc['Y55_CERI_Coverage']]
    ax2.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "s")
    ann(tc, 'Y55_CERI_Score', y)
    
    x= tc['UWOPS87_CERI_Score']
    y= [-(np.log10(i)) for i in tc['UWOPS87_CERI_P_val']]
    s= [i * 100 for i in tc['UWOPS87_CERI_Coverage']]
    ax2.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "o")
    ann(tc, 'UWOPS87_CERI_Score', y)
    
    x= tc['YPS606_CERI_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_CERI_P_val']]
    s= [i * 100 for i in tc['YPS606_CERI_Coverage']]
    ax2.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")
    ann(tc, 'YPS606_CERI_Score', y)
    
    
ax3= fig.add_subplot(gs[0,3])
ax3.set_ylim(1, 7, auto=False)
ax3.set_xlim(1.5, 2.1, auto=False)
ax3.spines['left'].set_visible(False)
ax3.set_yticklabels([])
ax3.set_yticks([])
ax3.set_xticklabels([])
ax3.set_xticks([])
kwargs.update(transform=ax3.transAxes)  # switch to the bottom axes
ax3.plot((-d,+d), (1-d,1+d), **kwargs)
ax3.plot((-d,+d), (-d,+d), **kwargs)
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"   
    x= tc['S288C_CERI_Score']
    y= [-(np.log10(i)) for i in tc['S288C_CERI_P_val']]
    s= [i * 100 for i in tc['S288C_CERI_Coverage']]
    ax3.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle="-",  marker = ">")
    ann(tc, 'S288C_CERI_Score', y)
    
    x= tc['Y55_CERI_Score']
    y= [-(np.log10(i)) for i in tc['Y55_CERI_P_val']]
    s= [i * 100 for i in tc['Y55_CERI_Coverage']]
    ax3.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "s")
    ann(tc, 'Y55_CERI_Score', y)
    
    x= tc['UWOPS87_CERI_Score']
    y= [-(np.log10(i)) for i in tc['UWOPS87_CERI_P_val']]
    s= [i * 100 for i in tc['UWOPS87_CERI_Coverage']]
    ax3.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "o")
    ann(tc, 'UWOPS87_CERI_Score', y)
    
    x= tc['YPS606_CERI_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_CERI_P_val']]
    s= [i * 100 for i in tc['YPS606_CERI_Coverage']]
    ax3.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")
    ann(tc, 'YPS606_CERI_Score', y)

###############ARV1    
ax4 = fig.add_subplot(gs[1,4:6])
ax4.set_ylim(1, 7, auto=False)
ax4.set_xlim(0.125, 1, auto=False)
ax4.set_yticklabels([])
ax4.set_yticks([])
ax4.set_xticklabels([0.1,1])
ax4.set_xticks([0.125,1])
ax4.title.set_text(r'\textit{arv1}-$\Delta$')
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"       
    x= tc['S288C_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['S288C_ARV1_P_val']]
    s= [i * 100 for i in tc['S288C_ARV1_Coverage']]
    ax4.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle="-",  marker = ">")
    ann(tc, 'S288C_ARV1_Score', y)
    
    x= tc['Y55_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['Y55_ARV1_P_val']]
    s= [i * 100 for i in tc['Y55_ARV1_Coverage']]
    ax4.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "s")
    ann(tc, 'Y55_ARV1_Score', y)
    
    x= tc['UWOPS87_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['UWOPS87_ARV1_P_val']]
    s= [i * 100 for i in tc['UWOPS87_ARV1_Coverage']]
    ax4.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "o")
    ann(tc, 'UWOPS87_ARV1_Score', y)
    
    x= tc['YPS606_ARV1_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_ARV1_P_val']]
    s= [i * 100 for i in tc['YPS606_ARV1_Coverage']]
    ax4.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")     
    ann(tc, 'YPS606_ARV1_Score', y)


########HMG1
ax5 = fig.add_subplot(gs[1,0:2])
ax5.set_ylim(1, 7, auto=False)
ax5.set_xlim(0.12, 1, auto=False)
ax5.set_xticks([0.12, 1])
ax5.set_xticklabels([0.1,1])
ax5.title.set_text(r'\textit{hmg1}-$\Delta$')
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"       
    x= tc['S288C_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['S288C_HMG1_P_val']]
    s= [i * 100 for i in tc['S288C_HMG1_Coverage']]
    ax5.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle="-",  marker = ">")
    ann(tc, 'S288C_HMG1_Score', y)
    
    x= tc['Y55_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['Y55_HMG1_P_val']]
    s= [i * 100 for i in tc['Y55_HMG1_Coverage']]
    ax5.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "s")
    ann(tc, 'Y55_HMG1_Score', y)
    
    x= tc['UWOPS87_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['UWOPS87_HMG1_P_val']]
    s= [i * 100 for i in tc['UWOPS87_HMG1_Coverage']]
    ax5.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "o")
    ann(tc, 'UWOPS87_HMG1_Score', y)
    
    x= tc['YPS606_HMG1_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_HMG1_P_val']]
    s= [i * 100 for i in tc['YPS606_HMG1_Coverage']]
    ax5.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")
    ann(tc, 'YPS606_HMG1_Score', y)
    
############HMG2
ax6= fig.add_subplot(gs[1,2])
ax6.set_ylim(1, 7, auto=False)
ax6.set_xlim(0.12, 0.5, auto=False)
ax6.set_yticklabels([])
ax6.set_yticks([])
ax6.set_xticklabels([0.1, 0.5])
ax6.set_xticks([0.12, 0.5])
ax6.spines['right'].set_visible(False)
ax6.yaxis.tick_left()
ax6.set_yticklabels([])
ax6.tick_params(labelright='off')
d = .015 
kwargs = dict(transform=ax6.transAxes, color='w', clip_on=False)
ax6.plot((1-d,1+d), (-d,+d), **kwargs)
ax6.plot((1-d,1+d),(1-d,1+d), **kwargs)
ax6.title.set_text(r'\textit{hmg2}-$\Delta$')
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"           
    x= tc['S288C_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['S288C_HMG2_P_val']]
    s= [i * 100 for i in tc['S288C_HMG2_Coverage']]
    ax6.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle="-",  marker = ">")
    ann(tc, 'S288C_HMG2_Score', y)
    
    x= tc['Y55_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['Y55_HMG2_P_val']]
    s= [i * 100 for i in tc['Y55_HMG2_Coverage']]
    ax6.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "s")
    ann(tc, 'Y55_HMG2_Score', y)
    
    x= tc['UWOPS87_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['UWOPS87_HMG2_P_val']]
    s= [i * 100 for i in tc['UWOPS87_HMG2_Coverage']]
    ax6.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "o")
    ann(tc, 'UWOPS87_HMG2_Score', y)
    
    x= tc['YPS606_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_HMG2_P_val']]
    s= [i * 100 for i in tc['YPS606_HMG2_Coverage']]
    ax6.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")
    ann(tc, 'YPS606_HMG2_Score', y)
    
ax7= fig.add_subplot(gs[1,3])
ax7.set_ylim(1, 7, auto=False)
ax7.set_xlim(1.5, 2.1, auto=False)
ax7.spines['left'].set_visible(False)
ax7.set_yticklabels([])
ax7.set_yticks([])
ax7.set_xticklabels([2])
ax7.set_xticks([2.1])
kwargs.update(transform=ax7.transAxes)  # switch to the bottom axes
ax7.plot((-d,+d), (1-d,1+d), **kwargs)
ax7.plot((-d,+d), (-d,+d), **kwargs)
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         color = "#95a5a6"
    if type_dic == 1:
         tc =exel_dfhc
         color = "#e74c3c"
    if type_dic == 2:
         tc =exel_dfhe
         color = "#2ecc71"
    if type_dic == 3:
         tc = exel_dfhb
         color = "#3498db"
    if type_dic == 4:
         tc = exel_df3f
         color = "#fce166"   
    x= tc['S288C_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['S288C_HMG2_P_val']]
    s= [i * 100 for i in tc['S288C_HMG2_Coverage']]
    ax7.scatter(y=y, x=x , s=s, linewidths=2, c=color,  alpha=0.3, linestyle="-",  marker = ">")
    ann(tc, 'S288C_HMG2_Score', y)
    
    x= tc['Y55_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['Y55_HMG2_P_val']]
    s= [i * 100 for i in tc['Y55_HMG2_Coverage']]
    ax7.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "s")
    ann(tc, 'Y55_HMG2_Score', y)
    
    x= tc['UWOPS87_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['UWOPS87_HMG2_P_val']]
    s= [i * 100 for i in tc['UWOPS87_HMG2_Coverage']]
    ax7.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "o")
    ann(tc, 'UWOPS87_HMG2_Score', y)
    
    x= tc['YPS606_HMG2_Score']
    y= [-(np.log10(i)) for i in tc['YPS606_HMG2_P_val']]
    s= [i * 100 for i in tc['YPS606_HMG2_Coverage']]
    ax7.scatter(y=y, x=x , s=s, linewidths=2, c=color, alpha=0.3, linestyle="-", marker  = "d")
    ann(tc, 'YPS606_HMG2_Score', y)

    
    plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Figure5-C.png", dpi=1000, transparent = False)  
    
important_GO = {}
for type_dic in range(6):
    if type_dic == 0:
         tc = exel_df
         tcn = "TI"
    if type_dic == 1:
         tc =exel_dfhc
         tcn= "CC"
    if type_dic == 2:
         tc =exel_dfhe
         tcn = "EC"
    if type_dic == 3:
         tc = exel_dfhb
         tcn = "BC"
    if type_dic == 4:
         tc = exel_df3f
         tcn = "3FC"

    for given in tc.keys():
        importnat = []
        if re.search(".*Score.*", given):
            IDs  = re.sub("Score", "GO_id", given)
            covs = re.sub("Score", "Coverage", given)
            for name , score, coverage, pval in zip(tc[IDs], tc[given],
                                               tc[covs], y ):
                if score > 0.5 or coverage > 0.5:
                    print(name)
                    importnat.append(name)
        important_GO["{}_{}".format(tcn, given)] = importnat
        
df = pd.DataFrame.from_dict(important_GO, orient='index') 
df = df.transpose()  
df.to_excel("/home/chemgen/Desktop/Temp/Final_Figures/{}.xlsx".format("Important_go")) 
            
