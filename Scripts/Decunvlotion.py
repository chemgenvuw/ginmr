#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 11 20:39:56 2018

@author: chemgen
"""
from matplotlib.lines import Line2D
import matplotlib.gridspec as gridspec

def scatter_for_each_background(bt_list ,name , c):
    sorted_bt = sorted(bt_list)[-5:]
    x_val = bt_list * np.sin(bt_list)
    y_val = bt_list * np.cos(bt_list) 
    plt.axis('off')
    list_gene = []
    ax =plt.scatter(x_val , y_val, c = c)#,cmap=cdict)
    for label, x, y , cat_c in zip(name, x_val, y_val, bt_list):
        if  cat_c in sorted_bt:
            list_gene.append(label)
            plt.annotate(
                "",
                xy=(x, y), xytext=(-20, 20),
                textcoords='offset points', ha='right', va='bottom',
                arrowprops=dict(arrowstyle = '->', color='y',alpha = 1,
                                connectionstyle='arc3,rad=0')) 
    list_gene = id_concvertor(list_gene)
    plt.annotate("\n".join(list_gene), xy=(x, y), xytext=(x, y),
                 fontsize = 4)
    return ax 
#%%%
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi=1000)
gs = gridspec.GridSpec(4,5, left=None, bottom=None, right=None, top=None,
                wspace=None, hspace=None)
pad = 5
fig.suptitle("Deconvoluted Genes with Maximum Closeness Centrality", fontsize=16)

ax0_0 = fig.add_subplot(gs[0,0:1])
ax0_0.annotate(r"Ator", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_0.annotate("S288C", xy=(0, 0.1), xytext=(-ax0_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax0_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax0_0 = scatter_for_each_background(plot_dic['S288C']["ATOR_closeness"] ,plot_dic['S288C']["ATOR_names"], 
                       "red")

ax0_1 = fig.add_subplot(gs[0,1:2])
ax0_1.annotate(r"Ceri", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_1 = scatter_for_each_background(plot_dic['S288C']["CERI_closeness"] ,plot_dic['S288C']["CERI_names"], 
                       "red")

ax0_2 = fig.add_subplot(gs[0,2:3])
ax0_2.annotate(r'\textit{hmg1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_2 = scatter_for_each_background(plot_dic['S288C']["HMG1_closeness"],plot_dic['S288C']["HMG1_names"], 
                       "red")

ax0_3 = fig.add_subplot(gs[0,3:4])
ax0_3.annotate(r'\textit{hmg2}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_3 = scatter_for_each_background(plot_dic['S288C']["HMG2_closeness"] ,plot_dic['S288C']["HMG2_names"], 
                       "red")

ax0_4 = fig.add_subplot(gs[0,4:5])
ax0_4.annotate(r'\textit{arv1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_4 = scatter_for_each_background(plot_dic['S288C']["ARV1_closeness"],plot_dic['S288C']["ARV1_names"], 
                       "red")
########
ax1_0 = fig.add_subplot(gs[1,0:1])
ax1_0.annotate("Y55", xy=(0, 0.1), xytext=(-ax1_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax1_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax1_0 = scatter_for_each_background(plot_dic['Y55']["ATOR_closeness"] ,plot_dic['Y55']["ARV1_names"], 
                       "red")

ax1_1 = fig.add_subplot(gs[1,1:2])
ax1_1 = scatter_for_each_background(plot_dic['Y55']["CERI_closeness"]  ,plot_dic['Y55']["ARV1_names"], 
                       "red")

ax1_2 = fig.add_subplot(gs[1,2:3])
ax1_2 = scatter_for_each_background(plot_dic['Y55']["HMG1_closeness"]  ,plot_dic['Y55']["ARV1_names"], 
                       "red")

ax1_3 = fig.add_subplot(gs[1,3:4])
ax1_3 = scatter_for_each_background(plot_dic['Y55']["HMG2_closeness"] ,plot_dic['Y55']["ARV1_names"], 
                       "red")

ax1_4 = fig.add_subplot(gs[1,4:5])
ax1_4 = scatter_for_each_background(plot_dic['Y55']["ARV1_closeness"] ,plot_dic['Y55']["ARV1_names"], 
                       "red")
#######
ax2_0 = fig.add_subplot(gs[2,0:1])
ax2_0.annotate("UWOPS87", xy=(0, 0.1), xytext=(-ax2_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax2_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax2_0 = scatter_for_each_background(plot_dic['UWOPS87']["ATOR_closeness"]  ,plot_dic['UWOPS87']["ATOR_names"], 
                       "red")


ax2_1 = fig.add_subplot(gs[2,1:2])
ax2_1 = scatter_for_each_background(plot_dic['UWOPS87']["CERI_closeness"]  ,plot_dic['UWOPS87']["CERI_names"], 
                       "red")

ax2_2 = fig.add_subplot(gs[2,2:3])
ax2_2 = scatter_for_each_background(plot_dic['UWOPS87']["HMG1_closeness"] ,plot_dic['UWOPS87']["HMG1_names"], 
                       "red")

ax2_3 = fig.add_subplot(gs[2,3:4])
ax2_3 = scatter_for_each_background(plot_dic['UWOPS87']["HMG2_closeness"], plot_dic['UWOPS87']["HMG2_names"], 
                       "red")

ax2_4 = fig.add_subplot(gs[2,4:5])
ax2_4 = scatter_for_each_background(plot_dic['UWOPS87']["ARV1_closeness"], plot_dic['UWOPS87']["ARV1_names"], 
                       "red")
########
ax3_0 = fig.add_subplot(gs[3,0:1])
ax3_0.annotate("YPS606", xy=(0, 0.1), xytext=(-ax3_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax3_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax3_0 = scatter_for_each_background(plot_dic['YPS606']["ATOR_closeness"], plot_dic['YPS606']["ATOR_names"], 
                       "red")

ax3_1 = fig.add_subplot(gs[3,1:2])
ax3_1 = scatter_for_each_background(plot_dic['YPS606']["CERI_closeness"], plot_dic['YPS606']["CERI_names"], 
                       "red")

ax3_2 = fig.add_subplot(gs[3,2:3])
ax3_2 = scatter_for_each_background(plot_dic['YPS606']["HMG1_closeness"] ,plot_dic['YPS606']["HMG1_names"], 
                       "red")

ax3_3 = fig.add_subplot(gs[3,3:4])
ax3_3 = scatter_for_each_background(plot_dic['YPS606']["HMG2_closeness"] ,plot_dic['YPS606']["HMG2_names"], 
                       "red")

ax3_4 = fig.add_subplot(gs[3,4:5])
ax3_4 = scatter_for_each_background(plot_dic['YPS606']["ARV1_closeness"], plot_dic['YPS606']["ARV1_names"],
                       "red")

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Supplementary_Figure5_Closeness", dpi=1000, transparent = False)  

#%%%
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi=1000)
gs = gridspec.GridSpec(4,5, left=None, bottom=None, right=None, top=None,
                wspace=None, hspace=None)
pad = 5
fig.suptitle("Deconvoluted Genes with Maximum Betweenness Centrality", fontsize=16)

ax0_0 = fig.add_subplot(gs[0,0:1])
ax0_0.annotate(r"Ator", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_0.annotate("S288C", xy=(0, 0.1), xytext=(-ax0_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax0_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax0_0 = scatter_for_each_background(plot_dic['S288C']["ATOR_betwenness"] ,plot_dic['S288C']["ATOR_names"], 
                       "lightskyblue")

ax0_1 = fig.add_subplot(gs[0,1:2])
ax0_1.annotate(r"Ceri", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_1 = scatter_for_each_background(plot_dic['S288C']["CERI_betwenness"] ,plot_dic['S288C']["CERI_names"], 
                       "lightskyblue")

ax0_2 = fig.add_subplot(gs[0,2:3])
ax0_2.annotate(r'\textit{hmg1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_2 = scatter_for_each_background(plot_dic['S288C']["HMG1_betwenness"],plot_dic['S288C']["HMG1_names"], 
                       "lightskyblue")

ax0_3 = fig.add_subplot(gs[0,3:4])
ax0_3.annotate(r'\textit{hmg2}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_3 = scatter_for_each_background(plot_dic['S288C']["HMG2_betwenness"] ,plot_dic['S288C']["HMG2_names"], 
                       "lightskyblue")

ax0_4 = fig.add_subplot(gs[0,4:5])
ax0_4.annotate(r'\textit{arv1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_4 = scatter_for_each_background(plot_dic['S288C']["ARV1_betwenness"],plot_dic['S288C']["ARV1_names"], 
                       "lightskyblue")
########
ax1_0 = fig.add_subplot(gs[1,0:1])
ax1_0.annotate("Y55", xy=(0, 0.1), xytext=(-ax1_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax1_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax1_0 = scatter_for_each_background(plot_dic['Y55']["ATOR_betwenness"] ,plot_dic['Y55']["ARV1_names"], 
                       "lightskyblue")

ax1_1 = fig.add_subplot(gs[1,1:2])
ax1_1 = scatter_for_each_background(plot_dic['Y55']["CERI_betwenness"]  ,plot_dic['Y55']["ARV1_names"], 
                       "lightskyblue")

ax1_2 = fig.add_subplot(gs[1,2:3])
ax1_2 = scatter_for_each_background(plot_dic['Y55']["HMG1_betwenness"]  ,plot_dic['Y55']["ARV1_names"], 
                       "lightskyblue")

ax1_3 = fig.add_subplot(gs[1,3:4])
ax1_3 = scatter_for_each_background(plot_dic['Y55']["HMG2_betwenness"] ,plot_dic['Y55']["ARV1_names"], 
                       "lightskyblue")

ax1_4 = fig.add_subplot(gs[1,4:5])
ax1_4 = scatter_for_each_background(plot_dic['Y55']["ARV1_betwenness"] ,plot_dic['Y55']["ARV1_names"], 
                       "lightskyblue")
#######
ax2_0 = fig.add_subplot(gs[2,0:1])
ax2_0.annotate("UWOPS87", xy=(0, 0.1), xytext=(-ax2_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax2_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax2_0 = scatter_for_each_background(plot_dic['UWOPS87']["ATOR_betwenness"]  ,plot_dic['UWOPS87']["ATOR_names"], 
                       "lightskyblue")


ax2_1 = fig.add_subplot(gs[2,1:2])
ax2_1 = scatter_for_each_background(plot_dic['UWOPS87']["CERI_betwenness"]  ,plot_dic['UWOPS87']["CERI_names"], 
                       "lightskyblue")

ax2_2 = fig.add_subplot(gs[2,2:3])
ax2_2 = scatter_for_each_background(plot_dic['UWOPS87']["HMG1_betwenness"] ,plot_dic['UWOPS87']["HMG1_names"], 
                       "lightskyblue")

ax2_3 = fig.add_subplot(gs[2,3:4])
ax2_3 = scatter_for_each_background(plot_dic['UWOPS87']["HMG2_betwenness"], plot_dic['UWOPS87']["HMG2_names"], 
                       "lightskyblue")

ax2_4 = fig.add_subplot(gs[2,4:5])
ax2_4 = scatter_for_each_background(plot_dic['UWOPS87']["ARV1_betwenness"], plot_dic['UWOPS87']["ARV1_names"], 
                       "lightskyblue")
########
ax3_0 = fig.add_subplot(gs[3,0:1])
ax3_0.annotate("YPS606", xy=(0, 0.1), xytext=(-ax3_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax3_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax3_0 = scatter_for_each_background(plot_dic['YPS606']["ATOR_betwenness"], plot_dic['YPS606']["ATOR_names"], 
                       "lightskyblue")

ax3_1 = fig.add_subplot(gs[3,1:2])
ax3_1 = scatter_for_each_background(plot_dic['YPS606']["CERI_betwenness"], plot_dic['YPS606']["CERI_names"], 
                       "lightskyblue")

ax3_2 = fig.add_subplot(gs[3,2:3])
ax3_2 = scatter_for_each_background(plot_dic['YPS606']["HMG1_betwenness"] ,plot_dic['YPS606']["HMG1_names"], 
                       "lightskyblue")

ax3_3 = fig.add_subplot(gs[3,3:4])
ax3_3 = scatter_for_each_background(plot_dic['YPS606']["HMG2_betwenness"] ,plot_dic['YPS606']["HMG2_names"], 
                       "lightskyblue")

ax3_4 = fig.add_subplot(gs[3,4:5])
ax3_4 = scatter_for_each_background(plot_dic['YPS606']["ARV1_betwenness"], plot_dic['YPS606']["ARV1_names"],
                       "lightskyblue")

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Supplementary_Figure5_betwenness", dpi=1000, transparent = False)  


#%%%
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi=1000)
gs = gridspec.GridSpec(4,5, left=None, bottom=None, right=None, top=None,
                wspace=None, hspace=None)
pad = 5
fig.suptitle("Deconvoluted Genes with Maximum Eigenvector Centrality", fontsize=16)

ax0_0 = fig.add_subplot(gs[0,0:1])
ax0_0.annotate(r"Ator", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_0.annotate("S288C", xy=(0, 0.1), xytext=(-ax0_0.yaxis.labelpad + (3 *pad), 0),
                xycoords=ax0_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax0_0 = scatter_for_each_background(plot_dic['S288C']["ATOR_eigenvector"] ,plot_dic['S288C']["ATOR_names"], 
                       "g")

ax0_1 = fig.add_subplot(gs[0,1:2])
ax0_1.annotate(r"Ceri", xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_1 = scatter_for_each_background(plot_dic['S288C']["CERI_eigenvector"] ,plot_dic['S288C']["CERI_names"], 
                       "g")

ax0_2 = fig.add_subplot(gs[0,2:3])
ax0_2.annotate(r'\textit{hmg1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_2 = scatter_for_each_background(plot_dic['S288C']["HMG1_eigenvector"],plot_dic['S288C']["HMG1_names"], 
                       "g")

ax0_3 = fig.add_subplot(gs[0,3:4])
ax0_3.annotate(r'\textit{hmg2}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_3 = scatter_for_each_background(plot_dic['S288C']["HMG2_eigenvector"] ,plot_dic['S288C']["HMG2_names"], 
                       "g")

ax0_4 = fig.add_subplot(gs[0,4:5])
ax0_4.annotate(r'\textit{arv1}-$\Delta$', xy=(0.5, 1), xytext=(0, pad),
                xycoords='axes fraction', textcoords='offset points',
                fontsize=10, ha='center', va='baseline')
ax0_4 = scatter_for_each_background(plot_dic['S288C']["ARV1_eigenvector"],plot_dic['S288C']["ARV1_names"], 
                       "g")
########
ax1_0 = fig.add_subplot(gs[1,0:1])
ax1_0.annotate("Y55", xy=(0, 0.1), xytext=(-ax1_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax1_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax1_0 = scatter_for_each_background(plot_dic['Y55']["ATOR_eigenvector"] ,plot_dic['Y55']["ARV1_names"], 
                       "g")

ax1_1 = fig.add_subplot(gs[1,1:2])
ax1_1 = scatter_for_each_background(plot_dic['Y55']["CERI_eigenvector"]  ,plot_dic['Y55']["ARV1_names"], 
                       "g")

ax1_2 = fig.add_subplot(gs[1,2:3])
ax1_2 = scatter_for_each_background(plot_dic['Y55']["HMG1_eigenvector"]  ,plot_dic['Y55']["ARV1_names"], 
                       "g")

ax1_3 = fig.add_subplot(gs[1,3:4])
ax1_3 = scatter_for_each_background(plot_dic['Y55']["HMG2_eigenvector"] ,plot_dic['Y55']["ARV1_names"], 
                       "g")

ax1_4 = fig.add_subplot(gs[1,4:5])
ax1_4 = scatter_for_each_background(plot_dic['Y55']["ARV1_eigenvector"] ,plot_dic['Y55']["ARV1_names"], 
                       "g")
#######
ax2_0 = fig.add_subplot(gs[2,0:1])
ax2_0.annotate("UWOPS87", xy=(0, 0.1), xytext=(-ax2_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax2_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax2_0 = scatter_for_each_background(plot_dic['UWOPS87']["ATOR_eigenvector"]  ,plot_dic['UWOPS87']["ATOR_names"], 
                       "g")


ax2_1 = fig.add_subplot(gs[2,1:2])
ax2_1 = scatter_for_each_background(plot_dic['UWOPS87']["CERI_eigenvector"]  ,plot_dic['UWOPS87']["CERI_names"], 
                       "g")

ax2_2 = fig.add_subplot(gs[2,2:3])
ax2_2 = scatter_for_each_background(plot_dic['UWOPS87']["HMG1_eigenvector"] ,plot_dic['UWOPS87']["HMG1_names"], 
                       "g")

ax2_3 = fig.add_subplot(gs[2,3:4])
ax2_3 = scatter_for_each_background(plot_dic['UWOPS87']["HMG2_eigenvector"], plot_dic['UWOPS87']["HMG2_names"], 
                       "g")

ax2_4 = fig.add_subplot(gs[2,4:5])
ax2_4 = scatter_for_each_background(plot_dic['UWOPS87']["ARV1_eigenvector"], plot_dic['UWOPS87']["ARV1_names"], 
                       "g")
########
ax3_0 = fig.add_subplot(gs[3,0:1])
ax3_0.annotate("YPS606", xy=(0, 0.1), xytext=(-ax3_0.yaxis.labelpad + (3*pad), 0),
                xycoords=ax3_0.yaxis.label, textcoords='offset points',
                fontsize=10, ha='right', va='center',rotation=90)
ax3_0 = scatter_for_each_background(plot_dic['YPS606']["ATOR_eigenvector"], plot_dic['YPS606']["ATOR_names"], 
                       "g")

ax3_1 = fig.add_subplot(gs[3,1:2])
ax3_1 = scatter_for_each_background(plot_dic['YPS606']["CERI_eigenvector"], plot_dic['YPS606']["CERI_names"], 
                       "g")

ax3_2 = fig.add_subplot(gs[3,2:3])
ax3_2 = scatter_for_each_background(plot_dic['YPS606']["HMG1_eigenvector"] ,plot_dic['YPS606']["HMG1_names"], 
                       "g")

ax3_3 = fig.add_subplot(gs[3,3:4])
ax3_3 = scatter_for_each_background(plot_dic['YPS606']["HMG2_eigenvector"] ,plot_dic['YPS606']["HMG2_names"], 
                       "g")

ax3_4 = fig.add_subplot(gs[3,4:5])
ax3_4 = scatter_for_each_background(plot_dic['YPS606']["ARV1_eigenvector"], plot_dic['YPS606']["ARV1_names"],
                       "g")

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Supplementary_Figure5_Eigenvector", dpi=1000, transparent = False)  