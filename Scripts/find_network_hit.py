#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 12:46:01 2017

@author: chemgen
"""

import pandas as pd
import numpy as np
import os

filepath = "/media/chemgen/Eli_PFD/Bede Suppl data Aug 2017"
uotput_path = "/home/chemgen/Desktop/OneDrive/notebook/Logs/Pauls_Figure/Network"

ref_file_dic = {}
ref_file = pd.read_csv( "/home/chemgen/Desktop/Temp/safe/data/Costanzo_Science_2010_5col.txt", header= None, sep = "\t")
for row in ref_file.iterrows() :
    ref_file_dic[row[1][1]] = [row[1][3], row[1][4]]


for root, dirs, files in os.walk(filepath):
    for file in files:
        if file.endswith(".xlsx"):
            ORF_list = []
            file = os.path.join(root, file)
            df_file = pd.ExcelFile(file)
            df_parsed = df_file.parse('Sheet1')
            ORFS = df_parsed['ID Column']
            for ORF in ORFS:
                    ORF_list.append(ORF)
            file_out_name = os.path.join(uotput_path,file.split(".")[0] + "_Network.txt")  
            with open (file_out_name, "w") as outputfile :
                for ORF in ORFS:
                    if ORF in ref_file_dic.keys() and ref_file_dic[ORF][0] in ORF_list:
                        outputfile.write(str(ORF) + '\t' + str(ref_file_dic[ORF][0]) + '\t' + "1" + '\n')
                    if ORF in ref_file_dic.keys() :
                        if ORF == ref_file_dic[ORF][0] and ORF in ref_file_dic.keys():
                            outputfile.write(str(ORF) + '\t' + str(ref_file_dic[ORF][0]) + '\t' + "1" + '\n')
                
                        
