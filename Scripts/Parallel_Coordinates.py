#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import csv
import pandas.plotting 
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
plt.style.use("dark_background")
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

from __future__ import division

import numpy as np

from pandas.util._decorators import deprecate_kwarg
from pandas.core.dtypes.missing import notna
from pandas.compat import range, lrange, lmap, zip
from pandas.io.formats.printing import pprint_thing


from pandas.plotting._style import _get_standard_colors
from pandas.plotting._tools import _subplots, _set_ticks_props
from pandas.plotting import radviz

from matplotlib.transforms import Affine2D
import mpl_toolkits.axisartist.floating_axes as floating_axes
import numpy as np
import mpl_toolkits.axisartist.angle_helper as angle_helper
from matplotlib.projections import PolarAxes
from mpl_toolkits.axisartist.grid_finder import (FixedLocator, MaxNLocator,
                                                 DictFormatter)
import matplotlib.pyplot as plt

def setup_axes1(fig, rect):
    """
    for key
    """
    tr = Affine2D().scale(2, 1).rotate_deg(90)

    grid_helper = floating_axes.GridHelperCurveLinear(
        tr, extremes=(-0.5, 3.5, 0, 4))

    ax1 = floating_axes.FloatingSubplot(fig, rect, grid_helper=grid_helper)
    fig.add_subplot(ax1)

    aux_ax = ax1.get_aux_axes(tr)

    grid_helper.grid_finder.grid_locator1._nbins = 4
    grid_helper.grid_finder.grid_locator2._nbins = 4

    return ax1, aux_ax
# change the function to keep the legend line 133
#fig = plt.figure(1, dpi = 1000)
#
#ax1, aux_ax1 = setup_axes1(fig, 131)
#parallel_coordinates_m(df, 'SGA', color=('#fce166', '#3498db', '#e74c3c', '#2ecc71'))
#plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Figure5-B_key.png", dpi=1000,
#            layout= "tight", transparent = False)   


def parallel_coordinates_m(frame, class_column, cols=None, ax=None, color=None,
                         use_columns=False, xticks=None, colormap=None,
                         axvlines=True, axvlines_kwds=None, sort_labels=False,
                         **kwds):
    if axvlines_kwds is None:
        axvlines_kwds = {'linewidth': 1, 'color': 'black'}
    import matplotlib.pyplot as plt

    n = len(frame)
    classes = frame[class_column].drop_duplicates()
    class_col = frame[class_column]

    if cols is None:
        df = frame.drop(class_column, axis=1)
    else:
        df = frame[cols]

    used_legends = set([])

    ncols = len(df.columns)

    # determine values to use for xticks
    if use_columns is True:
        if not np.all(np.isreal(list(df.columns))):
            raise ValueError('Columns must be numeric to be used as xticks')
        x = df.columns
    elif xticks is not None:
        if not np.all(np.isreal(xticks)):
            raise ValueError('xticks specified must be numeric')
        elif len(xticks) != ncols:
            raise ValueError('Length of xticks must match number of columns')
        x = xticks
    else:
        x = lrange(ncols)

    if ax is None:
        ax = plt.gca()

    color_values = _get_standard_colors(num_colors=len(classes),
                                        colormap=colormap, color_type='random',
                                        color=color)

    if sort_labels:
        classes = sorted(classes)
        color_values = sorted(color_values)
    colors = dict(zip(classes, color_values))

    for i in range(n):
        y = df.iloc[i].values
        kls = class_col.iat[i]
        label = pprint_thing(kls)
        if label not in used_legends:
            used_legends.add(label)
            ax.plot(x, y, color=colors[kls], label=label, **kwds)
        else:
            ax.plot(x, y, color=colors[kls], **kwds)

    if axvlines:
        for i in x:
            ax.axvline(i, c= "w")
    ax.set_yticks([0, 1])
    ax.set_xticks(x)
    ax.set_yticklabels(["-", "+"])
    ax.set_xticklabels(["Algorithm", "Silhouette ", "C.H.",
                               "$\#$Clusters"])
    ax.set_xlim(x[0], x[-1])
    ax.legend(loc='upper right')
    ax.legend_.remove()
    ax.grid(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.yaxis.label.set_color("#3498db")
    ax.xaxis.label.set_color("#3498db")
    ax.tick_params(axis='y', labelsize= 26 ,colors="#3498db")
    ax.tick_params(axis='x', labelsize=17 )
                   
   
    #ax.xaxis.tick_top()
    for tick in ax.get_xticklabels():
        tick.set_rotation(90)
    for tick in ax.get_yticklabels():
        tick.set_rotation(90)
    locs = ax.get_xticks()

    return ax



df_file = pd.ExcelFile("/home/chemgen/Desktop/Temp/Final_Figures/Supplementary_Figure5_A_melted.xlsx")
df = df_file.parse("melted", index=0)

cols = ["Type", "SCS", "CHI", "Number"]
min_max_range = {}
for col in cols:
    min_max_range[col] = [df[col].min(), df[col].max(), np.ptp(df[col])]
    df[col] = np.true_divide(df[col] - df[col].min(), np.ptp(df[col]))


fig, ax = plt.subplots(figsize=(8, 6))         
parallel_coordinates_m(df, 'SGA', color=('#fce166', '#3498db', '#e74c3c', '#2ecc71'))
fig.tight_layout()
plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Figure5-B.png", dpi=1000,
            layout= "tight", transparent = False)  




