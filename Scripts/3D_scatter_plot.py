#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  1 13:39:05 2018

@author: chemgen
"""

"""3D Plot"""
from mpl_toolkits.mplot3d import Axes3D
plt.style.use('seaborn-paper')
plt.style.use("dark_background")
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
fig = plt.figure(dpi = 1000)
ax = fig.add_subplot(111, projection='3d')
fig.patch.set_facecolor('white')


ax.set_zticks([0.0e6, 0.2e6, 0.4e6, 0.6e6, 0.8e6, 1.0e6, 1.2e6, 1.4e6])
ax.set_zticklabels([0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4  ])

from matplotlib.colors import LinearSegmentedColormap
import seaborn as sns
flatui = ["#2ecc71", "#e74c3c", '#feb308', '#2a71b2']
colors  = sns.color_palette(flatui, 4).as_hex()

n_bins =  4 
cmap_name = 'my_list'
cm = LinearSegmentedColormap.from_list(
    cmap_name, colors, N=n_bins)

#plt.ticklabel_format(style='sci', axis='z', scilimits=(0,0), useMathText= True)  
ax.set_xlabel('Closeness Centrality')
ax.set_ylabel('Eigenvector Centrality')
ax.set_zlabel('Betweenness Centrality (x$10^{6}$)' , labelpad=0)
ax.set_xlim([0.1, 0.5])
ax.set_zlim([0, 1500000])
b= ax.zaxis.set_label_coords(0,0)
for key in clustered:
    size =  [(i +1) * 10 for i in clustered[key][4]]
    ax.scatter(clustered[key][2] , clustered[key][3], clustered[key][1],
               c=clustered[key][4], marker = "o" , alpha = 0.7,
               cmap =cm, s = size)
    

from matplotlib.lines import Line2D
cmap = plt.cm.RdBu
legend_elements = [Line2D([0], [0], marker='o', color='k', markerfacecolor='#2ecc71'),
                          Line2D([0], [0], marker='o', color='k', markerfacecolor='#e74c3c'),
                          Line2D([0], [0], marker='o', color='k', markerfacecolor='#feb308'),
                          Line2D([0], [0], marker='o', color='k',  markerfacecolor='#2a71b2')]
                          
ax.legend(legend_elements, [r'Cluster $\alpha$', r'Cluster $\beta$',
                         r'Cluster $\gamma$', r'Cluster $\delta$'], loc=0)

                     
plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/Figure5-A.png", dpi=1000, transparent = False)    
