#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 12:23:02 2018

@author: chemgen
"""
import csv 
import os
import matplotlib as plt
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
import seaborn as sns
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, ClusterNode,leaves_list
from scipy.spatial.distance import pdist
import matplotlib.gridspec as gridspec

raw_data_path = "/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/Raw_data"
mergef_out_df={}

"""
background_key = ["Ator_S288c", "Ator_UWOPS87", "Ator_YPS606", "Ator_Y55",
                  "Ceri_S288c", "Ceri_UWOPS87", "Ceri_YPS606", "Ceri_Y55",
                  "hmg1_S288c", "hmg1_UWOPS87", "hmg1_YPS606", "hmg1_Y55",
                  "hmg2_S288c", "hmg2_UWOPS87", "hmg2_YPS606", "hmg2_Y55",
                  "arv1_S288c", "arv1_UWOPS87", "arv1_YPS606", "arv1_Y55"]

"""

for root, dirs, files in os.walk(raw_data_path):
    for file in files:
        if file.endswith(".txt"):  
            SGA_name= file.split(".")[0]
            file = os.path.join(root, file)
            tem_dic = {}
            with open(file , "r", encoding='iso-8859-1') as csv_file:
                merged_csv = csv.reader(csv_file ,delimiter='\t', quotechar='|')
                for row in merged_csv:
                    tem_dic[row[18]] = row[6]
            mergef_out_df[SGA_name] =  tem_dic        



df = pd.DataFrame(mergef_out_df)                      
df_parsed = pd.DataFrame.from_dict(mergef_out_df)
#df_parsed.to_excel("/home/chemgen/Desktop/Sgas_meged.xlsx")

 
df_file = pd.ExcelFile("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Sgas_merged_scores.xlsx")
df_parsed = df_file.parse("Sheet1", index_col =0)

num_col =0
for i in df_parsed.iteritems():
    num_col += 1
    
for i in range(num_col):
    float_array = df_parsed.iloc[:,i].values.astype(float).reshape(-1,1)
    imputer = preprocessing.Imputer()
    float_array= imputer.fit_transform(float_array)
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(float_array)
    df_parsed.iloc[:,i] = x_scaled
    
latex_row_name = {}
for i in range(len(df_parsed.index)):
    latex_row_name[df_parsed.index[i]] = "\\textit{%s}-$\Delta$" % df_parsed.index[i].lower()
    
df_parsed.rename(index=latex_row_name, inplace=True)

#### GET THE ZOOM IN

#row number in file /home/chemgen/Desktop/notebook/Logs/Pauls_Figure/Sgas_merged_scores.xlsx - #2
#hmg1 = 2967 -> 1 -> 0:10
#hmg2 = 2898 -> 0 -> 0:10

#hac1 = 1245 -> 4241 - 4236:4246
#ire1 = 1778 -> 96  -> 91:101
#sil1 = 3577 -> 215 -> 211:219

z= linkage(df_parsed, 'centroid')
c, coph_dists = cophenet(z, pdist(df_parsed))

leaves_list_array = leaves_list(z)
list_leaves =leaves_list(z).tolist()
list_leaves.index(3575)

#num_col =0
#for i in df_parsed.iteritems():
#    num_col += 1
#    
#for i in range(num_col):
#    float_array = df_parsed.iloc[:,i].values.astype(float).reshape(-1,1)
#    imputer = preprocessing.Imputer()
#    float_array= imputer.fit_transform(float_array)
#    min_max_scaler = preprocessing.MinMaxScaler()
#    x_scaled = min_max_scaler.fit_transform(float_array)
#    df_parsed.iloc[:,i] = x_scaled
#    
#    

hmg1_2_neighbour = leaves_list_array[0:10].tolist()
hac1_neighbour = leaves_list_array[4236:4246]
ire1_neighbour = leaves_list_array[91:101]
sil1_neighbour = leaves_list_array[211:219]

#%%

df_file_m = pd.ExcelFile("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/dataset/Mevalonate_pathway_scores.xlsx")
df_parsed_m = df_file.parse("name", index_col =0)

num_col =0
for i in df_parsed_m.iteritems():
    num_col += 1
    
for i in range(num_col):
    float_array = df_parsed_m.iloc[:,i].values.astype(float).reshape(-1,1)
    imputer = preprocessing.Imputer()
    float_array= imputer.fit_transform(float_array)
    min_max_scaler = preprocessing.MinMaxScaler()
    x_scaled = min_max_scaler.fit_transform(float_array)
    df_parsed_m.iloc[:,i] = x_scaled
    
latex_row_name = {}
for i in range(len(df_parsed.index)):
    latex_row_name[df_parsed_m.index[i]] = "\\textit{%s}-$\Delta$" % df_parsed.index[i].lower()
df_parsed_m.rename(index=latex_row_name, inplace=True)   

#%%
plt.style.use('seaborn-paper')
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)
cmap = sns.diverging_palette(220, 15, sep=90, as_cmap=True)

fig = plt.figure(dpi=1000)
gs = gridspec.GridSpec(3,2, left=None, bottom=None, right=None, top=None,
                wspace=None, hspace=None)

ax0_0 = fig.add_subplot(gs[0:2,0])
ax0_0 = sns.clustermap(df_parsed, cmap =cmap, method="centroid")
ax0_0.cax.set_visible(False)
ax0_0.ax_row_dendrogram.set_visible(False)
plt.setp(ax0_0.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
hm = ax0_0.ax_heatmap.get_position()
ax0_0.ax_heatmap.set_position([hm.x0, hm.y0, hm.width*0.5, hm.height*1.5])
col = ax0_0.ax_col_dendrogram.get_position()
ax0_0.ax_col_dendrogram.set_position([col.x0, col.y0*1.4, col.width*0.5, col.height*0.9])
row = ax0_0.ax_row_dendrogram.get_position()
ax0_0.ax_row_dendrogram.set_position([row.x0, row.y0, row.width, row.height*1.2])

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/3-A.png", dpi=1000,
            bbox_inches = 'tight', transparent = True)

ax0_1 = fig.add_subplot(gs[2:4,0])
ax0_1 = sns.clustermap(df_parsed.iloc[hmg1_2_neighbour], cmap =cmap,method="centroid")
ax0_1.cax.set_visible(False)
plt.setp(ax0_1.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
plt.setp(ax0_1.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
hm = ax0_1.ax_heatmap.get_position()
ax0_1.ax_heatmap.set_position([hm.x0, hm.y0, hm.width*0.5, hm.height*0.4])
col = ax0_1.ax_col_dendrogram.get_position()
ax0_1.ax_col_dendrogram.set_position([col.x0, col.y0*0.5, col.width*0.5, col.height])
row = ax0_1.ax_row_dendrogram.get_position()
ax0_1.ax_row_dendrogram.set_position([row.x0, row.y0, row.width, row.height*0.4])

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/3-C.png", dpi=1000,
            bbox_inches = 'tight', transparent = True)

#
#cm = sns.clustermap(df_parsed.iloc[hac1_neighbour], cmap =cmap,method="centroid")
#plt.setp(cm.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
#plt.setp(cm.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
#hm = cm.ax_heatmap.get_position()
#cm.ax_heatmap.set_position([hm.x0, hm.y0, hm.width*0.5, hm.height*0.35])
#col = cm.ax_col_dendrogram.get_position()
#cm.ax_col_dendrogram.set_position([col.x0, col.y0*0.45, col.width*0.5, col.height])
#row = cm.ax_row_dendrogram.get_position()
#cm.ax_row_dendrogram.set_position([row.x0*1.6, row.y0, row.width, row.height*0.35])
#plt.savefig("/home/chemgen/Desktop/notebook/Logs/Pauls_Figure/ire", dpi=500,
#            bbox_inches = 'tight', transparent = True)

ax0_2 = fig.add_subplot(gs[2:4,1])
ax0_2 = sns.clustermap(df_parsed.iloc[ire1_neighbour], cmap =cmap,method="centroid" )
ax0_1.cax.set_visible(False)
plt.setp(ax0_2.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
plt.setp(ax0_2.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
hm = ax0_2.ax_heatmap.get_position()
ax0_2.ax_heatmap.set_position([hm.x0, hm.y0, hm.width*0.5, hm.height*0.4])
col = ax0_2.ax_col_dendrogram.get_position()
ax0_2.ax_col_dendrogram.set_position([col.x0, col.y0*0.5, col.width*0.5, col.height])
row = ax0_2.ax_row_dendrogram.get_position()
ax0_2.ax_row_dendrogram.set_position([row.x0, row.y0, row.width, row.height*0.4])

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/3-B.png", dpi=1000,
            bbox_inches = 'tight', transparent = True)

ax0_2 = fig.add_subplot(gs[0:4,1])
cm = sns.clustermap(df_parsed, cmap =cmap, method="centroid")
plt.setp(cm.ax_heatmap.xaxis.get_majorticklabels(), rotation=90)
plt.setp(cm.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
hm = cm.ax_heatmap.get_position()
cm.ax_heatmap.set_position([hm.x0, hm.y0, hm.width*0.5, hm.height*0.8])
col = cm.ax_col_dendrogram.get_position()
cm.ax_col_dendrogram.set_position([col.x0, col.y0*0.83, col.width*0.5, col.height])
row = cm.ax_row_dendrogram.get_position()
cm.ax_row_dendrogram.set_position([row.x0, row.y0, row.width, row.height*0.8])

plt.savefig("/home/chemgen/Desktop/Temp/Final_Figures/3-D.png", dpi=500,
            bbox_inches = 'tight', transparent = True)





    